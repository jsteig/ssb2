#include "declares.h"
#include "globals.h"

#pragma comment(lib, "ws2_32")

/*void PrintChats()
{
	_listnode <Chat> * parse = DATABASE.ChatDatabase.head;

	printf("\n<<<<<<<<<<Chat list>>>>>>>>>>\n");

	while (parse)
	{
		printf("%s:", parse->item->Name);

		{
			_listnode <Chatter> * chatter = parse->item->Participants.head;

			while (chatter)
			{
				printf("%s", chatter->item->Player->player->Name);
				chatter = chatter->next;
				if (chatter) printf(",");
			}
		}

		printf("\n");

		parse = parse->next;
	}
	printf("<<<<<<<<<<<<<< >>>>>>>>>>>>>>\n\n");
}

void PrintSquads()
{
	_listnode <Squad> * parse = DATABASE.SquadDatabase.head;

	printf("\n<<<<<<<<<<Squad list>>>>>>>>>>\n");

	while (parse)
	{
		printf("%s:", parse->item->Name);

		{
			_listnode <User> * squaddie = parse->item->Participants.head;

			while (squaddie)
			{
				printf("%s", squaddie->item->Name);
				squaddie = squaddie->next;
				if (squaddie) printf(",");
			}
		}

		printf("\n");

		parse = parse->next;
	}
	printf("<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>\n\n");
}*/

void main()
{
	printf("Billing server version %s starting\n", VERSION);

	/* Load .INI files */
	DATABASE.LoadSettings();

	/* Load .DAT files */
	DATABASE.LoadFromDisk();

	/* Create a Biller */
	WinsockSession Biller;
	Biller.Initialize(DATABASE.Port);

	printf("\n");

	bool Running = true;
	DWORD Counter = GetTickCount();
	do
	{
		Sleep(250);

		if (GetTickCount() >= DATABASE.PeriodicSave + Counter)
		{
			_listnode <Ban> * parse;
			_listnode <Ban> * skip;
			time_t CDate;
			time(&CDate);

		Biller.LockClass();
			// ?write
			DATABASE.SaveToDisk();

			// Ban timer expiration
			parse = DATABASE.BanDatabase.head;
			while (parse)
			{
				skip = parse;
				parse = parse->next;

				if (skip->item->Timer < CDate)
					DATABASE.BanDatabase.Delete(skip, false);
			}
		Biller.UnlockClass();

			Sleep(500);

			Counter = GetTickCount();
		}

		if (kbhit())
		{
			switch (getch())
			{
			case 'e':
			case 'E':
			case 'q':
			case 'Q':
			case 'x':
			case 'X':
				{
					Running = false;
				}
				break;
			case 'l':
			case 'L':
			case 'h':
			case 'H':
				{
					Biller.LockClass();
					Biller.PrintHosts();
					Biller.UnlockClass();
				}
				break;
			/*case 'c':
			case 'C':
				{
					Biller.LockClass();
					PrintChats();
					Biller.UnlockClass();
				}
				break;
			case 's':
			case 'S':
				{
					Biller.LockClass();
					PrintSquads();
					Biller.UnlockClass();
				}
				break;*/
			};
		}
	} while (Running);

	/* Shut down Biller */
	Biller.Terminate();

	/* Save .DAT files */
	DATABASE.SaveToDisk();
}
