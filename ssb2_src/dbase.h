#ifndef DBASE_H
#define DBASE_H
#endif

Chat * ChatSystem::CreateChat(BYTE * Name, LONG Owner)
{
	_listnode <Chat> * parse = DATABASE.ChatDatabase.head;
	while (parse)
	{
		if (_strnicmp((char*)&parse->item->Name[0], (char*)Name, 31) == 0)
		{
			return NULL;
		}
		parse = parse->next;
	}
	
	Chat * chat = new Chat(Name, Owner);
	DATABASE.ChatDatabase.Append(chat);

	return chat;
}

void ChatSystem::DestroyChat(Chat * chat)
{
	_listnode <Chat> * parse = DATABASE.ChatDatabase.head;
	while (parse)
	{
		if (parse->item == chat)
		{
			if (parse->item->Owner) return;
			DATABASE.ChatDatabase.Delete(parse->item, false);
			chat = NULL;
			return;
		}
		parse = parse->next;
	}

	return;
}

// Active user

LUserNode::LUserNode(Host * NewZone, User * NewPlayer, Score * NewScore, LONG UserID)
		  : FloodProtection()
{
	PlayerID	= UserID;		// Set zone-wide unique player ID
	player		= NewPlayer;	// Set User pointer
	ChatNotify	= (player->regform ? (player->regform->NotifyChat	? true : false) : false);
	BanNotify	= (player->regform ? (player->regform->NotifyBan	? true : false) : false);
	ZoneNotify	= (player->regform ? (player->regform->NotifyZone	? true : false) : false);
								// Load flags
	zone		= NewZone;		// Set Zone pointer
	score		= NewScore;		// Set Score pointer, NULL for no score
	for (int i = 0; i < 10; i++)
		channels[i] = NULL;		// Clear chat channels
	OpStatus	= NULL;			// This would not be a good thing to forget (Yes, i did)
	Ghost		= false;		// Ghosts!  Protection from ?find and ?whois [and ?chat]
	KickTime	= 0;			// Disable the player kick.
	CheckMsgTime	= 0;			// Disable the message check
}

void LUserNode::SendChat(BYTE Channel, const BYTE * Message)
{
	BYTE buffer[512];

	SHORT Length = MakeChatPrivate(buffer, PlayerID, Channel, Message);

	zone->Send(buffer, Length, true);
}

void LUserNode::SendArena(const BYTE * Message)
{
	BYTE buffer[512];

	SHORT Length = MakeArenaPrivate(buffer, PlayerID, Message);

	zone->Send(buffer, Length, true);
}

void LUserNode::SendChat(BYTE Channel, const char * Message)
{
	BYTE buffer[512];

	SHORT Length = MakeChatPrivate(buffer, PlayerID, Channel, (BYTE*)Message);

	zone->Send(buffer, Length, true);
}

void LUserNode::SendArena(const char * Message)
{
	BYTE buffer[512];

	SHORT Length = MakeArenaPrivate(buffer, PlayerID, (BYTE*)Message);

	zone->Send(buffer, Length, true);
}

void LUserNode::CheckKick()
{
	if (KickTime && (GetTickCount() > KickTime))
		Kick(0, KickReason, KickBanID);
	if (CheckMsgTime && (GetTickCount() > CheckMsgTime))
		CheckMsg(0);
}

void LUserNode::CheckMsg(LONG Delay)
{
	if (Delay)
	{
		CheckMsgTime = GetTickCount() + Delay;
	}
	else
	{
		CheckMsgTime = 0;
		LONG Counter = 0;
		_listnode <GMessage> * parse = DATABASE.GMessageDatabase.head;
		while (parse)
		{
			if ((parse->item->UserID == player->LoadID) && (parse->item->Special == 0)) Counter++;
			parse = parse->next;
		}
		if (Counter)
		{
			_string s;
			s = "You have ";
			s += Counter;
			s += " new messages. Type ?msglist to list them and ?msgview # to read one";
			SendArena(s.Text);
		}
	}
}

void LUserNode::Kick(LONG Delay, LONG Reason, LONG BanID)
{
	if (Delay)
	{
		KickTime = GetTickCount() + Delay;
		KickReason = Reason;
		KickBanID = BanID;
	}
	else
	{
		// Send a message about the kick
		if (BanID == 0)
		{
			SendArena("You are locked out of this network");
		}
		else if (BanID != -1)
		{
			_listnode <Ban> * bparse = DATABASE.BanDatabase.head;
			Ban * ban = NULL;

			while (bparse)
			{
				if (bparse->item->BanID == BanID)
				{
					ban = bparse->item;

					break;
				}

				bparse = bparse->next;
			}

			if (ban)
			{
				// Send a message about the ban
				_string bantext;
				bantext = "You have been banned from ";
				if (ban->GroupID == 0)
				{
					if (DATABASE.NetworkName != "")
					{
						bantext += "the ";
						bantext += DATABASE.NetworkName;
					}
					else
					{
						bantext += "this";
					}
					bantext += " network";
				}
				else if (ban->ScoreID == 0)
				{
					bantext += "zone group #";
					bantext += ban->GroupID;
				}
				else
				{
					bantext += (char*)&zone->Name[0];
				}
				bantext += " until ";
				tm * CDate = Localize(&ban->Timer, player->TimeZoneBias);
				if (CDate)
				{
					bantext += CDate->tm_mon + 1;
					if (CDate->tm_mday < 10)
						bantext += "-0";
					else
						bantext += "-";
					bantext += CDate->tm_mday;
					bantext += "-";
					bantext += GetYear(CDate);
				}
				else
				{
					bantext += "(Invalid date)";
				}
				bantext += ".  Ban #";
				bantext += ban->BanID;
				bantext += " by ";
				bantext += (ban->Creator ? (char*)ban->Creator->Name : "(UNKNOWN)");
				SendArena(bantext.Text);

				if ((strlen(DATABASE.NetworkBanText)) && (ban->GroupID == 0))
				{
					SendArena(DATABASE.NetworkBanText);
				}
				else
				{
					_listnode <Zone> * zparse = DATABASE.ZoneDatabase.head;
					while (zparse)
					{
						if (zparse->item->ScoreID == zone->ScoreID)
						{
							if (strlen(zparse->item->BanText))
								SendArena(zparse->item->BanText);
							break;
						}
						zparse = zparse->next;
					}
				}
			}
		}

		BYTE buffer[9];

		MakePlayerKick(buffer, PlayerID, Reason);

		zone->Send(buffer, 9, true);

		KickTime = 0;
	}
}

Chat * LUserNode::JoinChat(BYTE * Name, BYTE ID, bool Notify)
{
	Chat * chan = DATABASE.FindChat(Name);

	if (chan)
	{
		if (chan->PlayerInChat(this))
		{
			printf("NOTICE: %s tried to join chat \"%s\", but is already in it\n", this->player->Name, Name);
			return NULL;
		}

		if ((!chan->PlayerAllowed(this->player->LoadID)) && (!this->OpStatus))
		{
			_string s;
			s = "You are not allowed on chat ";
			s += (char*)&chan->Name[0];

			s += ". The channel is owned by ";

			_listnode <User> * parse = DATABASE.UserDatabase.head;
			
			bool Found = false;
			while (parse)
			{
				if (chan->Owner == parse->item->LoadID)
				{
					s += (char*)&parse->item->Name[0];
					Found = true;

					break;
				}
				parse = parse->next;
			}

			if (!Found) s += "(INVALID)";

			this->SendArena(s.Text);

			return NULL;
		}

		channels[ID] = chan;

		chan->Participants.Append(new Chatter(this, ID));

		if (chan->Participants.Total == 1)
		{
			if (ChatNotify)
			{
			char buffer[64];
			strcpy(buffer, (char*)&player->Name[0]);
			strcat(buffer, " created ");
			strcat(buffer, (char*)&channels[ID]->Name[0]);

			// Route player entering to owner
			SendChat(ID, buffer);
			}
		}
		else
		{
			if (Notify)
				DATABASE.BroadcastEntering(this, chan);
		}

		if (chan->Owner)
		{
			_string s;
			s = "This channel is owned by ";
			_listnode <User> * parse = DATABASE.UserDatabase.head;
			
			bool Found = false;
			while (parse)
			{
				if (chan->Owner == parse->item->LoadID)
				{
					s += (char*)&parse->item->Name[0];
					Found = true;

					break;
				}
				parse = parse->next;
			}

			if (!Found) s += "(INVALID)";

			SendChat(ID, s.Text);
			if (chan->WelcomeMsg[0] != 0) SendChat(ID, chan->WelcomeMsg);
		}
		return chan;
	}
	else
	{
		// Did not find the channel
		// Create the channel
		channels[ID] = new Chat(Name, 0);

		channels[ID]->Participants.Append(new Chatter(this, ID));

		DATABASE.ChatDatabase.Append(channels[ID]);

		if (ChatNotify)
		{
			// Make user entering message
			char buffer[64];
			strcpy(buffer, (char*)&player->Name[0]);
			strcat(buffer, " created ");
			strcat(buffer, (char*)&channels[ID]->Name[0]);

			// Route player entering to owner
			SendChat(ID, buffer);
		}

		return channels[ID];
	}
}

void LUserNode::ExitAllChats(bool Notify)
{
	ExitChat(0, Notify);
	ExitChat(1, Notify);
	ExitChat(2, Notify);
	ExitChat(3, Notify);
	ExitChat(4, Notify);
	ExitChat(5, Notify);
	ExitChat(6, Notify);
	ExitChat(7, Notify);
	ExitChat(8, Notify);
	ExitChat(9, Notify);
}

void LUserNode::ExitChat(BYTE ID, bool Notify)
{
	if (channels[ID] == NULL) return;

	if (Notify)
		DATABASE.BroadcastLeaving(this, channels[ID]);

	// Remove player
	_listnode <Chatter> * parse = channels[ID]->Participants.head;

	while (parse)
	{
		if ((parse->item->ID == ID) && (parse->item->Player == this))
		{
			// Clear the chat's reference
			channels[ID]->Participants.Delete(parse, false);

			if (channels[ID]->Participants.Total == 0)
			{
				// Delete the chat if noone is on it
				//DATABASE.ChatDatabase.Delete(channels[ID], false);
				DATABASE.ChatSys.DestroyChat(channels[ID]);
			}

			// Clear the player's reference
			channels[ID] = NULL;

			return;
		}

		parse = parse->next;
	}

	// Shouldn't reach this point
	channels[ID] = NULL;
}


bool LUserNode::JoinSquad(BYTE * Name, BYTE * Password)
{
	_listnode <Squad> * parse = DATABASE.SquadDatabase.head;

	// Search for the given squad name
	while (parse)
	{
		if (_strnicmp((char*)&parse->item->Name[0], (char*)Name, (strlen((char*)Name) > strlen((char*)parse->item->Name) ? strlen((char*)Name) : strlen((char*)parse->item->Name))) == 0)	// Case-independent compare
		{
			if (DATABASE.ValidatePassword(parse->item, Password))
				// Invalid password
				return false;

			// Add the player to this squad
			parse->item->Participants.Append(player);
			return true;
		}

		parse = parse->next;
	}

	// Add a new squad with the player on it
	DATABASE.AddSquad(Name, Password, player);
	return true;
}

void LUserNode::ExitSquad()
{
	Squad * squad = player->squad;

	if (squad)
	{
		squad->Participants.Delete(player, true);

		if (squad->Participants.Total == 0)
		{
			DATABASE.SquadDatabase.Delete(squad, false);
		}
	}
}


// Registration process

RegForm::RegForm(User * NewOwner, char * NewName, char * NewEmail, char * NewComment, LONG NewHardwareID, bool NewNotifyChat, bool NewNotifyBan, bool NewNotifyZone)
{
	Owner = NewOwner;
	memcpy(Name, NewName, 32);
	memcpy(Email, NewEmail, 32);
	memcpy(Comment, NewComment, 128);
	HardwareID = NewHardwareID;
	NotifyChat = NewNotifyChat;
	NotifyBan = NewNotifyBan;
	NotifyZone = NewNotifyZone;
}

void Database::AddReg(User * Owner, char * Name, char * Email, char * Comment, LONG HardwareID)
{
	char b_Name[32];
	char b_Email[32];
	char b_Comment[128];

	memset(b_Name, 0, 32);
	memset(b_Email, 0, 32);
	memset(b_Comment, 0, 128);

	int NameLen = strlen(Name);
	if (NameLen > 31)
		NameLen = 31;
	memcpy(b_Name, Name, NameLen);

	NameLen = strlen(Email);
	if (NameLen > 31)
		NameLen = 31;
	memcpy(b_Email, Email, NameLen);

	NameLen = strlen(Comment);
	if (NameLen > 31)
		NameLen = 31;
	memcpy(b_Comment, Comment, NameLen);

	RegForm * regform = new RegForm(Owner, b_Name, b_Email, b_Comment, HardwareID, false, false, false);
	RegDatabase.Append(regform);
	Owner->regform = regform;
}


// Ban database

Ban::Ban(LONG NewBanID, LONG NewGroupID, LONG NewScoreID, BYTE NewOpLevel, const char * NewName, const char * NewComment, LONG NewIPStart, LONG NewIPEnd, LONG NewMachineID, LONG NewHardwareID, User * NewCreator, time_t NewCreation, time_t NewAccess, time_t NewTimer, LONG NewUserID, BYTE NewDenyBanfree)
{
	ChangeName(NewName);
	ChangeComment(NewComment);
	UserID			= NewUserID;
	IPStart			= NewIPStart;
	IPEnd			= NewIPEnd;
	MachineID		= NewMachineID;
	HardwareID		= NewHardwareID;
	Creator			= NewCreator;
	BanID			= NewBanID;
	GroupID			= NewGroupID;
	ScoreID			= NewScoreID;
	OpLevel			= NewOpLevel;
	CreationDate	= NewCreation;
	LastLogin		= NewAccess;
	Timer			= NewTimer;
	DenyBanfree		= NewDenyBanfree;
}

void Ban::ChangeIP(LONG NewIPStart, LONG NewIPEnd)
{
	IPStart	= NewIPStart;
	IPEnd	= NewIPEnd;
}

void Ban::ChangeMacID(LONG NewMachineID)
{
	MachineID = NewMachineID;
}

void Ban::ChangeHwID(LONG NewHardwareID)
{
	HardwareID = NewHardwareID;
}

void Ban::ChangeComment(const char * NewComment)
{
	memset(Comment, 0, 128);
	strcpy(Comment, NewComment);
}

void Ban::ChangeName(const char * NewName)
{
	memset(Name, 0, 32);
	strcpy(Name, NewName);
}

void Ban::ChangeOwner(User * NewCreator)
{
	Creator = NewCreator;
}

void Ban::UpdateLogin(LONG NewType)
{
	time(&LastLogin);
	LastType = NewType;
}


void Database::BanUser(User * Player, LONG GroupID, LONG ScoreID, BYTE OpLevel, const char * Comment, LONG IPStart, LONG IPEnd, LONG MachineID, LONG HardwareID, LONG Days, User * Creator, BYTE DenyBanfree)
{
	time_t CDate;
	time(&CDate);

	// Generate a ban entry
	Ban * ban = new Ban(++(NextUniversalBanID),				// Generate a new universal ban ID
						GroupID,							// Group ID
						ScoreID,							// Score ID
						OpLevel,							// Operator level
						(char*)&Player->Name[0],			// Player name
						Comment,							// Ban comment
						IPStart,							// IP range start
						IPEnd,								// IP range end
						MachineID,							// Harddrive serial number
						HardwareID,							// Hardware info hash result
						Creator,							// User who made the ban
						CDate,								// Creation date
						CDate,								// Last access date
						CDate + 86400 * Days,				// Days until the ban is removed
						Player->LoadID,						// Player name
						DenyBanfree);						// Deny banfree

	// Add to universal ban database (for saving to disk)
	BanDatabase.Append(ban);

	// Add it to other concerned hosts
	/*if (ScoreID == 0)
	{
		_listnode <Host> * parse = Socket->HostList.head;

		while (parse)
		{
			if ( (GroupID == 0) || (parse->item->GroupID == GroupID) )
				parse->item->BanDatabase.Append(ban);

			parse = parse->next;
		}
	}
	else
	{
		BanDatabase.Append(ban);
	}*/
}

void Database::UnbanUser(Ban * Entry)
{
	BanDatabase.Delete(Entry, false);

	/*if (Entry->ScoreID == 0)
	{
		_listnode <Host> * parse = Socket->HostList.head;

		while (parse)
		{
			if ( (GroupID == 0) || (parse->item->GroupID == GroupID) )
				parse->item->BanDatabase.Delete(Entry, true);

			parse = parse->next;
		}
	}
	else
	{
		BanDatabase.Delete(Entry, true);
	}*/
}

Ban * Database::CheckBan(LONG UserID, LONG IP, LONG MachineID, LONG HardwareID, LONG GroupID, LONG ScoreID)
{
	_listnode <Ban> * parse = BanDatabase.head;

	if (HardwareID == -1)
	{
		while (parse)
		{
			Ban * ban = parse->item;

			if (ban->ScoreID == 0) // Group-wide or net-wide
			{
				if (ban->GroupID != 0)
				{
					if (ban->GroupID != GroupID) goto NextParse;
				}
			}
			else // Zone-wide
			{
				if (ban->ScoreID != ScoreID) goto NextParse;
			}

			if (ban->UserID == UserID)
			{
				ban->UpdateLogin(0);

				return ban;
			}
			else if (ban->MachineID == MachineID)
			{
				ban->UpdateLogin(1);

				return ban;
			}
			else if (CheckRange(IP, ban->IPStart, ban->IPEnd))
			{
				ban->UpdateLogin(2);

				return ban;
			}
			
			NextParse:
			parse = parse->next;
		}
	}
	else
	{
		while (parse)
		{
			Ban * ban = parse->item;

			if (ban->ScoreID == 0) // Group-wide or net-wide
			{
				if (ban->GroupID != 0)
				{
					if (ban->GroupID != GroupID) goto NextParse2;
				}
			}
			else // Zone-wide
			{
				if (ban->ScoreID != ScoreID) goto NextParse2;
			}

			if (ban->UserID == UserID)
			{
				ban->UpdateLogin(0);

				return ban;
			}
			else if (ban->MachineID == MachineID)
			{
				ban->UpdateLogin(1);

				return ban;
			}
			else if (CheckRange(IP, ban->IPStart, ban->IPEnd))
			{
				ban->UpdateLogin(2);

				return ban;
			}
			else if (ban->HardwareID == HardwareID)
			{
				ban->UpdateLogin(3);

				return ban;
			}

			NextParse2:
			parse = parse->next;
		}
	}

	return NULL;
}

Ban * Database::CheckBanID(LONG HardwareID)
{
	_listnode <Ban> * parse = BanDatabase.head;

	while (parse)
	{
		Ban * ban = parse->item;

		if (ban->HardwareID == HardwareID)
		{
			ban->UpdateLogin(3);

			return ban;
		}

		parse = parse->next;
	}

	return NULL;
}


// Source database

_listnode <RecentSource> * Database::FindSource(long IP)
{
	_listnode <RecentSource> * parse = SrcDatabase.head;

	while (parse)
	{
		if (parse->item->IP == IP)
			return parse;

		parse = parse->next;
	}

	return NULL;
}

void Database::DecrementSourceCounters()
{
	_listnode <RecentSource> * parse = SrcDatabase.head;

	while (parse)
	{
		if (parse->item->RemoveCounter())
		{
			_listnode <RecentSource> * next = parse->next;

			SrcDatabase.Delete(parse, false);

			parse = next;
		}
		else
		{
			parse = parse->next;
		}
	}
}


// Chat entry

Chatter::Chatter(LUserNode * NewPlayer, BYTE NewID)
{
	Player	= NewPlayer;
	ID		= NewID;
}


Chat * Database::FindChat(BYTE * Name)
{
	_listnode <Chat> * parse = ChatDatabase.head;

	// Search for the given chat name
	while (parse)
	{
		if (_strnicmp((char*)&parse->item->Name[0], (char*)Name, (strlen((char*)Name) > strlen((char*)&parse->item->Name[0]) ? strlen((char*)Name) : strlen((char*)&parse->item->Name[0])) ) == 0)	// Case-independent compare
			return parse->item;

		parse = parse->next;
	}

	return NULL;
}

ChatPremitList::ChatPremitList(LONG NewUserID)
{
	UserID = NewUserID;
}

Chat::Chat(BYTE * NewName, LONG NewOwner)
{
	memset(Name, 0, 32);
	memcpy(Name, NewName, TrimInt(strlen((char*)NewName), 31));
	memset(WelcomeMsg, 0, 128);
	Owner = NewOwner;
}

bool Chat::PlayerInChat(LUserNode * player)
{
	_listnode <Chatter> * parse = Participants.head;
	while (parse)
	{
		if (parse->item->Player == player) return true;
		parse = parse->next;
	}

	return false;
}

bool Chat::PlayerAllowed(LONG UserID)
{
	if (!Owner) return true;
	if (!NeedPremission) return true;
	if (UserID == Owner) return true;

	_listnode <ChatPremitList> * parse = Allowed.head;

	while (parse)
	{
		if (parse->item->UserID == UserID) return true;
		parse = parse->next;
	}

	return false;
}

// Operator entry

Operator::Operator(User * NewOwner, LONG NewOpID, LONG NewGroupID, LONG NewScoreID, BYTE NewLevel, char * NewPassword)
{
	memset(Password, 0, 32);
	memcpy(Password, NewPassword, TrimInt(strlen(NewPassword), 31));
	Level = NewLevel;

	OpID = NewOpID;
	GroupID = NewGroupID;
	ScoreID = NewScoreID;
	Owner = NewOwner;
	LoginCounter = 0;
}

_listnode <Operator> * Database::FindOp(BYTE * Name)
{
	_listnode <Operator> * parse = OpDatabase.head;

	while (parse)
	{
		if (parse->item->Owner)
			if (_strnicmp((char*)&parse->item->Owner->Name[0], (char*)Name, 19) == 0)	// Case-independent compare
				return parse;

		parse = parse->next;
	}

	return NULL;
}

_listnode <Operator> * Database::FindOp(LONG OpOffset)
{
	_listnode <Operator> * parse = OpDatabase.head;

	while (parse)
	{
		/*Operator * op = parse->item;
		bool Allow = false;

		// Check if op is in the same sector of the biller
		if (Perspective->GroupID == 0)
			Allow = true;
		else if (Perspective->GroupID == op->GroupID)
		{
			if (Perspective->ScoreID == 0)
				Allow = true;
			else if (Perspective->ScoreID == op->ScoreID)
				Allow = true;
		}

		// Check if op has a restricted viewing level
		switch (Perspective->Level)
		{
			case 2:	if (op->Level <= 0)	Allow = false;
				break;
			case 3:	if (op->Level <= 1)	Allow = false;
				break;
			case 4:	if (op->Level <= 1)	Allow = false;
				break;
		};*/

		/*if (Allow)
		{
			Index++;*/
			if (OpOffset == parse->item->OpID)
				return parse;
		//}

		parse = parse->next;
	}

	return NULL;
}


// Zone entry

Zone::Zone(LONG NewGroupID, LONG NewScoreID, char * NewPassword, char * NewBanText)
{
	memset(Password, 0, 32);
	memcpy(Password, NewPassword, 31);
	memset(BanText, 0, 128);
	memcpy(BanText, NewBanText, 127);
	GroupID = NewGroupID;
	ScoreID = NewScoreID;
}

_listnode <Zone> * Database::FindZone(LONG ScoreID)
{
	_listnode <Zone> * parse = ZoneDatabase.head;

	while (parse)
	{
		if (parse->item->ScoreID == ScoreID)
			return parse;

		parse = parse->next;
	}

	return NULL;
}


// Banfree entry

Banfree::Banfree(LONG NewBanfreeID, LONG NewGroupID, LONG NewScoreID, LONG NewOwner, BYTE NewLevel)
{
	BanfreeID = NewBanfreeID;
	GroupID   = NewGroupID;
	ScoreID   = NewScoreID;
	Owner	  = NewOwner;
	Level     = NewLevel;
	Player	  = NULL;
}

GMessage::GMessage(LONG NewUserID, LONG NewFromID, LONG NewMsgID, time_t NewPostTime, BYTE NewSpecial, char * NewMessage)
{
	UserID = NewUserID;
	FromID = NewFromID;
	MsgID = NewMsgID;
	PostTime = NewPostTime;
	Special = NewSpecial;
	memset(Message, 0, 256);
	memcpy(Message, NewMessage, TrimInt(strlen(NewMessage), 255));
}

User * Database::ComputeBanfree(Banfree * banfree)
{
	_listnode <User> * uparse = UserDatabase.head;

	while (uparse)
	{
		User * user = uparse->item;

		if (user->LoadID == banfree->Owner)
		{
			banfree->Player = user;

			return user;
		}

		uparse = uparse->next;
	}

	return NULL;
}


Banfree * Database::CheckBanfree(LONG UserID, LONG ScoreID)
{
	_listnode <Banfree> * parse = BanfreeDatabase.head;

	while (parse)
	{
		Banfree * banfree = parse->item;

		// Check if the player matches
		//if (banfree->Owner == UserID) return banfree;
		if ((banfree->Owner == UserID) && ((banfree->GroupID == 0) || (banfree->ScoreID == ScoreID))) return banfree;

		parse = parse->next;
	}

	return NULL;
}


// Squad entry

Squad::Squad(BYTE * NewName, BYTE * NewPassword, User * NewOwner)
{
	memset(Name, 0, 32);
	memset(Password, 0, 32);
	memcpy(Name, NewName, 31);
	memcpy(Password, NewPassword, 31);

	Owner = NewOwner;
}

void Squad::SetPassword(char * pw)
{
	memset(Password, 0, 32);
	memcpy(Password, pw, min(31, (unsigned long)strlen(pw)));
	HashPassword(Password);
}

_listnode <User> * Squad::FindUser(char * Name)
{
	_listnode <User> * parse = Participants.head;

	while (parse)
	{
		if (!_strnicmp((char*)&parse->item->Name[0], (char*)Name, (strlen((char*)parse->item->Name) > strlen((char*)Name) ? strlen((char*)parse->item->Name) : strlen((char*)Name))))	// Case-independent compare
			return parse;

		parse = parse->next;
	}

	return NULL;
}


Squad * Database::FindSquad(BYTE * Name)
{
	_listnode <Squad> * parse = SquadDatabase.head;

	while (parse)
	{
		if (!_strnicmp((char*)&parse->item->Name[0], (char*)Name, (strlen((char*)parse->item->Name) > strlen((char*)Name) ? strlen((char*)parse->item->Name) : strlen((char*)Name))))	// Case-independent compare
			return (parse->item);

		parse = parse->next;
	}

	return NULL;
}

void Database::AddSquad(BYTE * Name, BYTE * Password, User * Owner)
{
	HashPassword(Password);		// Note: This overwrites the given password

	// Add a new squad
	SquadDatabase.Append(Owner->squad = new Squad(Name, Password, Owner));
	Owner->squad->Participants.Append(Owner);
}

void Database::DeleteSquad(Squad * squad)
{
	// Erase player references to the, soon-to-be non-existant, squad
	_listnode <User> * parse = squad->Participants.head;

	while (parse)
	{
		parse->item->squad = NULL;

		parse = parse->next;
	}

	// Delete the squad
	SquadDatabase.Delete(squad, false);
}

bool Database::ValidatePassword(Squad * squad, BYTE * Password)
{
	HashPassword(Password);		// Note: This overwrites the given password

	if (!strcmp((char*)&squad->Password[0], (char*)Password))
		return true;
	else
		return false;
}


// User entry

User::User(BYTE * NewName, BYTE * NewPassword)
{
	memset(Name, 0, 32);
	memset(Password, 0, 32);
	memcpy(Name, NewName, TrimInt(strlen((char*)NewName), 31));
	memcpy(Password, NewPassword, TrimInt(strlen((char*)NewPassword), 31));
	SetSquad(NULL);
	SetRegForm(NULL);
	IP = 0;
}

bool User::CheckLastAccess(BYTE * NewPassword)
{
	time_t CDate;
	time(&CDate);

	if ((CDate - LastLogin) > (DATABASE.AccountReclaimDelay))
	{
		memset(Password, 0, 32);
		memcpy(Password, NewPassword, TrimInt(strlen((char*)NewPassword), 31));
		HashPassword(Password);

		DATABASE.RegDatabase.Delete(regform, false);
		regform = NULL;

		return true;	// Last access was over (90) days ago
	}
	else
		return false;	// Last access was less than (90) days ago
}

void User::SetCreationDate()
{
	time(&CreationDate);
}

void User::SetLastLogin()
{
	time(&LastLogin);
	TotalLogins++;
}

void User::SetSquad(Squad * NewSquad)
{
	squad = NewSquad;
}

void User::SetBanner(BYTE * NewBanner)
{
	memcpy(Banner, NewBanner, 96);
}

void User::SetRegForm(RegForm * NewRegForm)
{
	regform = NewRegForm;
}

void User::SetBanInfo(LONG NewIP, LONG NewMacID, SHORT NewTZB)
{
	IP				= NewIP;
	MacID			= NewMacID;
	TimeZoneBias	= NewTZB;
}


User * Database::AddUser(BYTE * Name, BYTE * Password, LONG IP, LONG MacID, SHORT TZB)
{
	HashPassword(Password);		// Note: This overwrites the given password

	User * player = new User(Name, Password);
		player->SetCreationDate();
		player->LastLogin = player->CreationDate;
		player->SetBanInfo(IP, MacID, TZB);
		memset(player->Banner, 0, 96);
		player->SetSquad(NULL);
		player->TotalSeconds	= 0;
		player->TotalLogins		= 0;
		player->PassBlock		= 0;
		player->LoadID = (DATABASE.UserDatabase.Total + 1);

	UserDatabase.Append(player);

	return player;
}

_listnode <User> * Database::FindUser(BYTE * Name)
{
	_listnode <User> * parse = UserDatabase.head;

	while (parse)
	{
		if (_strnicmp((char*)&parse->item->Name[0], (char*)Name, 19) == 0)	// Case-independent compare
		{
			return parse;
		}

		parse = parse->next;
	}

	return NULL;
}

_listnode <LUserNode> * Database::FindLUser(BYTE * Name, _listnode <Host> * HostNode, bool UseExactLength)
{
	_listnode <LUserNode> * RetVal;
	while (HostNode)
	{
		RetVal = HostNode->item->Find((char*)&Name[0], UseExactLength);

		if (RetVal) return RetVal;

		HostNode = HostNode->next;
	}

	return NULL;
}

bool Database::ValidatePassword(User * player, BYTE * Password)
{
	HashPassword(Password);		// Note: This overwrites the given password

	if (strcmp((char*)&player->Password[0], (char*)Password) == 0)
		return true;
	else
		return false;
}

bool Database::ValidateBillingPassword(LONG ScoreID, BYTE * Password)
{
	HashPassword(Password);

	if (strcmp((char*)&BillingPassword[0], (char*)Password) == 0)
		return true;
	else
	{
		_listnode <Zone> * parse = ZoneDatabase.head;

		while (parse)
		{
			if (parse->item->ScoreID == ScoreID)
			{
				if (strcmp((char*)&parse->item->Password[0], (char*)Password) == 0)
					return true;
				else
					return false;
			}

			parse = parse->next;
		}

		return false;
	}

}

Operator * Database::ValidateSysopPassword(LUserNode * Player, BYTE * Password)
{
	_listnode <Operator> * parse = OpDatabase.head;

	HashPassword(Password);

	while (parse)
	{
		if (((parse->item->Owner == NULL) || (parse->item->Owner == Player->player)) && (strcmp((char*)&parse->item->Password[0], (char*)Password) == 0))
			return parse->item;

		parse = parse->next;
	}

	return NULL;
}


// Score entry

Score::Score(User * NewOwner, LONG SID)
{
	ScoreID	= SID;
	Owner	= NewOwner;
}

void Score::SetScore(SHORT W, SHORT L, SHORT F, LONG P, LONG EP)
{
	Wins		= W;
	Losses		= L;
	Flags		= F;
	Points		= P;
	EventPoints	= EP;
}

void Database::LoadScores(Host * Zone)
{
	// Load scores
	_listnode <Score> * score_parse = ScoreDatabase.head;

	while (score_parse)
	{
		if (Zone->ScoreID == score_parse->item->ScoreID)
		{
			// Add score to the lazy list
			Zone->ScoreDatabase.Append(score_parse->item);
		}

		score_parse = score_parse->next;
	}

	// Load bans
	/*_listnode <Ban> * ban_parse = DATABASE.BanDatabase.head;

	while (ban_parse)
	{
		Ban * ban = ban_parse->item;

		if (  (ban->GroupID == 0)             ||	// Network-wide ban
			  (Zone->ScoreID == ban->ScoreID) ||	// Ban from zone
			  (Zone->GroupID == ban->GroupID)  )	// Ban from group
		{
			// Add ban to the lazy list
			Zone->BanDatabase.Append(ban);
		}

		ban_parse = ban_parse->next;
	}*/

	// Load banfrees
	/*_listnode <Banfree> * banfree_parse = BanfreeDatabase.head;

	while (banfree_parse)
	{
		Banfree * banfree = banfree_parse->item;

		if (  (banfree->GroupID == 0)             ||	// Network-wide banfree
			  (Zone->ScoreID == banfree->ScoreID) ||	// Banfree from zone
			  (Zone->GroupID == banfree->GroupID)  )	// Banfree from group
		{
			// Add banfree to the lazy list
			Zone->BanfreeDatabase.Append(banfree);
		}

		banfree_parse = banfree_parse->next;
	}*/
}


// Cold (disk) storage

void Database::LoadSettings()
{
	DWORD Counter = GetTickCount();
	DWORD Counter2;
	printf("Reading SUBBILL.INI...");

	// Get path
	char Path[520];
	GetCurrentDirectory(520, Path);
	strcat(Path, "\\SUBBILL.INI");

	// Delete old password and sysop entries
	bool Initializing = false;

	if (BillingPassword)	delete BillingPassword;
	if (SysopPassword)
		delete SysopPassword;
	else
		Initializing = true;

	// Load passwords
	BillingPassword	= new char [32];
	SysopPassword	= new char [32];
	memset(BillingPassword, 0, 32);
	memset(SysopPassword, 0, 32);
	GetPrivateProfileString("Misc", "SysopPassword", "", SysopPassword, 32, Path);
	GetPrivateProfileString("Misc", "BillingPassword", "", BillingPassword, 32, Path);
	HashPassword((BYTE*)SysopPassword);
	HashPassword((BYTE*)BillingPassword);

	if (Initializing)
		OpDatabase.Append(new Operator(NULL, 0, 0, 0, 0, SysopPassword));

	// Load numerical settings
	char buffer[128];

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "KickOutDelay", "60000", buffer, 16, Path);
	KickOutDelay		= atoi(buffer);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Comms", "PacketLossLimit", "30000", buffer, 16, Path);
	PacketLossLimit		= atoi(buffer);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Comms", "PeriodicSave", "1800000", buffer, 16, Path);
	PeriodicSave		= atoi(buffer);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "LowPriority", "0", buffer, 3, Path);
	LowPriority			= (atoi(buffer) ? true : false);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "AllowNegativeMacID", "0", buffer, 3, Path);
	AllowNegativeMacID	= (atoi(buffer) ? true : false);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "AllowNewUsers", "0", buffer, 3, Path);
	AllowNewUsers		= (atoi(buffer) ? true : false);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Comms", "AskNewUsers", "1", buffer, 3, Path);
	AskNewUsers			= (atoi(buffer) ? true : false);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Comms", "DisableHelp", "1", buffer, 3, Path);
	DisableHelp			= (atoi(buffer) ? true : false);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Comms", "EncryptMode", "1", buffer, 3, Path);
	EncryptMode			= (atoi(buffer) ? true : false);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "MaxQueueForLogin", "50", buffer, 16, Path);
	MaxQueueForLogin	= atoi(buffer);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "AccountReclaimDelay", "90", buffer, 16, Path);
	AccountReclaimDelay	= atoi(buffer) * 86400;

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "Port", "900", buffer, 16, Path);
	Port				= atoi(buffer);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "NetworkName", "", buffer, 31, Path);
	memcpy(NetworkName, buffer, 32);

	memset(buffer, 0, 128);
	GetPrivateProfileString("Misc", "NetworkBanText", "", buffer, 127, Path);
	memcpy(NetworkBanText, buffer, 128);

	Counter2 = GetTickCount();
	if (Counter <= Counter2)
	{
		printf("			Elapsed time: %ims\n", (Counter2 - Counter));
	}
	else
	{
		printf("			Elapsed time: (INVALID)\n");
	}
}

void Database::LoadFromDisk()
{
	// Switches
	register bool ReadUser,
				  ReadSquad,
				  ReadScore,
				  ReadBan,
				  ReadOp,
				  ReadZone,
				  ReadReg,
				  ReadBanfree,
				  ReadGMessage,
				  ReadChatOwn;

	// Define buffers
	SavedUser		fdUser;		User		* fpUser;
	SavedSquad		fdSquad;	Squad		* fpSquad;
	SavedScore		fdScore;	Score		* fpScore;
	SavedReg		fdReg;		RegForm		* fpReg;
	SavedBan		fdBan;		Ban			* fpBan;
	SavedZone		fdZone;		Zone		* fpZone;
	SavedOp			fdOp;		Operator	* fpOp;
	SavedBanfree	fdBanfree;	Banfree		* fpBanfree;
	SavedGMessage   fdGMessage; GMessage    * fpGMessage;

	// Pointer network resolution
	_listnode <User>	* score_parse;
	_listnode <Squad>	* squad_parse;
	_listnode <Ban>		* ban_parse;

	// Open for binary read
	register int fnUser		= _open("User.dat",		_O_BINARY, _O_RDONLY);
	register int fnSquad	= _open("Squad.dat",	_O_BINARY, _O_RDONLY);
	register int fnScore	= _open("Score.dat",	_O_BINARY, _O_RDONLY);
	register int fnReg		= _open("User2.dat",	_O_BINARY, _O_RDONLY);
	register int fnBan		= _open("Ban.dat",		_O_BINARY, _O_RDONLY);
	register int fnZone		= _open("Zone.dat",		_O_BINARY, _O_RDONLY);
	register int fnOp		= _open("Op.dat",		_O_BINARY, _O_RDONLY);
	register int fnBanfree	= _open("Banfree.dat",	_O_BINARY, _O_RDONLY);
	register int fnGMessage	= _open("Message.dat",	_O_BINARY, _O_RDONLY);
	register int fnChatOwn	= _open("Chat.dat",		_O_BINARY, _O_RDONLY);

	// Toggle switches
	ReadUser		= (fnUser     == -1)	?	false : true;
	ReadSquad		= (fnSquad    == -1)	?	false : true;
	ReadScore		= (fnScore    == -1)	?	false : true;
	ReadReg			= (fnReg      == -1)	?	false : true;
	ReadBan			= (fnBan      == -1)	?	false : true;
	ReadZone		= (fnZone     == -1)	?	false : true;
	ReadOp			= (fnOp	      == -1)	?	false : true;
	ReadBanfree		= (fnBanfree  == -1)	?	false : true;
	ReadGMessage	= (fnGMessage == -1)	?	false : true;
	ReadChatOwn		= (fnChatOwn  == -1)	?	false : true;

	NextUniversalMsgID = 0;
	NextUniversalOpID = 0;
	NextUniversalBanfreeID = 0;
	NextUniversalBanID = 0;

	DWORD Counter;
	DWORD TCounter = GetTickCount();
	DWORD Counter2;
	DWORD TCounter2;

	if (ReadZone)
	{
		printf("Reading Zone.dat	");

		// Read zone passwords
		Counter = GetTickCount();
		while (ReadZone)
		{
			_read(fnZone, &fdZone.ScoreID, 4);
			_read(fnZone, &fdZone.GroupID, 4);
			_read(fnZone, &fdZone.Password, 32);
			if (_read(fnZone, &fdZone.Bantext, 128))
			{
				// Read some Zone information -> fpZone
				fpZone = new Zone(fdZone.GroupID, fdZone.ScoreID, (char*)&fdZone.Password[0], (char*)&fdZone.Bantext[0]);

				// Add the zone
				ZoneDatabase.Append(fpZone);
			}
			else
			{
				// Close file handle
				_close(fnZone);
				ReadZone = false;
				printf("(%i entries)", ZoneDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadBanfree)
	{
		printf("Reading Banfree.dat	");

		Counter = GetTickCount();

		_read(fnBanfree, &NextUniversalBanfreeID, 4);

		if (NextUniversalBanfreeID < 0)
			NextUniversalBanfreeID = 0;

		while (ReadBanfree)
		{
			_read(fnBanfree, &fdBanfree.BanfreeID, 4);
			_read(fnBanfree, &fdBanfree.OwnerID, 4);
			_read(fnBanfree, &fdBanfree.GroupID, 4);
			_read(fnBanfree, &fdBanfree.ScoreID, 4);
			if (_read(fnBanfree, &fdBanfree.Level, 1))
			{
				// Read some Banfree information -> fpBanfree
				fpBanfree = new Banfree(fdBanfree.BanfreeID, fdBanfree.GroupID, fdBanfree.ScoreID, fdBanfree.OwnerID, fdBanfree.Level);

				// Add the zone
				DATABASE.BanfreeDatabase.Append(fpBanfree);
			}
			else
			{
				// Close file handle
				_close(fnBanfree);
				ReadBanfree = false;
				printf("(%i entries)", DATABASE.BanfreeDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadSquad)
	{
		printf("Reading Squad.dat	");

		// Read squad data
		Counter = GetTickCount();
		while (ReadSquad)
		{
			_read(fnSquad, &fdSquad.EntryID, 4);
			_read(fnSquad, &fdSquad.Name, 32);
			_read(fnSquad, &fdSquad.Password, 32);
			if (_read(fnSquad, &fdSquad.OwnerID, 4))
			{
				// Read some Squad information -> fpSquad
				fpSquad = new Squad((BYTE*)&fdSquad.Name[0], (BYTE*)&fdSquad.Password[0], NULL);
				fpSquad->LoadID		= fdSquad.EntryID;
				fpSquad->OwnerID	= fdSquad.OwnerID;

				// Add the squad
				SquadDatabase.Append(fpSquad);
			}
			else
			{
				// Close file handle
				_close(fnSquad);
				ReadSquad = false;
				printf("(%i entries)", SquadDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadBan)
	{
		printf("Reading Ban.dat		");

		Counter = GetTickCount();

		_read(fnBan, &NextUniversalBanID, 4);

		if (NextUniversalBanID < 0)
			NextUniversalBanID = 0;

		// Read ban data
		while (ReadBan)
		{
			_read(fnBan, &fdBan.EntryID, 4);
			_read(fnBan, &fdBan.UserID, 4);
			_read(fnBan, &fdBan.LastLogin, 4);
			_read(fnBan, &fdBan.CreationDate, 4);
			_read(fnBan, &fdBan.Timer, 4);
			_read(fnBan, &fdBan.IPStart, 4);
			_read(fnBan, &fdBan.IPEnd, 4);
			_read(fnBan, &fdBan.MachineID, 4);
			_read(fnBan, &fdBan.HardwareID, 4);
			_read(fnBan, &fdBan.CreatorID, 4);
			_read(fnBan, &fdBan.ScoreID, 4);
			_read(fnBan, &fdBan.GroupID, 4);
			_read(fnBan, &fdBan.OpLevel, 1);
			_read(fnBan, &fdBan.Name, 32);
			_read(fnBan, &fdBan.Comment, 128);
			if (_read(fnBan, &fdBan.DenyBanfree, 1))
			{
				// Read some Ban information -> fpBan
				fpBan = new Ban(fdBan.EntryID, fdBan.GroupID, fdBan.ScoreID, fdBan.OpLevel, (char*)&fdBan.Name[0], (char*)&fdBan.Comment[0], fdBan.IPStart, fdBan.IPEnd, fdBan.MachineID, fdBan.HardwareID, NULL, fdBan.CreationDate, fdBan.LastLogin, fdBan.Timer, fdBan.UserID, fdBan.DenyBanfree);
				fpBan->OwnerID	= fdBan.CreatorID;

				// Add the block
				DATABASE.BanDatabase.Append(fpBan);
			}
			else
			{
				// Close file handle
				_close(fnBan);
				ReadBan = false;
				printf("(%i entries)", DATABASE.BanDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadUser)
	{
		printf("Reading User.dat	");

		// Read player data
		Counter = GetTickCount();
		while (ReadUser)
		{
			_read(fnUser, &fdUser.EntryID, 4);
			_read(fnUser, &fdUser.Name, 32);
			_read(fnUser, &fdUser.Password, 32);
			_read(fnUser, &fdUser.Banner, 96);
			_read(fnUser, &fdUser.SquadID, 4);
			_read(fnUser, &fdUser.TotalSeconds, 4);
			_read(fnUser, &fdUser.TotalLogins, 4);
			_read(fnUser, &fdUser.LastIP, 4);
			_read(fnUser, &fdUser.MachineID, 4);
			_read(fnUser, &fdUser.LastLoginDate, 4);
			_read(fnUser, &fdUser.CreationDate, 4);
			_read(fnUser, &fdUser.TZB, 2);
			if (_read(fnUser, &fdUser.NoBlock, 2))
			{
				// Read some User information -> fpUser
				fpUser = new User((BYTE*)&fdUser.Name[0], (BYTE*)&fdUser.Password[0]);
				fpUser->LoadID			= fdUser.EntryID;
				fpUser->TotalSeconds	= fdUser.TotalSeconds;
				fpUser->TotalLogins		= fdUser.TotalLogins;
				fpUser->CreationDate	= fdUser.CreationDate;
				fpUser->PassBlock		= fdUser.NoBlock;
				fpUser->LastLogin		= fdUser.LastLoginDate;
				fpUser->SetBanner((BYTE*)&fdUser.Banner[0]);
				fpUser->SetBanInfo(fdUser.LastIP, fdUser.MachineID, fdUser.TZB);

				// Search for bans belonging to the player
				ban_parse = DATABASE.BanDatabase.head;
				while (ban_parse)
				{
					// Set ban's owner
					if (ban_parse->item->OwnerID == fdUser.EntryID)
					{
						ban_parse->item->Creator = fpUser;
					}
					ban_parse = ban_parse->next;
				};

				// Search for the user's squad
				//if (fdUser.SquadID != 0)
				//{
					squad_parse = SquadDatabase.head;
					while (squad_parse)
					{
						// Set player's squad
						if (squad_parse->item->LoadID == fdUser.SquadID)
						{
							fpUser->SetSquad(squad_parse->item);
							squad_parse->item->Participants.Append(fpUser);
						}
						// Set squad's owner
						if (squad_parse->item->OwnerID == fdUser.EntryID)
						{
							squad_parse->item->Owner = fpUser;
						}
						squad_parse = squad_parse->next;
					};

				//	if (!fpUser->squad)
				//	{
				//		printf("(Squad not found(%i:%s))", fdUser.EntryID, fdUser.Name);
				//	}
				//}

				// Add the user
				UserDatabase.Append(fpUser);
			}
			else
			{
				// Close file handle
				_close(fnUser);
				ReadUser = false;
				printf("(%i entries)", UserDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadReg)
	{
		printf("Reading User2.dat	");

		// Read registration data
		Counter = GetTickCount();
		while (ReadReg)
		{
			_read(fnReg, &fdReg.OwnerID, 4);
			_read(fnReg, &fdReg.HardwareID, 4);
			_read(fnReg, &fdReg.Notification, 1);
			_read(fnReg, &fdReg.Name, 32);
			_read(fnReg, &fdReg.Email, 32);
			if (_read(fnReg, &fdReg.Comment, 128))
			{
				// Read some Reg information -> fpReg
				bool NChat	= (fdReg.Notification & 1) ? true : false,
					 NBan	= (fdReg.Notification & 2) ? true : false,
					 NZone	= (fdReg.Notification & 4) ? true : false;
				fpReg = new RegForm(NULL, (char*)&fdReg.Name[0], (char*)&fdReg.Email[0], (char*)&fdReg.Comment[0], fdReg.HardwareID, NChat, NBan, NZone);

				// Search for the user who owns the score
				score_parse = UserDatabase.head;
				while (score_parse)
				{
					if (score_parse->item->LoadID == fdReg.OwnerID)
					{
						score_parse->item->regform = fpReg;
						fpReg->Owner = score_parse->item;
						break;
					}

					score_parse = score_parse->next;
				};

				// Add the info
				if (fpReg->Owner)
				{
					RegDatabase.Append(fpReg);
				}
				else
				{
					delete fpReg;
				}
			}
			else
			{
				// Close file handle
				_close(fnReg);
				ReadReg = false;
				printf("(%i entries)", RegDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadOp)
	{
		printf("Reading Op.dat		");

		Counter = GetTickCount();

		_read(fnOp, &NextUniversalOpID, 4);

		if (NextUniversalOpID < 0)
			NextUniversalOpID = 0;

		// Read operator data
		while (ReadOp)
		{
			_read(fnOp, &fdOp.OpID, 4);
			_read(fnOp, &fdOp.OwnerID, 4);
			_read(fnOp, &fdOp.GroupID, 4);
			_read(fnOp, &fdOp.ScoreID, 4);
			_read(fnOp, &fdOp.OpLevel, 1);
			if (_read(fnOp, &fdOp.Password, 32))
			{
				// Read some Op information -> fpOp
				fpOp = new Operator(NULL, fdOp.OpID, fdOp.GroupID, fdOp.ScoreID, fdOp.OpLevel, (char*)&fdOp.Password[0]);

				// Search for the user who owns the operator entry
				score_parse = UserDatabase.head;
				while (score_parse)
				{
					if (score_parse->item->LoadID == fdOp.OwnerID)
					{
						fpOp->Owner = score_parse->item;
						break;
					}

					score_parse = score_parse->next;
				};

				// Add the info
				if (fpOp->Owner)
				{
					OpDatabase.Append(fpOp);
				}
				else
				{
					delete fpOp;
					//printf("(Operator not found(%i))", fdOp.OwnerID);
				}
			}
			else
			{
				// Close file handle
				_close(fnOp);
				ReadOp = false;
				printf("(%i entries)", OpDatabase.Total - 1);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadGMessage)
	{
		printf("Reading Message.dat	");

		_read(fnGMessage, &NextUniversalMsgID, 4);

		if (NextUniversalMsgID < 0)
			NextUniversalMsgID = 0;

		// Read zone passwords
		Counter = GetTickCount();
		while (ReadGMessage)
		{
			_read(fnGMessage, &fdGMessage.UserID, 4);
			_read(fnGMessage, &fdGMessage.FromID, 4);
			_read(fnGMessage, &fdGMessage.MsgID, 4);
			_read(fnGMessage, &fdGMessage.PostTime, 4);
			_read(fnGMessage, &fdGMessage.Special, 1);
			char MsgBuffer[256];
			if (_read(fnGMessage, &MsgBuffer, 256))
			{
				// Add the message
				score_parse = UserDatabase.head;
				while (score_parse)
				{
					if (score_parse->item->LoadID == fdGMessage.UserID)
					{
						fpGMessage = new GMessage(fdGMessage.UserID, fdGMessage.FromID, fdGMessage.MsgID, fdGMessage.PostTime, fdGMessage.Special, MsgBuffer);
						GMessageDatabase.Append(fpGMessage);
						break;
					}
					score_parse = score_parse->next;
				}
			}
			else
			{
				// Close file handle
				_close(fnGMessage);
				ReadGMessage = false;
				printf("(%i entries)", GMessageDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadScore)
	{
		printf("Reading Score.dat	");

		// Read score data
		Counter = GetTickCount();
		while (ReadScore)
		{
			_read(fnScore, &fdScore.EntryID, 4);
			_read(fnScore, &fdScore.ScoreID, 4);
			_read(fnScore, &fdScore.UserID, 4);
			_read(fnScore, &fdScore.Total, 2);
			_read(fnScore, &fdScore.Wins, 2);
			_read(fnScore, &fdScore.Losses, 2);
			_read(fnScore, &fdScore.Flags, 2);
			_read(fnScore, &fdScore.Points, 4);
			if (_read(fnScore, &fdScore.FlagPoints, 4))
			{
				// Read some Score information -> fpScore
				fpScore = new Score(NULL, fdScore.ScoreID);
				fpScore->SetScore(fdScore.Wins, fdScore.Losses, fdScore.Flags, fdScore.Points, fdScore.FlagPoints);

				// Search for the user who owns the score
				score_parse = UserDatabase.head;
				while (score_parse)
				{
					if (score_parse->item->LoadID == fdScore.UserID)
					{
						fpScore->Owner = score_parse->item;
						break;
					}

					score_parse = score_parse->next;
				};

				if (!fpScore->Owner)
				{
					// Found a corrupted database entry
					//printf("(User not found(%i:%i))", fdScore.EntryID, fdScore.UserID);
					delete fpScore;
				}
				else
				{
					// Add the score
					ScoreDatabase.Append(fpScore);
				}				
			}
			else
			{
				// Close file handle
				_close(fnScore);
				ReadScore = false;
				printf("(%i entries)", ScoreDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	if (ReadChatOwn)
	{
		printf("Reading Chat.dat	");

		Counter = GetTickCount();
		while (ReadChatOwn)
		{
			char Name[32];
			LONG Owner;
			LONG TotalPremitted;
			char NeedPremission;
			char WelcomeMsg[128];
			bool Found = false;
			_read(fnChatOwn, &Name[0], 32);
			_read(fnChatOwn, &Owner, 4);

			score_parse = UserDatabase.head;
			while (score_parse)
			{
				if (score_parse->item->LoadID == Owner)
				{
					time_t CDate;
					time(&CDate);

					if ((CDate - score_parse->item->LastLogin) > (DATABASE.AccountReclaimDelay))
						// Account expired
						Found = false;
					else
						// Account not expired
						Found = true;
					break;
				}
				score_parse = score_parse->next;
			}

			_read(fnChatOwn, &NeedPremission, 1);
			_read(fnChatOwn, &WelcomeMsg, 128);

			if (_read(fnChatOwn, &TotalPremitted, 4))
			{
				if (Found)
				{
					Chat * chat = ChatSys.CreateChat((BYTE*)&Name[0], Owner);
					if (NeedPremission) chat->NeedPremission = true; else chat->NeedPremission = false;
					memcpy(chat->WelcomeMsg, WelcomeMsg, 128);

					if (TotalPremitted && NeedPremission)
					{
						for (LONG i = 1; i <= TotalPremitted; i++)
						{
							LONG UserID;
							bool UsrFound = false;
							_read(fnChatOwn, &UserID, 4);
							score_parse = UserDatabase.head;
							while (score_parse)
							{
								if (score_parse->item->LoadID == UserID)
								{
									UsrFound = true;
									break;
								}
								score_parse = score_parse->next;
							}
	
							if (UsrFound) chat->Allowed.Append(new ChatPremitList(UserID));
						}
					}
				}
			}
			else
			{
				// Close file handle
				_close(fnChatOwn);
				ReadChatOwn = false;
				printf("(%i entries)", ChatDatabase.Total);
				Counter2 = GetTickCount();
				printf("	Elapsed time: %ims\n", (Counter2 - Counter));
			}
		}
	}

	TCounter2 = GetTickCount();
	if (TCounter <= TCounter2)
	{
		printf("Total time: %ims\n", (TCounter2 - TCounter));
	}
}

void PrintError(const char * filename)
{
	char buffer[512];

	strcpy(buffer, filename);
	strcat(buffer, ": ");

	switch(errno)
	{
	case EACCES:
		strcat(buffer, "Tried to open read-only file for writing, or file�s sharing mode does not allow specified operations, or given path is directory");
		break;

	case EEXIST:
		strcat(buffer, "_O_CREAT and _O_EXCL flags specified, but filename already exists");
		break;

	case EINVAL:
		strcat(buffer, "Invalid oflag or pmode argument");
		break;

	case EMFILE:
		strcat(buffer, "No more file handles available (too many open files)");
		break;

	case ENOENT:
		strcat(buffer, "File or path not found");
		break;

	default:
		strcat(buffer, "Unknown error");
	};
}

void Database::SaveToDisk()
{
	// Buffers
	SavedUser		fdUser;
	SavedSquad		fdSquad;
	SavedScore		fdScore;
	SavedBan		fdBan;
	SavedReg		fdReg;
	SavedOp			fdOp;
	SavedZone		fdZone;
	SavedBanfree	fdBanfree;
	SavedGMessage	fdGMessage;

	// Object ID's
	LONG Index;

	// Pointer network resolution
	_listnode <Squad>		* squad_parse;
	_listnode <User>		* user_parse;
	_listnode <Score>		* score_parse;
	_listnode <Ban>			* ban_parse;
	_listnode <RegForm>		* reg_parse;
	_listnode <Operator>	* op_parse;
	_listnode <Zone>		* zone_parse;
	_listnode <Banfree>		* banfree_parse;
	_listnode <GMessage>	* gmessage_parse;
	_listnode <Chat>		* chat_parse;

	DWORD Counter;
	DWORD TCounter = GetTickCount();
	DWORD Counter2;
	DWORD TCounter2;

	char Path[520];
	GetCurrentDirectory(520, Path);
	strcat(Path, "\\SUBBILL.INI");

	WritePrivateProfileString("Misc", "NetBanText", DATABASE.NetworkBanText, Path);

	// Store Squads
	int fnSquad = _open("Squad.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnSquad, 0, SEEK_SET);

	if (fnSquad != -1)
	{
		printf("Writing: Squad.dat");
		Counter = GetTickCount();

		squad_parse	= SquadDatabase.head;
		Index = 0;
		while (squad_parse)
		{
			Squad * squad = squad_parse->item;

			fdSquad.EntryID			= (squad->LoadID	= ++Index);
			memcpy(fdSquad.Name,	  squad->Name,		32);
			memcpy(fdSquad.Password,  squad->Password,	32);
			if (squad->Owner)
			{
				fdSquad.OwnerID		= squad->Owner->LoadID;

				_write(fnSquad, &fdSquad.EntryID, 4);
				_write(fnSquad, &fdSquad.Name, 32);
				_write(fnSquad, &fdSquad.Password, 32);
				_write(fnSquad, &fdSquad.OwnerID, 4);
			}
			else
			{
				printf("(Squad without owner: %i)", Index--);
			}

			squad_parse = squad_parse->next;
		}

		_chsize(fnSquad, 72 * Index);

		_close(fnSquad);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));

	}
	else
	{
		PrintError("Squad.dat");
	}

	// Store Users
	int fnUser = _open("User.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnUser, 0, SEEK_SET);

	if (fnUser != -1)
	{
		printf("Writing: User.dat");
		Counter = GetTickCount();

		user_parse = UserDatabase.head;
		while (user_parse)
		{
			User * player = user_parse->item;

			fdUser.EntryID			= player->LoadID;
			memcpy(fdUser.Name,		player->Name,		32);
			memcpy(fdUser.Password,	player->Password,	32);
			memcpy(fdUser.Banner,	player->Banner,		96);
			fdUser.CreationDate		= player->CreationDate;
			fdUser.LastIP			= player->IP;
			fdUser.LastLoginDate	= player->LastLogin;
			fdUser.MachineID		= player->MacID;
			fdUser.NoBlock			= player->PassBlock;
			fdUser.SquadID			= (player->squad ? player->squad->LoadID : 0);
			fdUser.TotalLogins		= player->TotalLogins;
			fdUser.TotalSeconds		= player->TotalSeconds;
			fdUser.TZB				= player->TimeZoneBias;

			_write(fnUser, &fdUser.EntryID, 4);
			_write(fnUser, &fdUser.Name, 32);
			_write(fnUser, &fdUser.Password, 32);
			_write(fnUser, &fdUser.Banner, 96);
			_write(fnUser, &fdUser.SquadID, 4);
			_write(fnUser, &fdUser.TotalSeconds, 4);
			_write(fnUser, &fdUser.TotalLogins, 4);
			_write(fnUser, &fdUser.LastIP, 4);
			_write(fnUser, &fdUser.MachineID, 4);
			_write(fnUser, &fdUser.LastLoginDate, 4);
			_write(fnUser, &fdUser.CreationDate, 4);
			_write(fnUser, &fdUser.TZB, 2);
			_write(fnUser, &fdUser.NoBlock, 2);

			user_parse = user_parse->next;
		}

		_chsize(fnUser, 196 * DATABASE.UserDatabase.Total);

		_close(fnUser);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("User.dat");
	}

	// Store Scores
	int fnScore = _open("Score.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnScore, 0, SEEK_SET);

	if (fnScore != -1)
	{
		printf("Writing: Score.dat");
		Counter = GetTickCount();

		Index = 0;
		score_parse	= ScoreDatabase.head;
		while (score_parse)
		{
			Score * score = score_parse->item;

			fdScore.EntryID		= ++Index;
			fdScore.ScoreID		= score->ScoreID;
			fdScore.Total		= ScoreDatabase.Total - 1;
			fdScore.Wins		= score->Wins;
			fdScore.Losses		= score->Losses;
			fdScore.Flags		= score->Flags;
			fdScore.Points		= score->Points;
			fdScore.FlagPoints	= score->EventPoints;
			fdScore.UserID		= score->Owner->LoadID;

			_write(fnScore, &fdScore.EntryID, 4);
			_write(fnScore, &fdScore.ScoreID, 4);
			_write(fnScore, &fdScore.UserID, 4);
			_write(fnScore, &fdScore.Total, 2);
			_write(fnScore, &fdScore.Wins, 2);
			_write(fnScore, &fdScore.Losses, 2);
			_write(fnScore, &fdScore.Flags, 2);
			_write(fnScore, &fdScore.Points, 4);
			_write(fnScore, &fdScore.FlagPoints, 4);

			score_parse = score_parse->next;
		}

		_chsize(fnScore, 28 * Index);

		_close(fnScore);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("Score.dat");
	}

	// Store GMessages
	int fnGMessage = _open("Message.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnGMessage, 0, SEEK_SET);

	if (fnGMessage != -1)
	{
		printf("Writing: Message.dat");
		Counter = GetTickCount();
		_write(fnGMessage, &NextUniversalMsgID, 4);

		gmessage_parse = DATABASE.GMessageDatabase.head;
		Index = 0;
		while (gmessage_parse)
		{
			GMessage * gmessage = gmessage_parse->item;

			Index++;
			fdGMessage.UserID = gmessage->UserID;
			fdGMessage.FromID = gmessage->FromID;
			fdGMessage.MsgID = gmessage->MsgID;
			fdGMessage.PostTime = gmessage->PostTime;
			fdGMessage.Special = gmessage->Special;
			memcpy(fdGMessage.Message, gmessage->Message, 256);

			_write(fnGMessage, &fdGMessage.UserID, 4);
			_write(fnGMessage, &fdGMessage.FromID, 4);
			_write(fnGMessage, &fdGMessage.MsgID, 4);
			_write(fnGMessage, &fdGMessage.PostTime, 4);
			_write(fnGMessage, &fdGMessage.Special, 1);
			_write(fnGMessage, &fdGMessage.Message, 256);

			gmessage_parse = gmessage_parse->next;
		}

		_chsize(fnGMessage, 273 * Index + 4);

		_close(fnGMessage);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("Message.dat");
	}

	// Store Bans
	int fnBan = _open("Ban.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnBan, 0, SEEK_SET);

	if (fnBan != -1)
	{
		printf("Writing: Ban.dat");
		Counter = GetTickCount();

		_write(fnBan, &NextUniversalBanID, 4);

		ban_parse	= DATABASE.BanDatabase.head;
		Index = 0;
		while (ban_parse)
		{
			Ban * ban = ban_parse->item;

			Index++;
			fdBan.EntryID		= ban->BanID;
			memcpy(fdBan.Name, ban->Name, 32);
			memcpy(fdBan.Comment, ban->Comment, 128);
			fdBan.LastLogin		= ban->LastLogin;
			fdBan.CreationDate	= ban->CreationDate;
			fdBan.Timer			= ban->Timer;
			fdBan.CreatorID		= (ban->Creator ? ban->Creator->LoadID : 0);
			fdBan.MachineID		= ban->MachineID;
			fdBan.HardwareID	= ban->HardwareID;
			fdBan.IPStart		= ban->IPStart;
			fdBan.IPEnd			= ban->IPEnd;
			fdBan.GroupID		= ban->GroupID;
			fdBan.ScoreID		= ban->ScoreID;
			fdBan.OpLevel		= ban->OpLevel;
			fdBan.UserID		= ban->UserID;

			_write(fnBan, &fdBan.EntryID, 4);
			_write(fnBan, &fdBan.UserID, 4);
			_write(fnBan, &fdBan.LastLogin, 4);
			_write(fnBan, &fdBan.CreationDate, 4);
			_write(fnBan, &fdBan.Timer, 4);
			_write(fnBan, &fdBan.IPStart, 4);
			_write(fnBan, &fdBan.IPEnd, 4);
			_write(fnBan, &fdBan.MachineID, 4);
			_write(fnBan, &fdBan.HardwareID, 4);
			_write(fnBan, &fdBan.CreatorID, 4);
			_write(fnBan, &fdBan.ScoreID, 4);
			_write(fnBan, &fdBan.GroupID, 4);
			_write(fnBan, &fdBan.OpLevel, 1);
			_write(fnBan, &fdBan.Name, 32);
			_write(fnBan, &fdBan.Comment, 128);
			_write(fnBan, &fdBan.DenyBanfree, 1);

			ban_parse = ban_parse->next;
		}

		_chsize(fnBan, 210 * Index + 4);

		_close(fnBan);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("Ban.dat");
	}

	// Store Regs
	int fnReg = _open("User2.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnReg, 0, SEEK_SET);

	if (fnReg != -1)
	{
		printf("Writing: User2.dat");
		Counter = GetTickCount();

		reg_parse	= RegDatabase.head;
		Index = 0;
		while (reg_parse)
		{
			RegForm * reg = reg_parse->item;

			if (reg->Owner)
			{
				Index++;
				fdReg.OwnerID = reg->Owner->LoadID;
				memcpy(fdReg.Name, reg->Name, 32);
				memcpy(fdReg.Email, reg->Email, 32);
				memcpy(fdReg.Comment, reg->Comment, 128);
				fdReg.HardwareID = reg->HardwareID;
				fdReg.Notification = 0;
				if (reg->NotifyChat)
					fdReg.Notification |= 1;
				if (reg->NotifyBan)
					fdReg.Notification |= 2;
				if (reg->NotifyZone)
					fdReg.Notification |= 4;
				_write(fnReg, &fdReg.OwnerID, 4);
				_write(fnReg, &fdReg.HardwareID, 4);
				_write(fnReg, &fdReg.Notification, 1);
				_write(fnReg, &fdReg.Name, 32);
				_write(fnReg, &fdReg.Email, 32);
				_write(fnReg, &fdReg.Comment, 128);
			}
			else
			{
				printf("Ignored: %s\n", reg->Name);
			}

			reg_parse = reg_parse->next;
		}

		_chsize(fnReg, 201 * Index);

		_close(fnReg);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("User2.dat");
	}

	// Store Ops
	int fnOp = _open("Op.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnOp, 0, SEEK_SET);

	if (fnOp != -1)
	{
		printf("Writing: Op.dat");
		Counter = GetTickCount();

		_write(fnOp, &NextUniversalOpID, 4);

		op_parse = OpDatabase.head;
		Index = 0;
		while (op_parse)
		{
			Operator * op = op_parse->item;

			if (op->Owner)
			{
				Index++;
				fdOp.OwnerID = op->Owner->LoadID;
				memcpy(fdOp.Password, op->Password, 32);
				fdOp.OpID    = op->OpID;
				fdOp.OpLevel = op->Level;
				fdOp.ScoreID = op->ScoreID;
				fdOp.GroupID = op->GroupID;

				_write(fnOp, &fdOp.OpID, 4);
				_write(fnOp, &fdOp.OwnerID, 4);
				_write(fnOp, &fdOp.GroupID, 4);
				_write(fnOp, &fdOp.ScoreID, 4);
				_write(fnOp, &fdOp.OpLevel, 1);
				_write(fnOp, &fdOp.Password, 32);
			}

			op_parse = op_parse->next;
		}

		_chsize(fnOp, 4 + 49 * Index);

		_close(fnOp);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("Op.dat");
	}

	// Store Zones
	int fnZone = _open("Zone.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnZone, 0, SEEK_SET);

	if (fnZone != -1)
	{
		printf("Writing: Zone.dat");
		Counter = GetTickCount();

		zone_parse = ZoneDatabase.head;
		Index = 0;
		while (zone_parse)
		{
			Zone * zone = zone_parse->item;

			Index++;
			memcpy(fdZone.Password, zone->Password, 32);
			memcpy(fdZone.Bantext, zone->BanText, 128);
			fdZone.ScoreID = zone->ScoreID;
			fdZone.GroupID = zone->GroupID;

			_write(fnZone, &fdZone.ScoreID, 4);
			_write(fnZone, &fdZone.GroupID, 4);
			_write(fnZone, &fdZone.Password, 32);
			_write(fnZone, &fdZone.Bantext, 128);

			zone_parse = zone_parse->next;
		}

		_chsize(fnZone, 168 * Index);

		_close(fnZone);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("Zone.dat");
	}

	// Store Banfree
	int fnBanfree = _open("Banfree.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnBanfree, 0, SEEK_SET);

	if (fnBanfree != -1)
	{
		printf("Writing: Banfree.dat");
		Counter = GetTickCount();

		_write(fnBanfree, &NextUniversalBanfreeID, 4);

		banfree_parse = DATABASE.BanfreeDatabase.head;
		Index = 0;
		while (banfree_parse)
		{
			Banfree * banfree = banfree_parse->item;

			Index++;
			fdBanfree.BanfreeID = banfree->BanfreeID;
			fdBanfree.ScoreID = banfree->ScoreID;
			fdBanfree.GroupID = banfree->GroupID;
			fdBanfree.OwnerID = banfree->Owner;
			fdBanfree.Level = banfree->Level;

			_write(fnBanfree, &fdBanfree.BanfreeID, 4);
			_write(fnBanfree, &fdBanfree.OwnerID, 4);
			_write(fnBanfree, &fdBanfree.GroupID, 4);
			_write(fnBanfree, &fdBanfree.ScoreID, 4);
			_write(fnBanfree, &fdBanfree.Level, 1);

			banfree_parse = banfree_parse->next;
		}

		_chsize(fnBanfree, 4 + 17 * Index);

		_close(fnBanfree);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("Banfree.dat");
	}

	// Store chats
	int fnChatOwn = _open("Chat.dat", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_lseek(fnChatOwn, 0, SEEK_SET);

	if (fnChatOwn != -1)
	{
		printf("Writing: Chat.dat");
		Counter = GetTickCount();

		chat_parse = ChatDatabase.head;
		Index = 0;
		LONG TotalSize = 0;
		while (chat_parse)
		{
			if (chat_parse->item->Owner)
			{
				Index++;
				_write(fnChatOwn, &chat_parse->item->Name[0], 32);
				TotalSize += 32;
				_write(fnChatOwn, &chat_parse->item->Owner, 4);
				TotalSize += 4;
				char NP;
				if (chat_parse->item->NeedPremission) NP = 1; else NP = 0;
				_write(fnChatOwn, &NP, 1);
				TotalSize += 1;
				_write(fnChatOwn, &chat_parse->item->WelcomeMsg, 128);
				TotalSize += 128;
				_write(fnChatOwn, &chat_parse->item->Allowed.Total, 4);
				TotalSize += 4;
				_listnode <ChatPremitList> * cpl_parse = chat_parse->item->Allowed.head;
				for (LONG i = 1; i <= chat_parse->item->Allowed.Total; i++)
				{
					_write(fnChatOwn, &cpl_parse->item->UserID, 4);
					TotalSize += 4;
					cpl_parse = cpl_parse->next;		// Don't forget to move to the next one...
														// Yes, I forgot...
				}
			}

			chat_parse = chat_parse->next;
		}

		_chsize(fnChatOwn, TotalSize);

		_close(fnChatOwn);
		Counter2 = GetTickCount();
		printf("	 %ims\n", (Counter2 - Counter));
	}
	else
	{
		PrintError("Chat.dat");
	}
	
	printf("Total time:	");
	TCounter2 = GetTickCount();
	printf("	 %ims\n", (TCounter2 - TCounter));
}
