#ifndef ENCRYPTION_H
#define ENCRYPTION_H
#endif


// Packet encryption

void _cdecl Host::InitializeEncryption(LONG Key)
{
	if (SentKey == Key)
	{
		SSEncrTable.SSKey = 0;
		return;
	}

	SSET * table = &SSEncrTable;

	SSEncrTable.SSKey = Key;

	// Do the ASM thing (Assembly rips provided by Coconut Emulator)
	__asm
	{
		// Initialize calculations
		mov ebx, table				// ebx = ptr to SSTable struct
		mov ecx, [ebx]				// ecx = value of SSTable.SSKey
		mov edi, ebx				// edi = ptr to SSTable.SSBuf
		add edi, 4					//    ''
		mov esi, 104h				// esi = LoopCounter (SSTable.SSBuf size=208h by)

SSInit_LoopHead:
		mov eax, 834E0B5Fh			// Calculations
		imul ecx					//   ''
		add edx, ecx				//   ''
		sar edx, 10h				//   ''
		mov eax, edx				//   ''
		shr eax, 1Fh				//   ''
		add edx, eax				//   ''
		lea eax, [edx+8*edx]		//   ''
		shl eax, 3					//   ''
		sub eax, edx				//   ''
		lea eax, [eax+4*eax]		//   ''
		shl eax, 1					//   ''
		sub eax, edx				//   ''
		shl eax, 2					//   ''
		mov ebx, eax				//   ''
		mov eax, ecx				//   ''
		cdq							//   ''
		mov ecx, 1F31Dh				//   ''
		idiv ecx					//   ''
		imul edx, 41A7h				//   ''
		sub edx, ebx				//   ''
		add edx, 7Bh				//   ''
		test edx, edx				//   ''
		mov ecx, edx				//   ''
		jg SSInit_Skip				//   ''
		lea ecx, [edx+7FFFFFFFh]	//   ''

SSInit_Skip:
		mov [edi], cx				// Save 2 by to SSTable.SSBuf
		add edi, 2					// Point to next position in SSTable.SSBuf
		dec esi						// LoopCounter = LoopCounter - 1
		jnz SSInit_LoopHead			// End loop (loop until LoopCounter=0)
	};
}

void Host::Encrypt(BYTE * Message, SHORT Length)
{
	if (SSEncrTable.SSKey == 0) return;

	LONG IV = SSEncrTable.SSKey;
	BYTE buffer[520];
	memset(buffer, 0, 520);

	if (Message[0] == 0)
		memcpy(buffer, &Message[2], Length - 2);
	else
		memcpy(buffer, &Message[1], Length - 1);

	for (SHORT Counter = 0; Counter < 520; Counter += 4)
	{
		IV = GetLong(buffer, Counter) ^ GetLong(SSEncrTable.SSBuf, Counter) ^ IV;

		*(long*)&buffer[Counter] = IV;
	}

	if (Message[0] == 0)
		memcpy(&Message[2], buffer, Length - 2);
	else
		memcpy(&Message[1], buffer, Length - 1);
}

void Host::Decrypt(BYTE * Message, SHORT Length)
{
	if (SSEncrTable.SSKey == 0) return;

	LONG IV = SSEncrTable.SSKey;
	LONG ESI;
	LONG EDX;
	BYTE buffer[520];
	memset(buffer, 0, 520);

	if (Message[0] == 0)
		memcpy(buffer, &Message[2], Length - 2);
	else
		memcpy(buffer, &Message[1], Length - 1);

	for (SHORT Counter = 0; Counter < 520; Counter += 4)
	{
		EDX = GetLong(buffer, Counter);
		ESI = GetLong(SSEncrTable.SSBuf, Counter) ^ IV ^ EDX;
		IV = EDX;
		*(long*)&buffer[Counter] = ESI;
	}

	if (Message[0] == 0)
		memcpy(&Message[2], buffer, Length - 2);
	else
		memcpy(&Message[1], buffer, Length - 1);
}


// One-way encryption algorithms

void HashPassword(BYTE * Password)
{
	size_t StrLen = strlen((char*)Password);
	SHORT Factor = 0;
	BYTE Char;

	for (size_t L = 0; L < StrLen; L++)
		Factor ^= Password[L];

	for (L = 0; L < StrLen; L++)
	{
		Char = (Password[L] ^= Factor);
		Factor = (Factor ^ (Char << (Char & 3))) & 255;
		if (Password[L] == 0)
			Password[L] = 0xED;
	}
}

unsigned long HashModemInfo(BYTE * Info)
{
    unsigned long h = 0, g;

	for (short i = 0; i < 608; i++)
    {
        h = ( h << 4 ) + *Info++;
        if ( g = h & 0xF0000000 )
            h ^= g >> 24;
        h &= ~g;
    }

    return h;
}
