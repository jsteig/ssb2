1.11j
	[Catid] Fixed a large security-related bug.  Cannot be specific because some people will be using the old version..
		For this reason, the source code has not been updated.  I'll release the actual source ASAP.

1.11i
	*Fixed a bug with -b switch on bans

1.11f
	*Removed squad and chat list showing in biller window

1.11e
	*Fixed a little typo in ?listban
	*Changed ?listban info in ?man
	*Changed some ?man ordering again
	*Fixed ?resetscore crash bug

1.11d
	*Changed ?broadcast and **message do %2 sound
	*Fixed bug that caused players to be locked out from some chats without a reason(May bring extra trouble later, but not likely. Not tested, so I have no idea if chats still work... ;))
	*Changed ?man ordering
	*Added examples to ?man for all the brainless n00bz

1.11c
	*Fixed some spelling
	*Fixed ?resetscore not showing up in ?man
	*Fixed empty 0: chat messages when heavy connect flood is triggered
	*Changed flood limiter from 5/9 to 10/20 (Light/Heavy flood)
	*Fixed HUGE bug with saving chat premitted list(Must stop the 1am coding...)

1.11b
	*Added GroupID showing for offline zones in ?listzone
	*Added notification for when a player enteres a chat hes already on(To help find a bug)

1.11a
	*Changed ?resetscore
	*Added ?networkreset
	*Changed login flood checks

1.11
	*Fixed bug in ?squadownlist
	*Changed some text in ?man listzone
	*Changed **message so it always sends net-wide
	*Removed "Group-wide banfree" from ?listbanfree description
		There is no way to banfree group-wide, so i dont know how it got in there
	*Added max comment lengths to ?man of ?commentban and ?details
	*Added premission checking to most L1 commands
	*Fixed duplicate banfree finding bug
	*Added ?c-grant
	*Added "staff chats not supported" message
	*Added ?broadcast
	*Added OpID and BanfreeID	***op.dat and banfree.dat must be deleted before running new version!***
	*Changed commands to use OpID and BanfreeID

1.10g
	*Fixed ?man display

1.10f
	*Fixed ?squadownlist

1.10e
	*Added "You are not on any chats" message
	*Fixed some stuff in ?man

1.10d
	*Fixed an error message

1.10c
	*Fixed TZB check
	*Added ?warn command

1.10b
	*Fixed bug in ?commentban
	*Fixed bug that made all bans netwide
	*Changed ban comment length to 128. Comments were way too short. ***BAN FILE INCOMPATIBLE WITH OLDER VERSIONS(DELETE IT)***
	*Removed ?networkreset
	*Changed ?groupreset to ?resetscore
	*Added ?resetscore to ?man

1.10a
	*Fixed 3 bugs in ?listban and ?listbanfree

1.10
	*Changed ?version to a public command
	*Changed chat system so chats get unowned when the owner account expires(AccountReclaimDelay)
	*Added ?squadonline command
	*Added ?squadownlist command

1.9g
	*Added ?c-needp command

1.9f
	*Fixed ?c-release crash bug

1.9e
	*Added ?c-kick to ?man
	*Added explanation on how you can read your messages....

1.9d
	*Changed it so that L2 or higher level operators can release chats of other players
	*Changed some ?man formatting
	*Added ?c-kick command

1.9c
	*Fixed ?top10 ratios
	*Fixed a bug with chats

1.9b
	*Added ?c-welcome command
	*Added chat welcome messages
	*Changed chat owning so that you can grab a chat when someone is in it
	*Added chat owner name to "You are not allowed on chat X" message

1.9a
	*Chagned ?top10 a bit. Also made columns aligned
	*Changed ?top10 to display for current zone only. Must stop coding at midnight...

1.9
	*Removed ?nick for security reasons	
	*Changed some text formatting and text
	*Added ?top10 command
	*Added ?msgclear command
	*Disallowed commas in names! <<<<<<<<<<
	*Added checking for invalid chat names
	*Removed ?addchat and ?leavechat
	*Added some checks to message loading
	*Added chat owning system

1.8h
	*Changed some ?info behaviour
	*Fixed infinite loop bug in ?info #UserID

1.8g
	*Added ?dop so u can use it if too lazy to type ?logout

1.8f
	*Fixed ?notifyban On/Off

1.8e
	*Fixed ?listban BanID not working sometimes
	*Fixed ?nick bug

1.8d
	*Fixed ?ban -b crash bug
	*Added -b switch display in ?listban
	*Changed ?listban to be faster when showing specific ban info

1.8c
	*Added fixes from Catid's release 10
		*Memory leak during player login
		*Invalid chat message crash

1.8b
	*Added automatic new message check. Removed the one from ?man
	*Removed a debug command that made biller say "BOO" to you sometimes :P
	*Added warning when a lower level biller op tries to ?kick you

1.8a
	*Changed operator level names. Old ones were too boring and inaccurate
	*Changed some text
	*Changed group-wide banning
	*Chagned "Player is already banned" error to a notification
	*Added viewing of single ban info
	*Added ?ver command

1.7
	*Changed some text formatting
	*Added remote banning
	*Fixed zone notify bug

1.6
	*Changed ?kick to work network-wide
	*Added network name specification in server.ini
		Misc:NetworkName=
		Used in network-wide ban messages. Max 31 characters
	*Added network wide ban text in server.ini  (Changeable with ?netbantext(L0))
		Misc:NetworkBanText=
		Used in network-wide ban messages. Max 127 characters
	*Added additional protection against hacked TZBs. Values taken from Windows 98 First Edition,
		so other Windowses might generate false alerts. Tell me if that happens. Some bots
		might also have problems(MERVBot and all its forms that use a -30 TZB. Easy fix,
		just remove -30 or make it divisible with 60).
	*Fixed ban notify bug
	*Fixed the fix for the ban notify bug
	*Fixed bug that caused all bans to be net-wide

1.5h
	*Fixed infinite loop bug in ?ban

1.5g
	*Fixed the new messages fix

1.5f
	*Removed a debug command that i forgot to remove
	*Fixed new message notification

1.5e
	*Added new message notification to ?man
	*Changed some text formatting

1.5d
	*Fixed bug that allowed some hacked clients to connect
	*Fixed bug that added all banfrees network-wide

1.5c
	*Changed some text formatting
	*Fixed ?man banfree, ?man listban and ?man listbanfree crash bugs

1.5b
	*Added ?msgview command
	*Added full inbox warning
	*Changed maximum message length to 255

1.5a
	*Fixed infinite loop bug in ?banfree

1.5
	*Added messaging system
	*Fixed ?kick

1.4c
	*Changed ?dop to ?logout
	*Changed help
	*Changed some commands to report status, not toggle when no parameters given
	*Added registration form warning to ?man

1.4b
	*Added ?banfree -* switch
	*Fixed banfree checking

1.4a
	*Fixed banfreeing players
	*Fixed banfree levels
	*Fixed banfree saving
	*Fixed ban saving
	*Fixed ?ban -b switch
	*Changed it so you cannot ban the same player multiple times
	*Changed it so you cannot banfree the same player multiple times

1.4
	*Changed database to be a little faster
	*Changed all ban kickout times to 10 seconds
	*Added ?kick command

1.3
	*Added network-wide ?listban
	*Added network-wide ?listbanfree
	*Added ?banfree levels
	*Chagned banfree file format
	*Changed ban file format
	*Added denying banfrees for banned players

1.2 and smaller
	*UNDOCUMENTED