#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#endif


/*
 * Linked List.H - Reinvented Wheel (tm) edition of the oh-so-cliche Linked List & String objects
 * 
 * Catid@pacbell.net (2/6/01 - 2/7/01)
 *
 */


#include <stdlib.h>


//Basic data types

//typedef unsigned short SHORT;
//typedef unsigned long LONG;
//typedef unsigned char BYTE;


#define TEMPLATE template <class AnonymousStruct>


TEMPLATE class _listnode;								// Linked list node
TEMPLATE class _linkedlist;								// Linked list

TEMPLATE class _jumptable;								// Jump table

TEMPLATE class _referencetable;							// Pointer array

class _string;											// String

class _trash;											// Keeps track of allocated memory


//Basic linked list node

TEMPLATE class _listnode
{
public:
	AnonymousStruct * item;
	_listnode		* next;
	_listnode		* last;

	inline void Delete();		// Beware of the garbage dereference

	inline _listnode(AnonymousStruct *);
	inline ~_listnode();
};

TEMPLATE _listnode <AnonymousStruct>::_listnode (AnonymousStruct * nItem)
{
	item = nItem;

	next = NULL;
	last = NULL;
}

TEMPLATE _listnode <AnonymousStruct>::~_listnode()
{
	if (item) delete item;
}

TEMPLATE void _listnode <AnonymousStruct>::Delete()
{
	if (next) next->last = last;
	if (last) last->next = next;

	delete this;
}


//Basic linked list

TEMPLATE class _linkedlist
{
public:
	_listnode <AnonymousStruct> * head;
	_listnode <AnonymousStruct> * tail;

	inline void Prepend(AnonymousStruct *);
	inline void Append(AnonymousStruct *);
	inline void Delete(_listnode <AnonymousStruct> *, bool);
	inline void Delete(AnonymousStruct *, bool);
	inline void InsertAfter(_listnode <AnonymousStruct> *, AnonymousStruct *);
	inline void InsertBefore(_listnode <AnonymousStruct> *, AnonymousStruct *);
	inline void Clear(bool);

	LONG Total;

	inline _linkedlist();
	inline ~_linkedlist();
};

TEMPLATE _linkedlist <AnonymousStruct>::_linkedlist()
{
	Total	= 0;

	head	= NULL;
	tail	= NULL;
}

TEMPLATE _linkedlist <AnonymousStruct>::~_linkedlist()
{
	Clear(false);
}

TEMPLATE void _linkedlist <AnonymousStruct>::Clear(bool Nullify)
{
	if (head)
	{
		_listnode <AnonymousStruct> * parse = head;

		Total = 0;

		if (Nullify)
		{
			while (parse)
			{
				_listnode <AnonymousStruct> * next = parse;

				parse = parse->next;

				next->item = NULL;
				delete next;
			}
		}
		else
		{
			while (parse)
			{
				_listnode <AnonymousStruct> * next = parse;

				parse = parse->next;

				delete next;
			}
		}
	}

	head = tail = NULL;
}

TEMPLATE void _linkedlist <AnonymousStruct>::Prepend(AnonymousStruct * nItem)
{
	_listnode <AnonymousStruct> * new_head = new _listnode <AnonymousStruct> (nItem);

	new_head->last = NULL;
	new_head->next = head;

	if (tail == NULL)
		tail = new_head;
	else
		head->last = new_head;

	head = new_head;

	Total++;
}

TEMPLATE void _linkedlist <AnonymousStruct>::Append(AnonymousStruct * nItem)
{
	_listnode <AnonymousStruct> * new_tail = new _listnode <AnonymousStruct> (nItem);

	new_tail->last = tail;
	new_tail->next = NULL;

	if (head == NULL)
		head = new_tail;
	else
		tail->next = new_tail;

	tail = new_tail;

	Total++;
}

TEMPLATE void _linkedlist <AnonymousStruct>::InsertAfter(_listnode <AnonymousStruct> * node, AnonymousStruct * nItem)
{
	if (node == tail)
	{
		Append(nItem);
	}
	else
	{
		_listnode <AnonymousStruct> * lastnode = node;
		_listnode <AnonymousStruct> * nextnode = node->next;
		_listnode <AnonymousStruct> * new_node = new _listnode <AnonymousStruct> (nItem);

		new_node->last = lastnode;
		new_node->next = nextnode;
		if (lastnode) lastnode->next = new_node;
		if (nextnode) nextnode->last = new_node;

		Total++;
	}
}

TEMPLATE void _linkedlist <AnonymousStruct>::InsertBefore(_listnode <AnonymousStruct> * node, AnonymousStruct * nItem)
{
	if (node == head)
	{
		Prepend(nItem);
	}
	else
	{
		_listnode <AnonymousStruct> * lastnode = node->last;
		_listnode <AnonymousStruct> * nextnode = node;
		_listnode <AnonymousStruct> * new_node = new _listnode <AnonymousStruct> (nItem);

		new_node->last = lastnode;
		new_node->next = nextnode;
		if (lastnode) lastnode->next = new_node;
		if (nextnode) nextnode->last = new_node;

		Total++;
	}
}

TEMPLATE void _linkedlist <AnonymousStruct>::Delete(_listnode <AnonymousStruct> * node, bool Nullify)
{
	if (tail == node)
	{
		tail = tail->last;
		if (tail) tail->next = NULL;
	}

	if (head == node)
	{
		head = head->next;
		if (head) head->last = NULL;
	}

	if (Nullify)
		node->item = NULL;

	node->Delete();

	Total--;
}

TEMPLATE void _linkedlist <AnonymousStruct>::Delete(AnonymousStruct * item, bool Nullify)
{
	_listnode <AnonymousStruct> * parse = head;

	while (parse)
	{
		if (parse->item == item)
		{
			Delete(parse, Nullify);
			return;
		}

		parse = parse->next;
	}
}


//Basic jump table

TEMPLATE class _jumptable
{
	bool (*RPC [256])(AnonymousStruct *);		// "Remote Procedure Call" implementation

public:
	inline _jumptable();

	inline void ClearHandlers();
	inline void RegisterHandler(unsigned char, bool (*)(AnonymousStruct *));
	inline void UnregisterHandler(unsigned char);
	inline void CallHandler(unsigned char, AnonymousStruct *);
};

TEMPLATE _jumptable <AnonymousStruct>::_jumptable()
{
	ClearHandlers();
}

TEMPLATE void _jumptable <AnonymousStruct>::ClearHandlers()
{
	memset(RPC, 0, 1024);
}

TEMPLATE void _jumptable <AnonymousStruct>::RegisterHandler(unsigned char Type, bool (*function)(AnonymousStruct *))
{
	RPC[Type] = function;
}

TEMPLATE void _jumptable <AnonymousStruct>::UnregisterHandler(unsigned char Type)
{
	RPC[Type] = NULL;
}

TEMPLATE void _jumptable <AnonymousStruct>::CallHandler(unsigned char Type, AnonymousStruct * params)
{
	if (RPC[Type])
		if ( RPC[Type](params) )
			delete params;
}

//Basic pointer array

TEMPLATE class _referencetable
{
	AnonymousStruct * LIST[256];

public:
	inline _referencetable();

	inline void ClearList();
	inline void AddItem(unsigned char, AnonymousStruct *);
	inline void NukeItem(unsigned char);
	inline AnonymousStruct * Get(unsigned char);
};

TEMPLATE inline _referencetable <AnonymousStruct>::_referencetable()
{
	ClearList();
}

TEMPLATE inline void _referencetable <AnonymousStruct>::ClearList()
{
	for (unsigned short i = 0; i < 256; i++)
		LIST[i] = NULL;
}

TEMPLATE inline void _referencetable <AnonymousStruct>::AddItem(unsigned char Type, AnonymousStruct * Data)
{
	LIST[Type] = Data;
}

TEMPLATE inline void _referencetable <AnonymousStruct>::NukeItem(unsigned char Type)
{
	LIST[Type] = NULL;
}

TEMPLATE inline AnonymousStruct * _referencetable <AnonymousStruct>::Get(unsigned char Type)
{
	return LIST[Type];
}


//Basic string implementation

class _string
{
public:
	bool operator==(const char *);					// l-string = r-string;

	void operator=(_string);						// l-string = r-string;
	void operator=(const char *);					// l-string = r-char array;
	void operator=(const int);						// l-string = r-int;
	void operator=(const long);						// l-string = r-long;

	void operator+=(_string);						// l-string += r-string;
	void operator+=(const char *);					// l-string += r-char array;
	void operator+=(const int);						// l-string += r-int;
	void operator+=(const long);					// l-string += r-long;

	_string operator+(_string);						// l-string + r-string;
	_string operator+(const char *);				// l-string + r-char array;
	_string operator+(const int);					// l-string + r-int;
	_string operator+(const long);					// l-string + r-long;

	char			* Text;							// String text
	LONG			  Length;						// Length of string

	_string(const char *);							// Initialize the string to something
	_string();										// Initialize the string to nothing
	~_string();										// Delete the string

	_string Mid(unsigned long, unsigned long);		// Extract portion of string
	_string Left(unsigned long);					// Extract portion of string
};

_string::_string(const char * Message)
{
	Text = new char[ (Length = strlen(Message)) + 1 ];
	memcpy(Text, Message, Length);
	Text[Length] = 0;
}

_string::_string()
{
	Length	= 0;
	Text	= new char[1];
	Text[0] = 0;
}

_string::~_string()
{
	delete Text;
}

bool _string::operator==(const char * rvalue)
{
	int index = 0;

	while(Text[index] == rvalue[index])
	{
		if (!Text[index])
			return true;

		index++;
	}

	return false;
}

void _string::operator=(_string rvalue)
{
	delete Text;

	Text = new char[ (Length = rvalue.Length) + 1 ];
	memcpy(Text, rvalue.Text, rvalue.Length);
}

void _string::operator=(const char * Message)
{
	delete Text;

	Text = new char[ (Length = strlen(Message)) + 1 ];
	memcpy(Text, Message, Length);
	Text[Length] = 0;
}

void _string::operator=(const int Number)
{
	char Message[50];
	itoa(Number, Message, 10);

	delete Text;

	Text = new char[ (Length = strlen(Message)) + 1 ];
	memcpy(Text, Message, Length + 1);
}

void _string::operator=(const long Number)
{
	char Message[50];
	itoa(Number, Message, 10);

	delete Text;

	Text = new char[ (Length = strlen(Message)) + 1 ];
	memcpy(Text, Message, Length + 1);
}

void _string::operator+=(_string rvalue)
{
	char * new_pointer;

	new_pointer = new char[ (Length + rvalue.Length) + 1 ];
	memcpy(new_pointer, Text, Length);
	memcpy(&new_pointer[Length], rvalue.Text, rvalue.Length + 1);

	delete Text;

	Text	 =	new_pointer;
	Length	+=	rvalue.Length;
}

void _string::operator+=(const char * Message)
{
	char * new_pointer;

	new_pointer = new char[ (Length + strlen(Message)) + 1 ];
	memcpy(new_pointer, Text, Length);
	memcpy(&new_pointer[Length], Message, strlen(Message));


	delete Text;

	Text		 = new_pointer;
	Length		+= strlen(Message);
	Text[Length] = 0;
}

void _string::operator+=(const int Number)
{
	char Message[50];
	itoa(Number, Message, 10);

	char * new_pointer;

	new_pointer = new char[ (Length + strlen(Message)) + 1 ];
	memcpy(new_pointer, Text, Length);
	memcpy(&new_pointer[Length], Message, strlen(Message) + 1);

	delete Text;

	Text	 =	new_pointer;
	Length	+=	strlen(Message);
}

void _string::operator+=(const long Number)
{
	char Message[50];
	itoa(Number, Message, 10);

	char * new_pointer;

	new_pointer = new char[ (Length + strlen(Message)) + 1 ];
	memcpy(new_pointer, Text, Length);
	memcpy(&new_pointer[Length], Message, strlen(Message) + 1);

	delete Text;

	Text	 =	new_pointer;
	Length	+=	strlen(Message);
}

_string _string::operator+(_string rvalue)
{
	_string S;

	S.Text = new char[ (Length + rvalue.Length) + 1 ];
	memcpy(S.Text, Text, Length);
	memcpy(&S.Text[Length], rvalue.Text, rvalue.Length + 1);

	S.Length =	Length + rvalue.Length;

	return S;
}

_string _string::operator+(const char * Message)
{
	_string S;

	S.Text = new char[ (S.Length = Length + strlen(Message)) + 1 ];
	memcpy(S.Text, Text, Length);
	memcpy(&S.Text[Length], Message, strlen(Message) + 1);

	return S;
}

_string _string::operator+(const int Number)
{
	char Message[50];
	itoa(Number, Message, 10);

	_string S;

	S.Text = new char[ (S.Length = Length + strlen(Message)) + 1 ];
	memcpy(S.Text, Text, Length);
	memcpy(&S.Text[Length], Message, strlen(Message) + 1);

	return S;
}

_string _string::operator+(const long Number)
{
	char Message[50];
	itoa(Number, Message, 10);

	_string S;

	S.Text = new char[ (S.Length = Length + strlen(Message)) + 1 ];
	memcpy(S.Text, Text, Length);
	memcpy(&S.Text[Length], Message, strlen(Message) + 1);

	return S;
}

_string _string::Mid(unsigned long Offset, unsigned long ResultLength)
{
	_string result;

	result.Text = new char[ (result.Length = ResultLength) + 1 ];
	memcpy(result.Text, &Text[Offset], ResultLength);
	result.Text[result.Length] = 0;

	return result;
}

_string _string::Left(unsigned long ResultLength)
{
	_string result;

	result.Text = new char[ (result.Length = ResultLength) + 1 ];
	memcpy(result.Text, Text, ResultLength);
	result.Text[result.Length] = 0;

	return result;
}


//Basic garbage collector

class _trash
{
	_linkedlist <void> garbage;

public:
	void Add(void * Trash)
	{
		garbage.Append(Trash);	// Use the built-in cleanup mechanism in _linkedlist
	}
};
