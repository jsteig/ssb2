#ifndef RPC_H
#define RPC_H
#endif


// Handlers

bool HandleUnknownSpecial	(PacketStruct * Packet)
{
	//printf("Unhandled special packet(%i)\n", (int)Packet->Message[1]);

	return true;
}

bool HandleEncryptRequest	(PacketStruct * Packet)
{
	if ((Packet->Length != 8) || GetLong(Packet->Message, 2) == 0)
	{
		// Display connection request
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		short Port = htons((*(sockaddr_in*)&(Packet->Source->Remote)).sin_port);
		printf("Malformed connection request from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
																			(int)(((BYTE*)&IP)[1]),
																			(int)(((BYTE*)&IP)[2]),
																			(int)(((BYTE*)&IP)[3]),
																			Port);

		Packet->Source->Disconnect(false);

		return true;
	}
	else
	{
		// I am a server
		Packet->Source->SetMode(false, Packet->Source->KickOffLimit);

		// Display connection request
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		short Port = htons((*(sockaddr_in*)&(Packet->Source->Remote)).sin_port);
		printf("Connection request from %i.%i.%i.%i:%i\n",	(int)(((BYTE*)&IP)[0]),
															(int)(((BYTE*)&IP)[1]),
															(int)(((BYTE*)&IP)[2]),
															(int)(((BYTE*)&IP)[3]),
															Port);
	}

	// Send SessionKey
	BYTE buffer[6];
	LONG Key;

	if (DATABASE.EncryptMode)
	{
		Key = ~GetLong(Packet->Message, 2) + 1;
		MakeSessionKey(buffer, Key);
	}
	else
	{
		Key = 0;
		MakeSessionKey(buffer, GetLong(Packet->Message, 2));
	}

	Packet->Source->Send(buffer, 6, false);
	Packet->Source->SetKey(Key);

	// Enable a few of the other protocol components
	Packet->Source->SpecialRouter.RegisterHandler(3,	HandleReliable);
	Packet->Source->SpecialRouter.RegisterHandler(4,	HandleACK);
	Packet->Source->SpecialRouter.RegisterHandler(5,	HandleSyncRequest);
	Packet->Source->SpecialRouter.RegisterHandler(6,	HandleSyncResponse);
	Packet->Source->SpecialRouter.RegisterHandler(8,	HandleSmallChunkBody);
	Packet->Source->SpecialRouter.RegisterHandler(9,	HandleSmallChunkTail);
	Packet->Source->SpecialRouter.RegisterHandler(10,	HandleBigChunk);
	Packet->Source->SpecialRouter.RegisterHandler(14,	HandleCluster);

	Packet->Source->SetHandler(2,	HandleLogin);

	return true;
}

bool HandleEncryptResponse	(PacketStruct * Packet)
{
	// I am a client
	Packet->Source->SetMode(true, Packet->Source->KickOffLimit);

	// Display connection accept
	long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
	int Port = htons((*(sockaddr_in*)&Packet->Source->Remote).sin_port);
	printf("Connection accepted to %i.%i.%i.%i:%i\n",	(int)(((BYTE*)&IP)[0]),
														(int)(((BYTE*)&IP)[1]),
														(int)(((BYTE*)&IP)[2]),
														(int)(((BYTE*)&IP)[3]),
														Port);

	// Set SessionKey
	if (Packet->Source->SSEncrTable.SSKey == GetLong(Packet->Message, 2))
	{
		Packet->Source->InitializeEncryption(0);
		printf("Session encryption: Disabled\n");
	}
	else
	{
		if ((~Packet->Source->SentKey + 1) != GetLong(Packet->Message, 2))
		{
			printf("Warning: Malformed encryption key.\n");
		}

		Packet->Source->InitializeEncryption(GetLong(Packet->Message, 2));
		printf("Session encryption: Enabled\n");
	}

	return true;
}

bool HandleReliable			(PacketStruct * Packet)
{
	if (Packet->Length >= 7)
	{
		return (Packet->Source->CheckBacklog(Packet));
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed reliable header from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
																		(int)(((BYTE*)&IP)[1]),
																		(int)(((BYTE*)&IP)[2]),
																		(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);

		return true;
	}
}

bool HandleACK				(PacketStruct * Packet)
{
	if (Packet->Length == 6)
	{
		Packet->Source->CheckPostlog(GetLong(Packet->Message, 2));
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed ACK from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
															(int)(((BYTE*)&IP)[1]),
															(int)(((BYTE*)&IP)[2]),
															(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);
	}

	return true;
}

bool HandleSyncRequest		(PacketStruct * Packet)
{
	if (Packet->Length >= 6)
	{
		// Send a sync response
		BYTE buffer[9];
		MakeSyncResponse(buffer, GetLong(Packet->Message, 2));
		Packet->Source->Send(buffer, 9, false);
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed sync request from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
																	(int)(((BYTE*)&IP)[1]),
																	(int)(((BYTE*)&IP)[2]),
																	(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);
	}

	return true;
}

bool HandleSyncResponse		(PacketStruct * Packet)
{
	if (Packet->Length >= 10)
	{
		// Get sync
		Packet->Source->DiffTime = GetLong(Packet->Message, 6) - GetLong(Packet->Message, 2);
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed sync response from %i.%i.%i.%i\n",		(int)(((BYTE*)&IP)[0]),
																	(int)(((BYTE*)&IP)[1]),
																	(int)(((BYTE*)&IP)[2]),
																	(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);
	}

	return true;
}

bool HandleDisconnect		(PacketStruct * Packet)
{
	// Display connection termination
	long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
	int Port = htons((*(sockaddr_in*)&Packet->Source->Remote).sin_port);
	printf("Connection termination from %i.%i.%i.%i:%i\n",			(int)(((BYTE*)&IP)[0]),
																	(int)(((BYTE*)&IP)[1]),
																	(int)(((BYTE*)&IP)[2]),
																	(int)(((BYTE*)&IP)[3]),
																	Port);

	// Delete the server
	Packet->Source->Disconnect(false);

	return true;
}

bool HandleSmallChunkBody	(PacketStruct * Packet)
{	// Chunk of packet
	if (Packet->Length > 2)
	{
		Packet->Source->IncomingChunks.Append((char*)&Packet->Message[2], Packet->Length - 2);
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed small chunk body from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
																		(int)(((BYTE*)&IP)[1]),
																		(int)(((BYTE*)&IP)[2]),
																		(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);
	}

	return true;
}

bool HandleSmallChunkTail	(PacketStruct * Packet)
{	// Last in a series of packet chunks
	if (Packet->Length > 2)
	{
		Packet->Source->IncomingChunks.Append((char*)&Packet->Message[2], Packet->Length - 2);

		Packet->Source->Route((BYTE*)Packet->Source->IncomingChunks.Contents, Packet->Source->IncomingChunks.Length);

		Packet->Source->IncomingChunks.Clear();
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed small chunk tail from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
																		(int)(((BYTE*)&IP)[1]),
																		(int)(((BYTE*)&IP)[2]),
																		(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);
	}

	return true;
}

bool HandleBigChunk			(PacketStruct * Packet)
{	// BIG chunk fragment
	if (Packet->Length > 6)
	{
		Packet->Source->BIGChunks.Append((char*)&Packet->Message[6], Packet->Length - 6);

		if (Packet->Source->BIGChunks.Length == GetLong(Packet->Message, 2))
		{
			Packet->Source->Route((BYTE*)Packet->Source->BIGChunks.Contents, GetShort(Packet->Message, 2));

			Packet->Source->BIGChunks.Clear();
		}
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed big chunk from %i.%i.%i.%i\n",		(int)(((BYTE*)&IP)[0]),
																(int)(((BYTE*)&IP)[1]),
																(int)(((BYTE*)&IP)[2]),
																(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);

		return true;
	}

	return true;
}

bool HandleCluster			(PacketStruct * Packet)
{	// Clump of packets
	SHORT Offset = 2;

	while (Offset < Packet->Length)
	{
		if (!Packet->Message[Offset] || ((Offset + Packet->Message[Offset]) >= Packet->Length))
		{
			long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
			printf("Malformed cluster from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
																	(int)(((BYTE*)&IP)[1]),
																	(int)(((BYTE*)&IP)[2]),
																	(int)(((BYTE*)&IP)[3]));
			// Delete the host
			Packet->Source->Disconnect(false);

			return true;
		}

		// Route packet
		Packet->Source->Route(&Packet->Message[Offset + 1], (SHORT)Packet->Message[Offset]);

		// Calculate next offset
		Offset += Packet->Message[Offset];
		Offset++;
	}

	return true;
}

bool HandleSpecial			(PacketStruct * Packet)
{
	if (Packet->Length > 1)
	{
		Packet->Source->SpecialRouter.CallHandler(Packet->Message[1], Packet);

		return false;
	}
	else
	{
		long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;
		printf("Malformed special header from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
																		(int)(((BYTE*)&IP)[1]),
																		(int)(((BYTE*)&IP)[2]),
																		(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);

		return true;
	}
}

bool HandleUnknown			(PacketStruct * Packet)
{
	printf("%i - Unhandled packet(%i)(%i)\n", Packet->Source->ScoreID, (int)Packet->Message[0], Packet->Length);

	return true;
}

bool HandlePing				(PacketStruct * Packet)
{
//	printf("%i - Ping?  Pong! (%i)(%i)\n", Packet->Source->ScoreID, (int)Packet->Message[0], Packet->Length);

	return true;
}

bool HandleLogin			(PacketStruct * Packet)
{
	// Offset 1 - ServerID
	// Offset 5 - GroupID
	// Offset 9 - ScoreID

	long IP = (*(sockaddr_in*)&Packet->Source->Remote).sin_addr.S_un.S_addr;

	if ((Packet->Length != 173) || InvalidZoneLogin(GetLong(Packet->Message, 5), GetLong(Packet->Message, 1), GetLong(Packet->Message, 9)))
	{
		printf("Invalid login from %i.%i.%i.%i\n",			(int)(((BYTE*)&IP)[0]),
															(int)(((BYTE*)&IP)[1]),
															(int)(((BYTE*)&IP)[2]),
															(int)(((BYTE*)&IP)[3]));
		// Delete the host
		Packet->Source->Disconnect(false);

		return true;
	}

	_string s;	// Broadcast to online operators

	int Port = htons((*(sockaddr_in*)&Packet->Source->Remote).sin_port);
	bool Allow = true;		// Bother checking for valid password

	// Find player in the re-log limiter
	_listnode <RecentSource> * Attempt = DATABASE.FindSource(IP);

	if (Attempt)
	{
		switch (Attempt->item->CheckLogins())
		{
		case 2:
			Packet->Source->Disconnect(false);

			Allow = false;

			break;
		case 1:
			// Send response
			printf("Zone connection flood from %i.%i.%i.%i:%i\n",			(int)(((BYTE*)&IP)[0]),
																			(int)(((BYTE*)&IP)[1]),
																			(int)(((BYTE*)&IP)[2]),
																			(int)(((BYTE*)&IP)[3]),
																			Port);

			Packet->Source->Disconnect(false);

			s  = "Zone connection flood detected from ";
			s += (int)(((BYTE*)&IP)[0]);
			s += ".";
			s += (int)(((BYTE*)&IP)[1]);
			s += ".";
			s += (int)(((BYTE*)&IP)[2]);
			s += ".";
			s += (int)(((BYTE*)&IP)[3]);

			Allow = false;
		};
	}
	else
	{	// Create a limiter entry since none was found
		if (DATABASE.SrcDatabase.Total > 100)
			DATABASE.SrcDatabase.Delete(DATABASE.SrcDatabase.head, false);

		RecentSource * src = new RecentSource(IP);
		DATABASE.SrcDatabase.Append(src);
	}

	if (Allow)
	{
		if (DATABASE.ValidateBillingPassword(GetLong(Packet->Message, 9), &Packet->Message[141]))
		{
			// Remove hosts sharing this server ID
			_listnode <Host> * parse = Packet->Source->Socket->HostList.head;

			while (parse)
			{
				if (parse->item->ScoreID == GetLong(Packet->Message, 9))
				{
					printf("%i - Connection broken: Same ScoreID requested session\n", parse->item->ScoreID);

					parse->item->Disconnect(false);

					break;
				}

				parse = parse->next;
			}

			// SubGame login accepted
			Packet->Source->SetName(&Packet->Message[13], GetLong(Packet->Message, 5), GetLong(Packet->Message, 9));
			DATABASE.LoadScores(Packet->Source);

			// Disable login packet handlers
			Packet->Source->SetHandler(2,	HandleUnknown);
			Packet->Source->SetSpecialHandler(1,	HandleUnknownSpecial);

			// Enable other handlers
			Packet->Source->SetHandler(1,	HandlePing);
			Packet->Source->SetHandler(3,	HandlePing);
			Packet->Source->SetHandler(4,	HandlePlayerEntering);
			Packet->Source->SetHandler(5,	HandlePlayerLeaving);
			Packet->Source->SetHandler(7,	HandleRemotePrivate);
			Packet->Source->SetHandler(13,	HandleRegistration);
			Packet->Source->SetHandler(14,	HandleLogMessage);
			Packet->Source->SetHandler(15,	HandleServerError);
			Packet->Source->SetHandler(16,	HandleChangeBanner);
			Packet->Source->SetHandler(17,	HandleStatus);
			Packet->Source->SetHandler(19,	HandleCommand);
			Packet->Source->SetHandler(20,	HandleChatMessage);

			printf("%i - Docking granted: %s\n", GetLong(Packet->Message, 9), &Packet->Message[13]);

			Packet->Source->HasSession = true;

			s  = (char*)&Packet->Message[13];
			s += "(";
			s += GetLong(Packet->Message, 9);
			s += ") - Zone connected.";
		}
		else
		{
			// SubGame login disallowed

			Packet->Source->Disconnect(false);

			printf("%i - Disallowed connection (Bad Password): %s\n", GetLong(Packet->Message, 9), &Packet->Message[13]);

			s  = (char*)&Packet->Message[13];
			s += "(";
			s += GetLong(Packet->Message, 9);
			s += ") - Disallowed connection (Bad password).";
		}
	}

	// Broadcast notification
	if (s.Text != "")
	{
		_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

		while (hparse)
		{
			_listnode <LUserNode> * uparse = hparse->item->UserList.head;

			while (uparse)
			{
				LUserNode * user = uparse->item;

				if (user->ZoneNotify && user->OpStatus && (user->OpStatus->Level <= 2))
				{
					if (user->OpStatus->GroupID == 0)
						user->SendChat(0, s.Text);
					else if (user->OpStatus->GroupID == GetLong(Packet->Message, 5))
					{
						if (user->OpStatus->ScoreID == 0)
							user->SendChat(0, s.Text);
						else if (user->OpStatus->ScoreID == GetLong(Packet->Message, 9))
							user->SendChat(0, s.Text);
					}
				}

				uparse = uparse->next;
			}

			hparse = hparse->next;
		}
	}

	return true;
}

bool HandlePlayerEntering	(PacketStruct * Packet)
{
	BYTE buffer[192];

	// Trim the last 5 letters of the player's name to fix social engineering exploits and other problems
	memset(&Packet->Message[25], 0, 5);

	// Trim leading and trailing zeros
	SpaceTrim(&Packet->Message[6], (char*)&Packet->Message[6], 20);

	// Block access to hacked clients (Bots generally)
	if (InvalidUsername((char*)&Packet->Message[6]))
	{
		MakePlayerResponse(buffer,						// Destination buffer
						   PRMEANING_BADNAME,				// Meaning
						   GetLong(Packet->Message, 70),	// PlayerID
						   NULL,							// Player name
						   NULL,							// Squad name
						   NULL,							// Banner
						   0,								// Total Seconds
						   0,								// Creation Year
						   0,								// Creation Month
						   0,								// Creation Day
						   0,								// Creation Hour
						   0,								// Creation Minute
						   0,								// Creation Second
						   0,								// Score ID
						   0,								// Wins
						   0,								// Losses
						   0,								// Goal count
						   0,								// Points
						   0,								// Event points
						   0,								// Extra 1
						   0);								// Extra 2

		// Send response
		Packet->Source->Send(buffer, 192, true);

		printf("%i - Player connection refused(Bad name): %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

		return true;
	}

	// Find player in the re-log limiter
	_listnode <RecentSource> * Attempt = DATABASE.FindSource(GetLong(Packet->Message, 2));

	if (Attempt)
	{
		switch (Attempt->item->CheckLogins())
		{
		case 2:		// 6 or more connection requests
			printf("%i - Player connection ignored(Flood): %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

			return true;
		case 1:		// 3 or more connection requests
			MakePlayerResponse(buffer,						// Destination buffer
							   PRMEANING_BUSY,					// Meaning
							   GetLong(Packet->Message, 70),	// PlayerID
							   NULL,							// Player name
							   NULL,							// Squad name
							   NULL,							// Banner
							   0,								// Total Seconds
							   0,								// Creation Year
							   0,								// Creation Month
							   0,								// Creation Day
							   0,								// Creation Hour
							   0,								// Creation Minute
							   0,								// Creation Second
							   0,								// Score ID
							   0,								// Wins
							   0,								// Losses
							   0,								// Goal count
							   0,								// Points
							   0,								// Event points
							   0,								// Extra 1
							   0);								// Extra 2

			// Send response
			Packet->Source->Send(buffer, 192, true);

			printf("%i - Player connection refused(Light flood): %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

			return true;
		};
	}
	else
	{	// Create a limiter entry since none was found
		if (DATABASE.SrcDatabase.Total > 100)
			DATABASE.SrcDatabase.Delete(DATABASE.SrcDatabase.head, false);

		RecentSource * src = new RecentSource(GetLong(Packet->Message, 2));
		DATABASE.SrcDatabase.Append(src);
	}

	// Find player in the user database
	_listnode <User> * parse = DATABASE.FindUser(&Packet->Message[6]);
	User * player;
	LONG ScoreID;

	if (parse)
	{
		player	= parse->item;
		ScoreID	= player->LoadID;
	}
	else
	{
		player	= NULL;
		ScoreID	= DATABASE.UserDatabase.Total + 1;
	}

	// Branch to handle different situations
	if (player)
	{
		if ((player->PassBlock != BLOCK_PASS) && (DATABASE.CheckBanfree(player->LoadID, Packet->Source->ScoreID) == NULL))
		{
			// Check if he's banned
			Ban * ban = DATABASE.CheckBan(ScoreID, GetLong(Packet->Message, 2), GetLong(Packet->Message, 74), (player->regform ? player->regform->HardwareID : -1) , Packet->Source->GroupID, Packet->Source->ScoreID);

			if (ban)
			{
				// The player is banned
				MakePlayerResponse(buffer,						// Destination buffer
								   PRMEANING_ENTER,					// Meaning
								   GetLong(Packet->Message, 70),	// PlayerID
								   "^Banned",						// Player name
								   NULL,							// Squad name
								   NULL,							// Banner
								   0,								// Total Seconds
								   0,								// Creation Year
								   0,								// Creation Month
								   0,								// Creation Day
								   0,								// Creation Hour
								   0,								// Creation Minute
								   0,								// Creation Second
								   0,								// Score ID
								   0,								// Wins
								   0,								// Losses
								   0,								// Goal count
								   0,								// Points
								   0,								// Event points
								   0,								// Extra 1
								   0);								// Extra 2

				// Send response
				Packet->Source->Send(buffer, 192, true);

				// Set up the user to be banned
				LUserNode * user = new LUserNode(Packet->Source, player, NULL, GetLong(Packet->Message, 70));
				Packet->Source->UserList.Append(user);
				user->Kick(10000, ban->BanID, ban->BanID);

				printf("%i - Banned user locked out: %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

				// Broadcast notification
				_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

				_string s;
				s  = (char*)&Packet->Source->Name[0];
				s += ": Banned user #";
				s += ban->BanID;
				s += " attempting to enter as ";
				s += (char*)&player->Name[0];
				switch (ban->LastType)
				{
				case 0:
					s += ". (Name match)";
					break;
				case 1:
					s += ". (Machine ID match)";
					break;
				case 2:
					if (ban->IPStart == ban->IPEnd)
						s += ". (IP match)";
					else
						s += ". (IP range match)";
					break;
				case 3:
					s += ". (Hardware ID match)";

					// Update ban information
					LONG IP = GetLong(Packet->Message, 2);

					switch (ban->IPEnd ^ ban->IPStart)
					{
					case 0:
						ban->IPStart = IP;
						ban->IPEnd = IP;
						break;
					case 0xFF000000:
						ban->IPStart = IP & 0x00FFFFFF;
						ban->IPEnd = IP | 0xFF000000;
						break;
					case 0xFFFF0000:
						ban->IPStart = IP & 0x0000FFFF;
						ban->IPEnd = IP | 0xFFFF0000;
						break;
					};

					break;
				};

				while (hparse)
				{
					_listnode <LUserNode> * uparse = hparse->item->UserList.head;

					while (uparse)
					{
						LUserNode * user = uparse->item;

						if (user->BanNotify && user->OpStatus)
							user->SendChat(0, s.Text);

						uparse = uparse->next;
					}

					hparse = hparse->next;
				}

				return true;
			}

			// Check if the TZB is hacked
			if (InvalidTimeZoneBias(GetShort(Packet->Message, 78)))
			{
				// The player is using a machine ID randomizer - make life miserable for him
				MakePlayerResponse(buffer,						// Destination buffer
								   PRMEANING_ENTER,					// Meaning
								   GetLong(Packet->Message, 70),	// PlayerID
								   "^Banned",						// Player name
								   NULL,							// Squad name
								   NULL,							// Banner
								   0,								// Total Seconds
								   0,								// Creation Year
								   0,								// Creation Month
								   0,								// Creation Day
								   0,								// Creation Hour
								   0,								// Creation Minute
								   0,								// Creation Second
								   0,								// Score ID
								   0,								// Wins
								   0,								// Losses
								   0,								// Goal count
								   0,								// Points
								   0,								// Event points
								   0,								// Extra 1
								   0);								// Extra 2

				// Send response
				Packet->Source->Send(buffer, 192, true);

				// Set up the user to be banned
				LUserNode * user = new LUserNode(Packet->Source, player, NULL, GetLong(Packet->Message, 70));
				Packet->Source->UserList.Append(user);
				user->Kick(10000, 0, 0);

				printf("%i - User randomizing TimeZoneBias locked out: %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

				// Broadcast notification
				_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

				_string s;
				s  = (char*)&Packet->Source->Name[0];
				s += ": ";
				s += (char*)&player->Name[0];
				s += " connecting with a hacked client. (Invalid TZB)(";
				s += GetShort(Packet->Message, 78);
				s += ")";

				while (hparse)
				{
					_listnode <LUserNode> * uparse = hparse->item->UserList.head;

					while (uparse)
					{
						LUserNode * user = uparse->item;

						if (user->BanNotify && user->OpStatus)
							user->SendChat(0, s.Text);

						uparse = uparse->next;
					}

					hparse = hparse->next;
				}

				return true;
			}

			// Check if LastIP = IP but LastBanID != BanID
			//  || ((player->IP == GetLong(Packet->Message, 2)) && (player->MacID != GetLong(Packet->Message, 74)))
			if (InvalidMachineID(GetLong(Packet->Message, 74)))
			{
				// The player is using a machine ID randomizer - make life miserable for him
				MakePlayerResponse(buffer,						// Destination buffer
								   PRMEANING_ENTER,					// Meaning
								   GetLong(Packet->Message, 70),	// PlayerID
								   "^Banned",						// Player name
								   NULL,							// Squad name
								   NULL,							// Banner
								   0,								// Total Seconds
								   0,								// Creation Year
								   0,								// Creation Month
								   0,								// Creation Day
								   0,								// Creation Hour
								   0,								// Creation Minute
								   0,								// Creation Second
								   0,								// Score ID
								   0,								// Wins
								   0,								// Losses
								   0,								// Goal count
								   0,								// Points
								   0,								// Event points
								   0,								// Extra 1
								   0);								// Extra 2

				// Send response
				Packet->Source->Send(buffer, 192, true);

				// Set up the user to be banned
				LUserNode * user = new LUserNode(Packet->Source, player, NULL, GetLong(Packet->Message, 70));
				Packet->Source->UserList.Append(user);
				user->Kick(10000, 0, 0);

				printf("%i - User with invalid machine ID locked out: %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

				// Broadcast notification
				_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

				_string s;
				s  = (char*)&Packet->Source->Name[0];
				s += ": ";
				s += (char*)&player->Name[0];
				s += " connecting with a hacked client. (Invalid MachineID)(";
				s += GetLong(Packet->Message, 74);
				s += ")";

				while (hparse)
				{
					_listnode <LUserNode> * uparse = hparse->item->UserList.head;

					while (uparse)
					{
						LUserNode * user = uparse->item;

						if (user->BanNotify && user->OpStatus)
							user->SendChat(0, s.Text);

						uparse = uparse->next;
					}

					hparse = hparse->next;
				}

				return true;
			}
		}

		// In database
		if (player->CheckLastAccess(&Packet->Message[38]) || DATABASE.ValidatePassword(player, &Packet->Message[38]))
		{
			// Correct password

			// Produce a date local to the user
			tm * CDate = Localize(&player->CreationDate, GetShort(Packet->Message, 78));

			// Produce a score
			_listnode <Score>	* scr_parse	= Packet->Source->ScoreDatabase.head;
			Score				* score		= NULL;

			while (scr_parse)
			{
				if (scr_parse->item->Owner == player)
				{
					score = scr_parse->item;
					break;
				}

				scr_parse = scr_parse->next;
			}

			if (score)
			{
				// Has a score
				MakePlayerResponse(buffer,						// Destination buffer
								   (player->regform ? PRMEANING_ENTER : PRMEANING_REGFORM),	// Meaning
								   GetLong(Packet->Message, 70),	// PlayerID
								   (char*)&Packet->Message[6],				// Player name
								   (player->squad ? player->squad->Name : NULL),			// Squad name
								   player->Banner,					// Banner
								   player->TotalSeconds,			// Total Seconds
								   (CDate ? GetYear(CDate) : 0),	// Creation Year
								   (CDate ? CDate->tm_mon + 1 : 0),	// Creation Month
								   (CDate ? CDate->tm_mday : 0),	// Creation Day
								   (CDate ? CDate->tm_hour : 0),	// Creation Hour
								   (CDate ? CDate->tm_min : 0),		// Creation Minute
								   (CDate ? CDate->tm_sec : 0),		// Creation Second
								   ScoreID,							// Score ID
								   score->Wins,						// Wins
								   score->Losses,					// Losses
								   score->Flags,					// Goal count
								   score->Points,					// Points
								   score->EventPoints,				// Event points
								   0,								// Extra 1
								   0);								// Extra 2

				printf("%i - Player entering: %s\n", Packet->Source->ScoreID, player->Name);
			}
			else
			{
				// Has no score
				MakePlayerResponse(buffer,						// Destination buffer
								   (player->regform ? PRMEANING_ENTER : PRMEANING_REGFORM),	// Meaning
								   GetLong(Packet->Message, 70),	// PlayerID
								   (char*)&Packet->Message[6],				// Player name
								   (player->squad ? player->squad->Name : NULL),			// Squad name
								   player->Banner,					// Banner
								   player->TotalSeconds,			// Total Seconds
								   (CDate ? GetYear(CDate) : 0),	// Creation Year
								   (CDate ? CDate->tm_mon + 1 : 0),	// Creation Month
								   (CDate ? CDate->tm_mday : 0),	// Creation Day
								   (CDate ? CDate->tm_hour : 0),	// Creation Hour
								   (CDate ? CDate->tm_min : 0),		// Creation Minute
								   (CDate ? CDate->tm_sec : 0),		// Creation Second
								   ScoreID,							// Score ID
								   0,								// Wins
								   0,								// Losses
								   0,								// Goal count
								   0,								// Points
								   0,								// Event points
								   0,								// Extra 1
								   0);								// Extra 2

				printf("%i - Player entering(No Score): %s\n", Packet->Source->ScoreID, player->Name);
			}

			// Add user to server
			LUserNode * user = new LUserNode(Packet->Source, player, score, GetLong(Packet->Message, 70));
			Packet->Source->UserList.Append(user);
			user->CheckMsg(10000);

			// Update "last*" user statistics
			player->SetLastLogin();
			player->SetBanInfo(GetLong(Packet->Message, 2), GetLong(Packet->Message, 74), GetShort(Packet->Message, 78));
		}
		else
		{
			// Incorrect password
			MakePlayerResponse(buffer,						// Destination buffer
							   PRMEANING_BADPW,					// Meaning
							   GetLong(Packet->Message, 70),	// PlayerID
							   NULL,							// Player name
							   NULL,							// Squad name
							   NULL,							// Banner
							   0,								// Total Seconds
							   0,								// Creation Year
							   0,								// Creation Month
							   0,								// Creation Day
							   0,								// Creation Hour
							   0,								// Creation Minute
							   0,								// Creation Second
							   0,								// Score ID
							   0,								// Wins
							   0,								// Losses
							   0,								// Goal count
							   0,								// Points
							   0,								// Event points
							   0,								// Extra 1
							   0);								// Extra 2

			printf("%i - Invalid password: %s\n", Packet->Source->ScoreID, player->Name);
		}
	}
	else if (DATABASE.AllowNewUsers == 0)
	{
		// He isn't in the database, and we aren't accepting new users
		MakePlayerResponse(buffer,						// Destination buffer
						   PRMEANING_NONEW,					// Meaning
						   GetLong(Packet->Message, 70),	// PlayerID
						   NULL,							// Player name
						   NULL,							// Squad name
						   NULL,							// Banner
						   0,								// Total Seconds
						   0,								// Creation Year
						   0,								// Creation Month
						   0,								// Creation Day
						   0,								// Creation Hour
						   0,								// Creation Minute
						   0,								// Creation Second
						   0,								// Score ID
						   0,								// Wins
						   0,								// Losses
						   0,								// Goal count
						   0,								// Points
						   0,								// Event points
						   0,								// Extra 1
						   0);								// Extra 2

		printf("%i - Player connection refused(New user): %s\n", Packet->Source->ScoreID, &Packet->Message[6]);
	}
	else
	{
		Ban * ban = DATABASE.CheckBan(ScoreID, GetLong(Packet->Message, 2), GetLong(Packet->Message, 74), -1, Packet->Source->GroupID, Packet->Source->ScoreID);

		if (ban)
		{
			// Player is banned
			MakePlayerResponse(buffer,						// Destination buffer
							   PRMEANING_ENTER,					// Meaning
							   GetLong(Packet->Message, 70),	// PlayerID
							   "^Banned",						// Player name
							   NULL,							// Squad name
							   NULL,							// Banner
							   0,								// Total Seconds
							   0,								// Creation Year
							   0,								// Creation Month
							   0,								// Creation Day
							   0,								// Creation Hour
							   0,								// Creation Minute
							   0,								// Creation Second
							   0,								// Score ID
							   0,								// Wins
							   0,								// Losses
							   0,								// Goal count
							   0,								// Points
							   0,								// Event points
							   0,								// Extra 1
							   0);								// Extra 2

			// Send response
			Packet->Source->Send(buffer, 192, true);

			// Set up the user to be banned
			LUserNode * user = new LUserNode(Packet->Source, DATABASE.BannedGuy, NULL, GetLong(Packet->Message, 70));
			Packet->Source->UserList.Append(user);
			user->Kick(10000, ban->BanID, ban->BanID);

			printf("%i - Banned user locked out: %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

			// Broadcast notification
			_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

			_string s;
			s  = (char*)&Packet->Source->Name[0];
			s += ": Banned user #";
			s += ban->BanID;
			s += " attempting to enter as ";
			s += (char*)&Packet->Message[6];
			switch (ban->LastType)
			{
			case 0:
				s += ". (Name match)";
				break;
			case 1:
				s += ". (Machine ID match)";
				break;
			case 2:
				if (ban->IPStart == ban->IPEnd)
					s += ". (IP match)";
				else
					s += ". (IP range match)";
				break;
			};

			while (hparse)
			{
				_listnode <LUserNode> * uparse = hparse->item->UserList.head;

				while (uparse)
				{
					LUserNode * user = uparse->item;

					if (user->BanNotify && user->OpStatus)
						user->SendChat(0, s.Text);

					uparse = uparse->next;
				}

				hparse = hparse->next;
			}

			return true;
		}

		// Check if the MacID is hacked
		if (InvalidMachineID(GetLong(Packet->Message, 74)))
		{
			// The player is using a machine ID randomizer - make life miserable for him
			MakePlayerResponse(buffer,						// Destination buffer
							   PRMEANING_ENTER,					// Meaning
							   GetLong(Packet->Message, 70),	// PlayerID
							   "^Banned",						// Player name
							   NULL,							// Squad name
							   NULL,							// Banner
							   0,								// Total Seconds
							   0,								// Creation Year
							   0,								// Creation Month
							   0,								// Creation Day
							   0,								// Creation Hour
							   0,								// Creation Minute
							   0,								// Creation Second
							   0,								// Score ID
							   0,								// Wins
							   0,								// Losses
							   0,								// Goal count
							   0,								// Points
							   0,								// Event points
							   0,								// Extra 1
							   0);								// Extra 2

			// Send response
			Packet->Source->Send(buffer, 192, true);

			// Set up the user to be banned
			LUserNode * user = new LUserNode(Packet->Source, DATABASE.BannedGuy, NULL, GetLong(Packet->Message, 70));
			Packet->Source->UserList.Append(user);
			user->Kick(10000, 0, 0);

			printf("%i - User with invalid MachineID locked out: %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

			// Broadcast notification
			_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

			_string s;
			s  = (char*)&Packet->Source->Name[0];
			s += ": ";
			s += (char*)&Packet->Message[6];
			s += " connecting with a hacked client. (Invalid MachineID)(";
			s += GetLong(Packet->Message, 74);
			s += ")";

			while (hparse)
			{
				_listnode <LUserNode> * uparse = hparse->item->UserList.head;

				while (uparse)
				{
					LUserNode * user = uparse->item;

					if (user->BanNotify && user->OpStatus)
						user->SendChat(0, s.Text);

					uparse = uparse->next;
				}

				hparse = hparse->next;
			}

			return true;
		}

		// Check if the TZB is hacked
		if (InvalidTimeZoneBias(GetShort(Packet->Message, 78)))
		{
			// The player is using a machine ID randomizer - make life miserable for him
			MakePlayerResponse(buffer,						// Destination buffer
							   PRMEANING_ENTER,					// Meaning
							   GetLong(Packet->Message, 70),	// PlayerID
							   "^Banned",						// Player name
							   NULL,							// Squad name
							   NULL,							// Banner
							   0,								// Total Seconds
							   0,								// Creation Year
							   0,								// Creation Month
							   0,								// Creation Day
							   0,								// Creation Hour
							   0,								// Creation Minute
							   0,								// Creation Second
							   0,								// Score ID
							   0,								// Wins
							   0,								// Losses
							   0,								// Goal count
							   0,								// Points
							   0,								// Event points
							   0,								// Extra 1
							   0);								// Extra 2

			// Send response
			Packet->Source->Send(buffer, 192, true);

			// Set up the user to be banned
			LUserNode * user = new LUserNode(Packet->Source, DATABASE.BannedGuy, NULL, GetLong(Packet->Message, 70));
			Packet->Source->UserList.Append(user);
			user->Kick(10000, 0, 0);

			printf("%i - User with invalid TimeZoneBias locked out: %s\n", Packet->Source->ScoreID, &Packet->Message[6]);

			// Broadcast notification
			_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

			_string s;
			s  = (char*)&Packet->Source->Name[0];
			s += ": ";
			s += (char*)&Packet->Message[6];
			s += " connecting with a hacked client. (Invalid TZB)(";
			s += GetShort(Packet->Message, 78);
			s += ")";

			while (hparse)
			{
				_listnode <LUserNode> * uparse = hparse->item->UserList.head;

				while (uparse)
				{
					LUserNode * user = uparse->item;

					if (user->BanNotify && user->OpStatus)
						user->SendChat(0, s.Text);

					uparse = uparse->next;
				}

				hparse = hparse->next;
			}

			return true;
		}

		// Not in database
		if (Packet->Message[1] || (DATABASE.AskNewUsers == 0))
		{
			// Add now
			MakePlayerResponse(buffer,						// Destination buffer
							   PRMEANING_REGFORM,				// Meaning
							   GetLong(Packet->Message, 70),	// PlayerID
							   (char*)&Packet->Message[6],		// Player name
							   NULL,							// Squad name
							   NULL,							// Banner
							   0,								// Total Seconds
							   0,								// Creation Year
							   0,								// Creation Month
							   0,								// Creation Day
							   0,								// Creation Hour
							   0,								// Creation Minute
							   0,								// Creation Second
							   ScoreID,							// Score ID
							   0,								// Wins
							   0,								// Losses
							   0,								// Goal count
							   0,								// Points
							   0,								// Event points
							   0,								// Extra 1
							   0);								// Extra 2

			player = DATABASE.AddUser(&Packet->Message[6], &Packet->Message[38], GetLong(Packet->Message, 2), GetLong(Packet->Message, 74), GetShort(Packet->Message, 78));

			LUserNode * user = new LUserNode(Packet->Source, player, NULL, GetLong(Packet->Message, 70));
			Packet->Source->UserList.Append(user);
			user->CheckMsg(10000);

			printf("%i - New player entering: %s\n", Packet->Source->ScoreID, player->Name);
		}
		else
		{
			// Ask first
			MakePlayerResponse(buffer,						// Destination buffer
							   PRMEANING_ASK,					// Meaning
							   GetLong(Packet->Message, 70),	// PlayerID
							   NULL,							// Player name
							   NULL,							// Squad name
							   NULL,							// Banner
							   0,								// Total Seconds
							   0,								// Creation Year
							   0,								// Creation Month
							   0,								// Creation Day
							   0,								// Creation Hour
							   0,								// Creation Minute
							   0,								// Creation Second
							   0,								// Score ID
							   0,								// Wins
							   0,								// Losses
							   0,								// Goal count
							   0,								// Points
							   0,								// Event points
							   0,								// Extra 1
							   0);								// Extra 2

			printf("%i - New player connected: %s\n", Packet->Source->ScoreID, &Packet->Message[6]);
		}

	}

	// Send response
	Packet->Source->Send(buffer, 192, true);

	return true;
}

bool HandleRegistration		(PacketStruct * Packet)
{
	/* -Field Title-	-Offset-	-Length-	-Comments-
	 * UserID			1			4			Zone ID
	 * Name				5			32
	 * Email			37			64			Twice as long as Name
	 * City				101			32
	 * State			133			24			Odd length
	 * Sex				157			1			'M' / 'F'
	 * Age				158			1
	 * Home				159			1			1(TRUE) / 0(FALSE)
	 * Work				160			1			1(TRUE) / 0(FALSE)
	 * School			161			1			1(TRUE) / 0(FALSE)
	 * Processor		162			2			ie. 586
	 * Unknown			164			6			WTF are these for?
	 * Hostname			170			80			Should this be used in HID?
	 */

	LONG fnFile = _open("regform.bin", _O_BINARY | _O_CREAT | _O_WRONLY, _S_IWRITE);
	_write(fnFile, Packet->Message, Packet->Length);
	_close(fnFile);

	_listnode <LUserNode> * parse = Packet->Source->FindByID(GetLong(Packet->Message, 1));

	if (parse)
	{
		// Extract Hardware ID
		DWORD HID = HashModemInfo(&Packet->Message[162]);

		Ban * ban = DATABASE.CheckBanID(HID);

		if (ban)
		{
			DATABASE.AddReg(parse->item->player, (char*)&Packet->Message[5], (char*)&Packet->Message[37], "(No details)", HID);

			printf("%i - Banned user getting kicked: %s\n", Packet->Source->ScoreID, parse->item->player->Name);
			parse->item->Kick(10000, ban->BanID, ban->BanID);

			// Update ban information
			LONG IP = parse->item->player->IP;
			switch (ban->IPEnd ^ ban->IPStart)
			{
			case 0:
				ban->IPStart = IP;
				ban->IPEnd = IP;
				break;
			case 0xFF000000:
				ban->IPStart = IP & 0x00FFFFFF;
				ban->IPEnd = IP | 0xFF000000;
				break;
			case 0xFFFF0000:
				ban->IPStart = IP & 0x0000FFFF;
				ban->IPEnd = IP | 0xFFFF0000;
				break;
			};

			// Broadcast notification
			_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

			_string s;
			s  = (char*)&Packet->Source->Name[0];
			s += ": Banned user #";
			s += ban->BanID;
			s += " attempting to enter as ";
			s += (char*)&parse->item->player->Name[0];
			s += ". (HardwareID match)";

			while (hparse)
			{
				_listnode <LUserNode> * uparse = hparse->item->UserList.head;

				while (uparse)
				{
					LUserNode * user = uparse->item;

					if (user->BanNotify && user->OpStatus)
						user->SendChat(0, s.Text);

					uparse = uparse->next;
				}

				hparse = hparse->next;
			}
		}
		else
		{
			bool Allow = true;

			if (HID == 0)
				Allow = false;

			switch (GetShort(Packet->Message, 162))
			{
			case 286:
			case 386:
			case 486:
			case 586:
			case 686:
				// Valid values
				break;
			default:
				Allow = false;
			};

			if (Allow == false)
			{
				// Player attempting to randomize hardware id
				printf("%i - Player sending invalid registration form: %s\n", Packet->Source->ScoreID, parse->item->player->Name);
				parse->item->Kick(10000, 0, 0);

				// Broadcast notification
				_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

				_string s;
				s  = (char*)&Packet->Source->Name[0];
				s += ": ";
				s += (char*)&parse->item->player->Name[0];
				s += " connecting with a hacked client. (Invalid Reg Form)";

				while (hparse)
				{
					_listnode <LUserNode> * uparse = hparse->item->UserList.head;

					while (uparse)
					{
						LUserNode * user = uparse->item;

						if (user->BanNotify && user->OpStatus)
							user->SendChat(0, s.Text);

						uparse = uparse->next;
					}

					hparse = hparse->next;
				}

				return true;
			}

			DATABASE.AddReg(parse->item->player, (char*)&Packet->Message[5], (char*)&Packet->Message[37], "(No details)", HID);

			printf("%i - %s sent registration data\n", Packet->Source->ScoreID, parse->item->player->Name);
		}
	}
	else
		printf("%i - (UNKNOWN) sent registration data\n", Packet->Source->ScoreID);

	return true;
}

bool HandlePlayerLeaving	(PacketStruct * Packet)
{
	_listnode <LUserNode> * parse = Packet->Source->FindByID(GetLong(Packet->Message, 1));

	if (parse)
	{
		// Found him in the zone

		// Leave chats
		LUserNode	* node		= parse->item;

		if (node->OpStatus)
			node->OpStatus->RemoveLogin();

		node->ExitAllChats(true);

		// Get User and Score
		User		* player	= node->player;
		Score		* score		= node->score;

		if ((Packet->Length > 15) && Packet->Source->AcceptScores)
		{
			// Packet contains a score

			if (!score)
			{
				// No score exists for this player
				score = new Score(player, Packet->Source->ScoreID);
				DATABASE.ScoreDatabase.Append(score);
				Packet->Source->ScoreDatabase.Append(score);

				printf("%i - Player leaving(New Score): %s\n", Packet->Source->ScoreID, player->Name);
			}
			else
			{
				// Score exists
				printf("%i - Player leaving: %s\n", Packet->Source->ScoreID, player->Name);
			}

			// Update score
			score->SetScore(GetShort(Packet->Message, 15),	// Wins
							GetShort(Packet->Message, 17),	// Losses
							GetShort(Packet->Message, 19),	// Goals
							GetLong(Packet->Message, 21),	// Points
							GetLong(Packet->Message, 25));	// Event points
		}
		else
		{
			// Packet does not contain a score

			printf("%i - Player leaving(No Score): %s\n", Packet->Source->ScoreID, player->Name);
		}

		// Update total seconds online
		player->TotalSeconds += GetShort(Packet->Message, 13);

		// Remove user from zone
		Packet->Source->UserList.Delete(parse, false);
	}
	else
	{
		// Could not find him in the zone

		printf("%i - Player leaving(INVALID)\n", Packet->Source->ScoreID);
	}

	return true;
}

bool HandleRemotePrivate	(PacketStruct * Packet)
{
	BYTE buffer[512];
	SHORT Length;

	// Write up a response
	Length = MakeRemotePrivate(buffer, &Packet->Message[11], Packet->Length);

	//-1 = Not a player message
	if (GetLong(Packet->Message, 1) != 0xFFFFFFFF)
	{
		// A player is commanding the biller to broadcast something
		_listnode <LUserNode> * result = Packet->Source->FindByID(GetLong(Packet->Message, 1));

		if (result)
		{
			LUserNode * node = result->item;
			bool Append = false;

			if (node->OpStatus == NULL)
			{
				return true;
			}
			else
			{
				if (node->OpStatus->GroupID)
						return true;

				switch (node->OpStatus->Level)
				{
				case 0:
				case 1:
					Append = false;
					break;
				case 2:
					Append = true;
					break;
				default:
					return true;
				};
			}

			if (Append)
			{
				// Add tag [Name@Zone]
				strcat((char*)&buffer[11], " [");
				strcat((char*)&buffer[11], (char*)&node->player->Name[0]);
				strcat((char*)&buffer[11], "@");
				strcat((char*)&buffer[11], (char*)&Packet->Source->Name[0]);
				strcat((char*)&buffer[11], "]");
				Length += (4 + strlen((char*)&node->player->Name[0]) + strlen((char*)&Packet->Source->Name[0]));
			}

			/*if (node->OpStatus->GroupID != 0)
			{
				// Send only to people in the group
				_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

				while (hparse)
				{
					if (hparse->item->BroadcastChat && (hparse->item->GroupID == node->OpStatus->GroupID))
						hparse->item->Send(buffer, Length, true);

					hparse = hparse->next;
				}

				_string s;
				s += "NOTICE: The message was only sent to zone group ";
				s += node->OpStatus->GroupID;
				node->SendArena(s.Text);

				return true;
			}*/
		}
		else
		{
			// Add tag [Zone]
			strcat((char*)&buffer[11], " [");
			strcat((char*)&buffer[11], (char*)&Packet->Source->Name[0]);
			strcat((char*)&buffer[11], "]");
			Length += (3 + strlen((char*)&Packet->Source->Name[0]));
		}
	}

	// Broadcast it
	_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;

	while (hparse)
	{
		if (hparse->item->BroadcastChat)
			hparse->item->Send(buffer, Length, true);

		hparse = hparse->next;
	}

	printf("%i - Broadcast %s\n", Packet->Source->ScoreID, &Packet->Message[11]);

	return true;
}

bool HandleLogMessage		(PacketStruct * Packet)
{
	//  For some odd reason, the LogMessages are sent with
	//  UserID tags instead of PlayerID tags.
	_listnode <User> * parse = DATABASE.UserDatabase.head;

	User * From		= NULL,
		 * To		= NULL;

	LONG FromID		= GetLong(Packet->Message, 1),
		 ToID		= GetLong(Packet->Message, 5);

	bool GotFrom	= (FromID == -1) ? true : false,
		 GotTo		= (ToID == -1) ? true : false;

	while (parse)
	{
		User * user = parse->item;

		if (user->LoadID == FromID)
		{
			From = parse->item;

			if (GotTo)
				break;

			GotFrom = true;
		}

		if (user->LoadID == ToID)
		{
			To = parse->item;

			if (GotFrom)
				break;

			GotTo = true;
		}

		parse = parse->next;
	}

	if (From)
		if (To)
		{
			printf("%i - (%s -> %s)> %s\n", Packet->Source->ScoreID, From->Name, To->Name, &Packet->Message[9]);

			/*if (MatchString((char*)&Packet->Message[9], "*monitor", 8))
			{
				_listnode <LUserNode> * parse = Packet->Source->UserList.head;

				while (parse)
				{
					LUserNode * user = parse->item;

					if (user->player == From)
					{
						if (user->OpStatus->Level <= 2)
						{
							_string s;
							s  = (char*)&To->Name[0];
							s += ": *monitor has not been implemented yet.";

							user->SendArena(s.Text);
						}

						break;
					}

					parse = parse->next;
				}
			}*/
		}
		else
			printf("%i - (%s)> %s\n", Packet->Source->ScoreID, From->Name, &Packet->Message[9]);

	return true;
}

bool HandleChatMessage		(PacketStruct * Packet)
{
	_listnode <LUserNode> * parse = Packet->Source->FindByID(GetLong(Packet->Message, 1));

	if (parse)
	{
		// Source player exists
		LUserNode * player = parse->item;

		// Find source channel
		BYTE c = Packet->Message[5],
			 Channel;
		if (c >= '0' && c <= '9')
		{
			Channel = c - '0';
		}
		else
		{
			Channel = 1;
		}

		if (player->channels[Channel])
		{
			// Channel exists
			Chat  * chat = player->channels[Channel];
			BYTE	buffer[512];
			SHORT	namelen;

			// Generate message body
			memcpy(buffer, player->player->Name, namelen = strlen((char*)player->player->Name));
			strcpy((char*)&buffer[namelen], "> ");
			memcpy(&buffer[2 + namelen], &Packet->Message[37], Packet->Length - 37);

			// Broadcast
			_listnode <Chatter> * parse = chat->Participants.head;

			while (parse)
			{
				LUserNode * chatter = parse->item->Player;

				if ((chatter != player) && (chatter->zone->BroadcastChat))
					chatter->SendChat(parse->item->ID, buffer);

				parse = parse->next;
			}

			printf("%i - Chat: (%s)%s>%s\n", Packet->Source->ScoreID, chat->Name, player->player->Name, &Packet->Message[37]);
		}
		else
		{
			// Channel does not exist

			printf("%i - Chat(IGNORED)%s>%s\n", Packet->Source->ScoreID, player->player->Name, &Packet->Message[37]);
		}
	}
	else
	{
		// Source player does not exist

		printf("%i - Chat(INVALID)\n", Packet->Source->ScoreID);
	}

	return true;
}

bool HandleServerError		(PacketStruct * Packet)
{
	printf("%i - %s\n", Packet->Source->ScoreID, &Packet->Message[5]);

	return true;
}

bool HandleChangeBanner		(PacketStruct * Packet)
{
	_listnode <LUserNode> * user = Packet->Source->FindByID(GetLong(Packet->Message, 1));

	if (user)
	{
		// Exists
		printf("%i - Got player banner: %s\n", Packet->Source->ScoreID, user->item->player->Name);

		user->item->player->SetBanner((BYTE*)&Packet->Message[5]);
	}
	else
	{
		// Does not
		printf("%i - Got player banner(INVALID)\n", Packet->Source->ScoreID);
	}

	return true;
}

bool HandleStatus			(PacketStruct * Packet)
{
	_listnode <LUserNode> * user = Packet->Source->FindByID(GetLong(Packet->Message, 1));

	if (user)
	{
		// Exists
		printf("%i - %s(%i)(%i)(%i)(%i)(%i)(%i)(%i)(%i)\n", Packet->Source->ScoreID,
															user->item->player->Name,
															GetShort(Packet->Message, 5),
															GetShort(Packet->Message, 9),
															GetShort(Packet->Message, 13),
															GetShort(Packet->Message, 17),
															GetShort(Packet->Message, 5),
															GetShort(Packet->Message, 9),
															GetShort(Packet->Message, 13),
															GetShort(Packet->Message, 17));
	}
	else
	{
		// Does not
		printf("%i - (INVALID)(%i)(%i)(%i)(%i)(%i)(%i)(%i)(%i)\n", Packet->Source->ScoreID,
																GetShort(Packet->Message, 5),
																GetShort(Packet->Message, 9),
																GetShort(Packet->Message, 13),
																GetShort(Packet->Message, 17),
																GetShort(Packet->Message, 5),
																GetShort(Packet->Message, 9),
																GetShort(Packet->Message, 13),
																GetShort(Packet->Message, 17));
	}

	return true;
}
