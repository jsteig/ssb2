#ifndef PRNG_H
#define PRNG_H
#endif


class LCG_PRNG;										// PRNG based on modulus arithmetic (not good with small numbers)

class LFSR_PRNG;									// PRNG based on bit-shifting (very nice)

inline unsigned long ROT(unsigned long, int);		// Rotate given 32 bits by given number of bits

inline unsigned long ROT13(unsigned long);			// Rotate given 32 bits by 13 bits (used by the LFSR generator)


// Linear Congruential Generator

class LCG_PRNG
{
	long s1, s2;

public:
	LCG_PRNG()
	{
		s1 = 99999;
		s2 = 55555;
	}

	double GetNext()
	{
		s1 = 40014 * (s1 % 53668) - (s1 /53668) * 12211;
		if (s1 < 0) s1 += 2147483563;

		s2 = 40692 * (s2 % 52774) - (s2 / 52774) * 3791;
		if (s2 < 0) s2 += 2147483399;

		long z = (s1 - 2147483563) + s2;
		if (z < 1) z += 2147483562;

		return((double) z / 2147483563.0);
	}

	void Seed(long NewSeed)
	{
		s1 = 55555 ^ NewSeed;
		s2 = NewSeed;
	}

	void Seed(long NewSeedX, long NewSeedY)
	{
		s1 = NewSeedX;
		s2 = NewSeedY;
	}
};


// Linear Feedback Shift Register Generator

inline unsigned long ROT(unsigned long x, int r)
{
	return (x << r) | (x >> ( 32 - r ));
}

inline unsigned long ROT13(unsigned long x)
{
	return (x << 13) | (x >> 19);
}

class LFSR_PRNG
{
	int				p1, p2;
	int				imin, iinterval;
	unsigned long	randbuffer[11];

public:
	LFSR_PRNG()
	{
		Seed(55555);
	}

	double GetNextPercent();
	unsigned long GetNextNumber();

	void Seed(unsigned long seed)
	{
		if (!seed) seed--;

		for (int i = 0; i < 11; i++)
		{
			seed ^= seed << 13;
			seed ^= seed >> 17;
			seed ^= seed << 5;
			randbuffer[i] = seed;
		}

		p1 = 0;
		p2 = 7;

		for (i = 0; i < 9; i++)
			GetNextNumber();
	}
};

double LFSR_PRNG::GetNextPercent()
{
	unsigned long x = (randbuffer[p1] = ROT13(randbuffer[p1] + randbuffer[p2]));

	if (--p1 < 0) p1 = 10;
	if (--p2 < 0) p2 = 10;

	union
	{
		double		  randp1;
		unsigned long randbits[2];
	};

	randbits[0] = (x << 20);
	randbits[1] = (x >> 12) | 0x3FF00000;

	return randp1 - 1.0;
}

unsigned long LFSR_PRNG::GetNextNumber()
{
	unsigned long x = (randbuffer[p1] = ROT13(randbuffer[p1] + randbuffer[p2]));

	if (--p1 < 0) p1 = 10;
	if (--p2 < 0) p2 = 10;

	return x;
}
