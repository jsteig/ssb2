#ifndef DECLARES_H
#define DECLARES_H
#endif


// Include files

#pragma pack(4)
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

#include <stdio.h>
#include <conio.h>
#include <time.h>

#include <io.h>
#include <fcntl.H>
#include <SYS\STAT.H>
#include <ERRNO.H>

#include "linkedlist.h"
#include "prng.h"
#include <winsock.h>


// Objects

class PacketStruct;		// Packet
class PostlogNode;		// Postlog node
class Mutex;			// Safe multi-threading made easy
class Host;				// Connected host object
class WinsockSession;	// Socket implementation

class User;				// User database entry
class Squad;			// Squad database entry
class Score;			// Score database entry
class ChatSystem;		// Chat system entry
class Chat;				// Chat database entry
class ChatPremitList;	// Chat premitted list entry
class Operator;			// Operator database entry
class Zone;				// Zone database entry
class RegForm;			// Registration database entry
class Ban;				// Ban database entry
class Database;			// Encapsulates disk access to .DAT files

class LUserNode;		// Node on a server's logged-in user list


// Player entering constants

enum Login_Params
{
	PRMEANING_ENTER,	// Implemented
	PRMEANING_ASK,		// Implemented
	PRMEANING_BADPW,	// Implemented
	PRMEANING_IPBLOCK,	// Implemented
	PRMEANING_NONEW,	// Implemented
	PRMEANING_BADNAME,	// Implemented
	PRMEANING_REGFORM,	// Implemented
	PRMEANING_BUSY,		// Implemented
	PRMEANING_DEMO		// Unused
};


// Player kick reason constants

enum Kick_Reasons
{
	KICK_SYSTEM,		// Other reason
	KICK_FLOOD,			// Player command-flooded
	KICK_BAN			// Player was banned (Or BanID..)
};


// Player pass block constants

enum Block_Constants
{
	BLOCK_OK,			// Decide to block or not
	BLOCK_PASS			// Player may pass blocks
};

// Macros

LONG GetLong(BYTE * Message, const SHORT Offset)
{	// Get a LONG from a char *
	return *(LONG*)&(Message[Offset]);
}

SHORT GetShort(BYTE * Message, const SHORT Offset)
{	// Get a SHORT from a char *
	return *(SHORT*)&(Message[Offset]);
}

LONG GetLong(char * Message, const SHORT Offset)
{	// Get a LONG from a char *
	return *(LONG*)&(Message[Offset]);
}

SHORT GetShort(char * Message, const SHORT Offset)
{	// Get a SHORT from a char *
	return *(SHORT*)&(Message[Offset]);
}

BYTE GetByte(char * Message, const SHORT Offset)
{	// Get a BYTE from a char *
	return *(BYTE*)&(Message[Offset]);
}

char * FillSpace(char * RawText, unsigned int DesiredLength)
{
	size_t StrLen = strlen(RawText);

	char * NewText = new char[DesiredLength + 1];
	memset(NewText, 0, DesiredLength + 1);
	memset(NewText, 32, DesiredLength);
	memcpy(NewText, RawText, StrLen);

	return NewText;
}

bool MatchString(const char * lparam, const char * rparam, int length)
{	// same as strcmp(), I think
	for (SHORT index = 0; lparam[index] == rparam[index]; index++)
	{
		if (!lparam[index] || (index == (length - 1)))
			return true;
	}

	return false;
}

inline int TrimInt(int Value, int Limit)
{
	if (Value > Limit)
		return Limit;
	else
		return Value;
}

bool ColonParse(char * Text, LONG Length, SHORT &LeftLength, char * &RightText)
{	// Parse colon-deliminated text
	for (SHORT index = 0; index < TrimInt(Length, 31); index++)
	{
		// Unexpectedly hit the end of the string
		if (Text[index] == 0)
			return false;

		// Found a colon!  Yeeehaw
		if (Text[index] == ':')
		{
			LeftLength = index;
			RightText = &Text[++index];
			return true;
		}
	}

	return false;
}

void SpaceTrim(BYTE * Dest, char * Src, int Length)
{	// Copy Src -> Dest, removing leading and trailing
	// spaces & allowing only one space in a series
	bool SeenSpace = false;
	bool SeenChar = false;
	SHORT i = 0;

	for (SHORT index = 0; index < Length; index++)
	{
		// Hit the end of the string
		if (Src[index] == 0)
			break;

		// Remove all extra spaces
		if (Src[index] == ' ')
		{
			SeenSpace = true;
		}
		else
		{
			if (SeenSpace && SeenChar)
				Dest[i++] = ' ';
			Dest[i++] = Src[index];

			SeenSpace = false;
			SeenChar = true;
		}
	}

	Dest[i] = 0;
}

bool ParseIP(char * Text, LONG Length, LONG &StartIP, LONG &EndIP, LONG &Offset)
{
	// ?ban -k -r |123.123.123.123-234.234.234.234 Catid|
	// ?ban -k -r |123.123.123.123 Catid|
	// ?ban -k -r |123.123.123.* Catid|

	bool Stream = true;
	int  IPNode = 0;
	char buffer[5];
	int  buflen = 0;

	for (SHORT index = 0; index < Length; index++)
	{
		// Unexpectedly hit the end of the string
		if (!Text[index])
			return false;

		switch (Text[index])
		{
		case ' ':
			// Terminate parser
			{
				if (IPNode != 3)
					return false;

				buffer[buflen] = 0;
				int val = atoi(buffer);
				if (val > 255)
					return false;
				if (Stream)
				{
					((char*)&StartIP)[IPNode] = (char)val;
					EndIP = StartIP;
				}
				else
					((char*)&EndIP)[IPNode] = (char)val;

				Offset = index + 1;
				return true;
			}
			break;

		case '-':
			// Start on the next IP stream
			{
				if (IPNode != 3)
					return false;

				buffer[buflen] = 0;
				int val = atoi(buffer);
				if (val > 255)
					return false;
				((char*)&StartIP)[IPNode] = (char)val;

				Stream = false;
				IPNode = 0;
				buflen = 0;
			}
			break;

		case '.':
			// Switch address node
			{
				if (IPNode > 2)
					return false;

				buffer[buflen] = 0;
				int val = atoi(buffer);
				if (val > 255)
					return false;
				if (Stream)
					((char*)&StartIP)[IPNode] = (char)val;
				else
					((char*)&EndIP)[IPNode] = (char)val;
				IPNode++;
				buflen = 0;
			}
			break;

		case '*':
			// Auto-range & Quit
			{
				EndIP = StartIP;

				Offset = index;

				for (int i = IPNode; i < 4; i++)
				{
					((unsigned char*)&StartIP)[i] = 0;
					((unsigned char*)&EndIP)[i] = 255;
					Offset += 2;
				}

				if (Offset >= Length)
					return false;
			}
			return true;

		default:
			// Add numbers to the buffer
			{
				if (buflen == 3)
					return false;

				if (Text[index] >= 48 && Text[index] <= 57)
				{
					buffer[buflen++] = Text[index];
				}
				else
				{
					return false;
				}
			}
		};
	}

	return false;
}

tm * Localize(time_t * RemoteTime, SHORT RemoteTZB)
{
	// Store timezone for recall later
	LONG MyTZ = _timezone;

	// Generate tm structure from shifted time_t
	_timezone = RemoteTZB * 60;
	tm * TimeZone = localtime(RemoteTime);

	// Recall timezone settings
	_timezone = MyTZ;

	// Return tm
	return TimeZone;
}

bool CheckRange(unsigned long IP, unsigned long RangeStart, unsigned long RangeEnd)
{
	for (int i = 0; i < 4; i++)
	{
		unsigned char RS = ((unsigned char*)&RangeStart)[i];
		unsigned char RE = ((unsigned char*)&RangeEnd)[i];
		unsigned char RI = ((unsigned char*)&IP)[i];

		if ( (RI < RS) || (RI > RE) )
			return false;
	}

	return true;
}

bool InvalidTimeZoneBias(short TZB)
{
	return (((TZB % 60) && ((TZB != 210) && (TZB != -210) && (TZB != -270) && (TZB != -330) && (TZB != -570))) || (TZB > 780) || (TZB < -780));
}

bool InvalidZoneLogin(LONG GroupID, LONG ServerID, LONG ScoreID)
{
	return ((GroupID <= 0) || (ServerID <= 0) || (ScoreID <= 0));
}

SHORT GetYear(tm * Date)
{
	return (Date->tm_year + 1900);
}

bool InvalidMachineID(LONG MachineID)
{
	if (MachineID == 101) return false;

	if (MachineID < 0) return true;

	if (MachineID < 2000) return true;

	return false;
}

void ShiftString(char * mStr, int offset)
{
	register len = strlen(mStr);

	memcpy(&mStr[offset], &mStr[offset + 1], len - offset);
}

bool InvalidUsername(char * Username)
{
	if ( (Username[0] > 47 && Username[0] < 58) ||
		 (Username[0] > 64 && Username[0] < 91) ||
		 (Username[0] > 96 && Username[0] < 123) )		// The first character is alphanumeric
	{
		int i = 1;
		while (Username[i])
		{
			if (Username[i] < 32 || Username[i] > 127 || Username[i] == ':' || Username[i] == '%' || Username[i] == ',')	// Other characters are printable
				return true;

			i++;
		}
	}
	else
	{
		return true;
	}

	return false;
}

bool InvalidSquadname(char * Squadname)
{
	int i = 0;
	while (Squadname[i])
	{
		if ( (Squadname[i] < 32) || (Squadname[i] > 127) )		// All characters are printable
			return true;

		i++;
	}

	return false;
}


// Packet constructors

SHORT MakeSessionKey		(BYTE * Message, LONG Key)
{
	Message[0] = 0;
	Message[1] = 2;
	*(LONG*)&Message[2] = Key;

	return 6;
}

SHORT MakeACK				(BYTE * Message, LONG ID)
{
	Message[0] = 0;
	Message[1] = 4;
	*(LONG*)&Message[2] = ID;

	return 6;
}

SHORT MakePlayerKick		(BYTE * Message, LONG ID, LONG Reason)
{
	Message[0] = 8;
	*(LONG*)&Message[1] = ID;
	*(LONG*)&Message[5] = Reason;

	return 9;
}

SHORT MakeZoneRecycle		(BYTE * Message)
{
	Message[0] = 4;
	Message[1] = 0;
	*(LONG*)&Message[2] = 1;
	*(LONG*)&Message[6] = 2;

	return 10;
}

SHORT MakeZoneShutdown		(BYTE * Message)
{
	Message[0] = 2;
	Message[1] = 0;
	*(LONG*)&Message[2] = 1;
	*(LONG*)&Message[6] = 2;

	return 10;
}

SHORT MakePlayerResponse	(BYTE * Message, BYTE Meaning, LONG PlayerID,
								 char * PlayerName, BYTE * SquadName, BYTE * Banner,
								 LONG TotalSeconds, SHORT CYear, SHORT CMonth,
								 SHORT CDay, SHORT CHour, SHORT CMinute,
								 SHORT CSecond, LONG ScoreID, SHORT Wins,
								 SHORT Losses, SHORT GoalCount, LONG Points,
								 LONG EventPoints, LONG Extra1, LONG Extra2)
{
	memset(Message, 0, 192);

	Message[0]				= 1;
	Message[1]				= Meaning;
	*(LONG*)&Message[2]		= PlayerID;
	if (PlayerName)
	{
		int len = strlen(PlayerName);

		if (len > 24)
			len = 24;

		memcpy(&Message[6], PlayerName, len);
	}
	if (SquadName)	memcpy(&Message[30], SquadName, 24);
	if (Banner)	memcpy(&Message[54], Banner, 96);
	*(LONG*)&Message[150]	= TotalSeconds;
	*(SHORT*)&Message[154]	= CYear;
	*(SHORT*)&Message[156]	= CMonth;
	*(SHORT*)&Message[158]	= CDay;
	*(SHORT*)&Message[160]	= CHour;
	*(SHORT*)&Message[162]	= CMinute;
	*(SHORT*)&Message[164]	= CSecond;
	*(LONG*)&Message[166]	= Extra1;
	*(LONG*)&Message[170]	= ScoreID;
	*(LONG*)&Message[174]	= Extra2;
	*(SHORT*)&Message[178]	= Wins;
	*(SHORT*)&Message[180]	= Losses;
	*(SHORT*)&Message[182]	= GoalCount;
	*(LONG*)&Message[184]	= Points;
	*(LONG*)&Message[188]	= EventPoints;

	return 192;
}

SHORT MakeRemotePrivate		(BYTE * Message, BYTE * Body, LONG PacketLength)
{
	Message[0] = 3;
	Message[1] = 184;
	Message[2] = 11;
	Message[3] = 0;
	Message[4] = 0;
	Message[5] = 2;
	Message[6] = 2;		// Sound
	memcpy(&Message[7], Body, PacketLength - 11);

	return (SHORT)(PacketLength - 4);
}

SHORT MakeZoneMessage		(BYTE * Message, BYTE * Body)
{
	register int len = strlen((char*)Body);

	Message[0] = 3;
	Message[1] = 184;
	Message[2] = 11;
	Message[3] = 0;
	Message[4] = 0;
	Message[5] = 2;
	Message[6] = 2;		//Sound
	memcpy(&Message[7], Body, len);
	Message[len + 7] = 0;

	return (len + 8);
}

SHORT MakeSyncResponse		(BYTE * Message, LONG ClientTime)
{
	Message[0] = 0;
	Message[1] = 5;
	*(long*)&Message[2] = ClientTime;
	*(long*)&Message[6] = GetTickCount() / 10;

	return 9;
}

SHORT MakeArenaPrivate		(BYTE * Message, LONG UserID, const BYTE * ArenaMessage)
{
	Message[0]			= 9;
	*(LONG*)&Message[1]	= UserID;
	memcpy(&Message[5], ArenaMessage, strlen((char*)ArenaMessage) + 1);

	return (strlen((char*)ArenaMessage) + 6);
}

SHORT MakeChatPrivate		(BYTE * Message, LONG UserID, BYTE Channel, const BYTE * ChatMessage)
{
	Message[0]			= 10;
	*(LONG*)&Message[1]	= UserID;
	Message[5]			= Channel;
	memcpy(&Message[6], ChatMessage, strlen((char*)ChatMessage) + 1);

	return (strlen((char*)ChatMessage) + 7);
}

// SubSpace Encryption Table

class SSET
{
public:
	long SSKey;
	char SSBuf[520];

	SSET()
	{
		SSKey = 0;
	}
};


// Utilities

LONG ResolveHostname(char * Hostname)
{
	LONG RetIP = inet_addr(Hostname);

	if (RetIP == -1)
	{
		hostent * host = gethostbyname(Hostname);

		if (host)
			RetIP = *((LONG*)*(host->h_addr_list));
		else
			RetIP = -1;
	}

	return RetIP;
}


/* WhoIs server plugin */
void LeaveWhoIsServer();
bool GetBlock(LONG IP, LONG &StartIP, LONG &EndIP);
void ConnectWhoIsServer();

unsigned int WhoIsLookupSocket = -1;

void ConnectWhoIsServer()
{
	if (WhoIsLookupSocket != -1)
		return;

	LONG WhoIsLookupIP = ResolveHostname("192.149.252.22");
	if (WhoIsLookupIP == -1)
	{
		printf("Error resolving (whois.arin.net)\n");
		return;
	}

	// Socket creation
	WhoIsLookupSocket = socket (2, SOCK_STREAM, 0);
	if (WhoIsLookupSocket == -1)
	{
		printf("Can't create socket\n");
		return;
	}

	bool KeepAlive = true;
	setsockopt(WhoIsLookupSocket, IPPROTO_TCP, SO_KEEPALIVE, (char*)&KeepAlive, sizeof(KeepAlive));

	// Connection
	unsigned int len = 16;
	struct sockaddr_in adr;

	memset (&adr, 0, 16);
	adr.sin_family		= 2;
	adr.sin_port		= htons (43);
	adr.sin_addr.s_addr	= WhoIsLookupIP;

	if (connect (WhoIsLookupSocket, (sockaddr*)&adr, len) == -1)
	{
		printf("Error connecting (whois.arin.net)\n");
		LeaveWhoIsServer();
		return;
	}
}

bool GetBlock(LONG IP, LONG &StartIP, LONG &EndIP)
{
	ConnectWhoIsServer();

	if (WhoIsLookupSocket == -1)
		return false;

	// Request
	int Result;
	in_addr ip;
	char buff[1024];
	memset (buff, 0, 1024);

	ip.s_addr = IP;
	sprintf (buff, "%s\n", inet_ntoa(ip));

	Result = send (WhoIsLookupSocket, buff, strlen(buff), 0);
	if (!Result || (Result == SOCKET_ERROR))
	{
		LeaveWhoIsServer();
		return false;
	}

	// Get reponse
	Result = recv (WhoIsLookupSocket, buff, 1024, 0);
	while (Result && (Result != SOCKET_ERROR))
	{
		// Parse response
		char * pNetblock = strstr (buff, "Netblock:");
		if (pNetblock)
		{
			char * pLow		= pNetblock + 10;
			char * pHigh	= strchr (pLow, '-') + 2;
			char * pEnd		= strchr (pHigh, 10);
			*(pHigh-3)		= 0;
			*(pEnd)			= 0;
			StartIP			= inet_addr (pLow);
			EndIP			= inet_addr (pHigh);
			LeaveWhoIsServer();
			return true;
		}

		// Get reponse
		memset (buff, 0, 1024);
		Result = recv (WhoIsLookupSocket, buff, 1024, 0);
	}

	// Result returning
	LeaveWhoIsServer();
	return false;
}

void LeaveWhoIsServer()
{
	closesocket (WhoIsLookupSocket);

	WhoIsLookupSocket = -1;
}


// Database entry overhead

class DatabaseEntry
{
public:
	LONG LoadID;	// Object index on disk
};

// Brute-force protection

class RecentSource
{
public:
	long	IP;
	int		Count;

	RecentSource(long nIP)
	{
		IP				= nIP;
		Count			= 1;
	}

	char CheckLogins()
	{	// Returns true if the counter is too high
		Count++;

		if (Count >= 20)
		{
			Count = 9;
			return 2;
		}
		else if (Count >= 10)
			return 1;
		else
			return 0;
	}

	bool RemoveCounter()
	{	// Returns true if the counter is depleted
		Count -= 19;
		if (Count > 0)
		{
			return false;
		}
		else
		{
			Count = 0;
			return true;
		}
	}
};


// Packet contents

class PacketStruct
{
public:
	/* Contents */
	Host	        * Source;	// Source of packet
	SHORT			  Length;	// Length of the packet
	BYTE			* Message;	// Contents of the packet (unencrypted)

	/* Parsers */
	PacketStruct(Host * NewSource, BYTE * NewMessage, SHORT NewLength)
	{
		Message = new BYTE[NewLength];
		memcpy(Message, NewMessage, NewLength);
		Length = NewLength;
		Source = NewSource;
	}

	~PacketStruct()
	{
		delete Message;
	}
};


// Packet contents

class PostlogNode
{
public:
	/* Contents */
	SHORT	Length;		// Length of the packet
	BYTE  * Message;	// Contents of the packet (encrypted)
	LONG	PacketID;	// ID of packet to be ACK'd
	LONG	FirstSend;	// 'Tick count from first send
	LONG	LastSend;	// 'Tick count from last send

	/* Parsers */
	void SetTime()
	{
		LastSend = GetTickCount();
	}

	PostlogNode(BYTE * NewMessage, SHORT NewLength, LONG NewPacketID)
	{	// NewMessage encrypted PRIOR to call
		Length		= NewLength;
		PacketID	= NewPacketID;
		LastSend	= GetTickCount();
		FirstSend	= LastSend;
		Message		= new BYTE[NewLength];
		memcpy(Message, NewMessage, Length);
	}

	~PostlogNode()
	{
		delete Message;
	}
};


// Mutex

class Mutex
{
	HANDLE hMutex;		// Mutex handle

public:
	Mutex()
	{
		hMutex = CreateMutex(NULL, false, NULL);
	}

	virtual ~Mutex()
	{
		CloseHandle(hMutex);
		hMutex = NULL;
	}

	void LockClass() const
	{
		WaitForSingleObject(hMutex, INFINITE);
	}

	void UnlockClass() const
	{
		ReleaseMutex(hMutex);
	}
};

enum Top10Type
{
	//TOP10_Rating,
	TOP10_Average,
	TOP10_Score,
	TOP10_Wins,
	TOP10_Losses,
	TOP10_Ratio
};

struct Top10ListEntry
{
	long Points;
	long Wins;
	long Losses;
	long Average;
	double Ratio;
	char Name[32];
};

// Disk-Save Formatted Score

struct SavedScore
{
	long	EntryID;
	long	ScoreID;
	long	UserID;

	short	Total;		// Not sure

	short	Wins;
	short	Losses;
	short	Flags;
	long	Points;
	long	FlagPoints;

	SavedScore()
	{
	}
};

struct SavedGMessage
{
	long	UserID;
	long	FromID;
	long	MsgID;
	time_t	PostTime;
	BYTE	Special;
	char	Message[256];

	SavedGMessage()
	{
	}
};

// Disk-Save Formatted Squad

struct SavedSquad
{
	long	EntryID;

	char	Name[32];
	char	Password[32];

	long	OwnerID;

	SavedSquad()
	{
	}
};


// Disk-Save Formatted Operator

struct SavedOp
{
	long	OpID;
	long	OwnerID;
	long	GroupID;
	long	ScoreID;
	char	OpLevel;
	char	Password[32];

	SavedOp()
	{
	}
};


// Disk-Save Formatted Banfree

struct SavedBanfree
{
	long	BanfreeID;
	long	OwnerID;
	long	GroupID;
	long	ScoreID;
	BYTE    Level;

	SavedBanfree()
	{
	}
};

// Disk-Save Formatted Zone

struct SavedZone
{
	long	ScoreID;
	long	GroupID;
	char	Password[32];
	char	Bantext[128];

	SavedZone()
	{
	}
};


// Disk-Save Formatted Ban

struct SavedBan
{
	long	EntryID;

	long	UserID;

	time_t	LastLogin;
	time_t	CreationDate;
	time_t	Timer;

	long	IPStart;
	long	IPEnd;
	long	MachineID;
	long	HardwareID;

	long	CreatorID;
	long	ScoreID;
	long	GroupID;
	char	OpLevel;

	char	Name[32];
	char	Comment[128];

	BYTE	DenyBanfree;

	SavedBan()
	{
	}
};

// Disk-Save Formatted Registered User

struct SavedReg
{
	long	OwnerID;

	long	HardwareID;
	char	Notification;

	char	Name[32];
	char	Email[32];
	char	Comment[128];

	SavedReg()
	{
	}
};


// Disk-Save Formatted User

struct SavedUser
{
	long	EntryID;

	char	Name[32];
	char	Password[32];

	char	Banner[96];

	long	SquadID;

	long	TotalSeconds;
	long	TotalLogins;

	long	LastIP;

	long	MachineID;

	time_t	LastLoginDate;
	time_t	CreationDate;

	short	TZB;
	short	NoBlock;

	SavedUser()
	{
	}
};


// Flood protection

class FloodProtection
{
public:
	DWORD LastTicks;
	DWORD SequenceTicks;
	signed int FloodedMessages;

	FloodProtection()
	{
		SequenceTicks = GetTickCount();
		LastTicks = 0;
		FloodedMessages = 0;
	}

	BYTE CheckFlood()
	{
		DWORD ticks = GetTickCount();

		if ((ticks - LastTicks) < 1500)
			FloodedMessages++;

		if ((ticks - SequenceTicks) > 3000)
		{
			FloodedMessages -= (ticks - SequenceTicks) / 3000;
			if (FloodedMessages < 0)
				FloodedMessages = 0;
			SequenceTicks = ticks;
		}

		LastTicks = ticks;

		if (FloodedMessages > 4)
			return 2;
		else if (FloodedMessages > 2)
			return 1;
		else
			return 0;

	}
};


// UserList node (In use w/ Chat channels)

class LUserNode : public FloodProtection
{
public:
	/* Player identifiers */
	Host	 * zone;			// Current zone (for chatters)
	User	 * player;			// Player data
	Score	 * score;			// Score data
	Chat	 * channels[10];	// Comm channels
	LONG	   PlayerID;		// Zone-level ID
	bool	   ChatNotify;		// Notify when users enter chat?
	bool	   ZoneNotify;		// Notify when zones connect?
	bool	   BanNotify;		// Notify when banned users enter?
	bool	   Ghost;			// Invisible to ?find (BSysOps: Invisible in chat)
	Operator * OpStatus;		// Operator status

	/* Initialization */
	LUserNode(Host * NewZone, User * NewPlayer, Score * NewScore, LONG UserID);

	/* Management */
	DWORD	   KickTime;		// Kick if GetTickCount() >= KickTime
	LONG	   KickReason;		// Kick reason
	LONG	   KickBanID;		// Link to ban text to display

	void Kick(LONG Delay, LONG Reason, LONG BanID);
	void CheckKick();

	void CheckMsg(LONG Delay);

	DWORD CheckMsgTime;
	/* Broadcast system */
	void SendChat(BYTE Channel, const BYTE * Message);
	void SendArena(const BYTE * Message);
	void SendChat(BYTE Channel, const char * Message);
	void SendArena(const char * Message);

	/* Chat */
	Chat * JoinChat(BYTE * Name, BYTE ID, bool Notify);
	void ExitChat(BYTE ID, bool Notify);
	void ExitAllChats(bool Notify);

	/* Squad database */
	bool JoinSquad(BYTE * Name, BYTE * Password);
	void ExitSquad();
};


// Chat Participant

class Chatter
{
public:
	/* Identifiers */
	LUserNode	* Player;	// Who he is
	BYTE		  ID;		// Which chat ID to broadcast with

	Chatter(LUserNode * NewPlayer, BYTE NewID);
};


class ChatPremitList
{
public:
	LONG UserID;

	ChatPremitList(LONG NewUserID);
};

// Database Chat Entry

class Chat
{
public:
	/* Chat information */
	BYTE	Name[32];						// Name of chat
	_linkedlist <Chatter> Participants;		// Chat participants
	_linkedlist <ChatPremitList> Allowed;	// Allowed people on channel
	LONG Owner;
	bool NeedPremission;
	char WelcomeMsg[128];

	/* Functions */
	bool PlayerInChat(LUserNode * player);
	bool PlayerAllowed(LONG UserID);

	/* Initialization */
	Chat(BYTE * NewName, LONG NewOwner);
};

class ChatSystem
{
public:
	Chat * CreateChat(BYTE * Name, LONG Owner);
	void DestroyChat(Chat * chat);
};

// Database Squad Entry

class Squad : public DatabaseEntry
{
public:
	/* Squad identifiers */
	BYTE	Name[32];		// Name of squad
	BYTE	Password[32];	// Password to squad

	/* Squad information */
	User * Owner;							// Reference to owner
	LONG OwnerID;							// Used only for parsing the .DAT files
	_linkedlist <User> Participants;		// Squad participants

	/* Initialization */
	Squad(BYTE * NewName, BYTE * NewPassword, User * NewOwner);
	void SetPassword(char * pw);
	_listnode <User> * FindUser(char * Name);

	~Squad()
	{
		Participants.Clear(true);
	}
};


// Database Registration Entry

class RegForm : public DatabaseEntry
{
public:
	// Contents
	char	Name[32];		// Player's actual name
	char	Email[32];		// Player's E-mail address
	char	Comment[128];	// User details
	LONG	HardwareID;		// Hardware ID
	User  * Owner;			// Owner ID
	bool	NotifyChat;		// Make NotifyChat setting immortal
	bool	NotifyBan;		// Make NotifyBan setting immortal
	bool	NotifyZone;		// Make NotifyZone setting immortal

	RegForm(User * NewOwner, char * NewName, char * NewEmail, char * NewComment, LONG NewHardwareID, bool NewNotifyChat, bool NewNotifyBan, bool NewNotifyZone);
};


// Database Zone Entry

class Zone : public DatabaseEntry
{
public:
	// Contents
	LONG	GroupID;		// Zone group identifier
	LONG	ScoreID;		// Zone score identifier
	char	Password[32];	// Zone password
	char	BanText[128];	// ^Banned players get this message

	Zone(LONG NewGroupID, LONG NewScoreID, char * NewPassword, char * NewBanText);
};

// Database Banfree Entry

class Banfree : public DatabaseEntry
{
public:
	// Contents
	LONG       BanfreeID;
	LONG	   Owner;		// Guy who is banfreed
	LONG	   GroupID;		// Banfree group identifier
	LONG	   ScoreID;		// Banfree score identifier
	BYTE       Level;       // Banfree level
	User	 * Player;		// Computed on-demand

	Banfree(LONG NewBanfreeID, LONG NewGroupID, LONG NewScoreID, LONG NewOwner, BYTE Level);
};

// Database GMessage Entry

class GMessage : public DatabaseEntry
{
public:
	LONG	UserID;
	LONG	FromID;
	LONG	MsgID;
	time_t	PostTime;
	BYTE	Special;
	char	Message[256];

	GMessage(LONG NewUserID, LONG NewFromID, LONG NewMsgID, time_t NewPostTime, BYTE NewSpecial, char * NewMessage);
};

// Database Operator Entry

class Operator : public DatabaseEntry
{
public:
	// Contents
	User  * Owner;			// Owner ID
	LONG	OpID;			// Operator ID
	LONG	GroupID;		// Server Group. X = access to one set of zones. 0 = network op
	LONG	ScoreID;		// Server. X = access in one zone. 0 = use a group of servers
	BYTE	Level;			// Operator access level
	char	Password[32];	// Login password
	LONG	LoginCounter;	// Number of logged in users under this name

	Operator(User * NewOwner, LONG NewOpID, LONG NewGroupID, LONG NewScoreID, BYTE NewLevel, char * NewPassword);

	void AddLogin()
	{
		LoginCounter++;
	}
	void RemoveLogin()
	{
		LoginCounter--;
	}
};


// Database User Entry

class User : public DatabaseEntry
{
public:
	/* Player identifiers */
	BYTE	Name[32];			// Name
	BYTE	Password[32];		// Password

	/* Player information */
	LONG	IP;					// IP source route
	LONG	MacID;				// Machine ID
	SHORT	TimeZoneBias;		// Time zone
	BYTE	Banner[96];			// Banner bytes
	SHORT	PassBlock;			// Can he get past bans?

	/* References */
	Squad	* squad;			// Squad information
	RegForm * regform;			// Registration information

	/* Date records */
	time_t	LastLogin;			// Last user login date in time_t format
	time_t	CreationDate;		// User creation date in time_t format

	/* Usage */
	LONG	TotalSeconds;		// Total seconds online
	LONG	TotalLogins;		// Total logins

	/* Initialization */
	User(BYTE * NewName, BYTE * NewPassword);

	void SetCreationDate();
	void SetLastLogin();

	void SetSquad(Squad * NewSquad);
	void SetRegForm(RegForm * NewRegForm);
	void SetBanner(BYTE * NewBanner);
	void SetBanInfo(LONG NewIP, LONG NewMacID, SHORT NewTZB);

	bool CheckLastAccess(BYTE * NewPassword);
};


// Database Score Entry

class Score : public DatabaseEntry
{
public:
	/* Score */
	SHORT	Wins;				// Number of kills
	SHORT	Losses;				// Number of deaths
	SHORT	Flags;				// Number of flags
	LONG	Points;				// Points
	LONG	EventPoints;		// Points from objectives

	/* Identification */
	User  * Owner;				// User in possession of the score
	LONG	ScoreID;			// Server in possession of the score

	void SetScore(SHORT W, SHORT L, SHORT F, LONG P, LONG EP);
	Score(User * NewOwner, LONG SID);
};


// Database Ban Entry

class Ban : public DatabaseEntry
{
public:
	// Entry information
	char	Name[32];		// User name
	char	Comment[128];	// Ban comment
	time_t	LastLogin;		// Last login
	time_t	CreationDate;	// Creation date
	time_t	Timer;			// Date when it should be removed
	LONG	LastType;		// Last login's piece of trigger info

	// Ban ID's
	LONG	UserID;			// Banned user's ID
	LONG	IPStart;		// Start of IP range
	LONG	IPEnd;			// End of IP range
	LONG	MachineID;		// Machine ID
	LONG	HardwareID;		// Hardware ID

	// Owner
	User  * Creator;		// Ban creator link
	LONG	OwnerID;		// Ban creator's ID
	LONG	BanID;			// Universal Ban ID namespace address
	LONG	GroupID;		// Network/Group-wide
	LONG	ScoreID;		// Group/Zone-wide
	BYTE	OpLevel;		// Operator level

	BYTE	DenyBanfree;	// Deny banfree

	void ChangeIP(LONG NewIPStart, LONG NewIPEnd);
	void ChangeMacID(LONG NewMachineID);
	void ChangeHwID(LONG NewHardwareID);
	void ChangeComment(const char * NewComment);
	void ChangeName(const char * NewName);
	void ChangeOwner(User * NewCreator);

	void UpdateLogin(LONG NewType);

	Ban(LONG NewBanID, LONG NewGroupID, LONG NewScoreID, BYTE NewOpLevel, const char * NewName, const char * NewComment, LONG NewIPStart, LONG NewIPEnd, LONG NewMachineID, LONG NewHardwareID, User * NewCreator, time_t NewCreation, time_t NewAccess, time_t NewTimer, LONG NewUserID, BYTE NewDenyBanfree);
};


// Database

class Database
{
public:
	/* Pseudo-random number generator */
	LFSR_PRNG Rnd;

	void BanUser(User * Player, LONG GrohostupID, LONG ScoreID, BYTE OpLevel, const char * Comment, LONG IPStart, LONG IPEnd, LONG MachineID, LONG HardwareID, LONG Days, User * Creator, BYTE DenyBanfree);
	void UnbanUser(Ban * Entry);
	Ban * CheckBan(LONG UserID, LONG IP, LONG MachineID, LONG HardwareID, LONG GroupID, LONG ScoreID);
	Ban * CheckBanID(LONG HardwareID);

	Banfree * CheckBanfree(LONG UserID, LONG ScoreID);

	/* Lists */
	_linkedlist <RecentSource>	SrcDatabase;		// Mortal
	_linkedlist <Chat>			ChatDatabase;		// Mortal
	_linkedlist <User>			UserDatabase;		// Immortal
	_linkedlist <Squad>			SquadDatabase;		// Immortal
	_linkedlist <Score>			ScoreDatabase;		// Immortal
	_linkedlist <RegForm>		RegDatabase;		// Immortal
	_linkedlist <Ban>			BanDatabase;		// Immortal
	_linkedlist <Operator>		OpDatabase;			// Immortal
	_linkedlist <Zone>			ZoneDatabase;		// Immortal
	_linkedlist <Banfree>		BanfreeDatabase;	// Immortal
	_linkedlist <GMessage>		GMessageDatabase;	// Immortal

	ChatSystem ChatSys;

	LONG NextUniversalBanID;						// Immortal
	LONG NextUniversalMsgID;						// Immortal
	LONG NextUniversalOpID;							// Immortal
	LONG NextUniversalBanfreeID;					// Immortal
	User * BannedGuy;

	/* Banfrees */
	User * ComputeBanfree(Banfree * banfree);

	/* Database core */
	void LoadFromDisk();
	void SaveToDisk();
	char NetworkName[32];
	char NetworkBanText[128];

	/* Copy scores */
	void LoadScores(Host * Zone);

	/* Source database */
	_listnode <RecentSource> * FindSource(long IP);
	void DecrementSourceCounters();

	/* Password system */
	friend void HashPassword(BYTE * Password);
	bool ValidatePassword(User * player, BYTE * Password);
	bool ValidatePassword(Squad * squad, BYTE * Password);

	/* Reg database */
	void AddReg(User * Owner, char * Name, char * Email, char * Comment, LONG HardwareID);

	/* Zone database */
	_listnode <Zone> * FindZone(LONG ScoreID);

	/* Operator database */
	_listnode <Operator> * FindOp(LONG OpOffset);
	_listnode <Operator> * FindOp(BYTE * Name);

	/* User database */
	User * AddUser(BYTE * Name, BYTE * Password, LONG IP, LONG MacID, SHORT TZB);
	_listnode <User> * FindUser(BYTE * Name);
	_listnode <LUserNode> * FindLUser(BYTE * Name, _listnode <Host> * HostNode, bool UseExactLength);

	/* Squad database */
	void AddSquad(BYTE * Name, BYTE * Password, User * Owner);
	void DeleteSquad(Squad * squad);
	Squad * FindSquad(BYTE * Name);
	void BroadcastEntering(LUserNode * user, Chat * chat)
	{
		// Make user entering message
		char buffer[512];
		strcpy(buffer, (char*)&user->player->Name[0]);
		strcat(buffer, " entering ");
		strcat(buffer, (char*)&chat->Name[0]);

		// Broadcast entering message
		_listnode <Chatter> * parse = chat->Participants.head;

		while (parse)
		{
			Chatter * chatter = parse->item;

			if (chatter->Player->ChatNotify && chatter->Player != user)
				chatter->Player->SendChat(chatter->ID, buffer);

			parse = parse->next;
		}
	}
	void BroadcastLeaving(LUserNode * user, Chat * chat)
	{
		// Make user leaving message
		_string s;
		s = (char*)&user->player->Name[0];
		s += " left ";
		s += (char*)&chat->Name[0];

		// Broadcast leaving message
		_listnode <Chatter> * parse = chat->Participants.head;

		while (parse)
		{
			if ((parse->item->Player->ChatNotify) && (parse->item->Player != user))
				parse->item->Player->SendChat(parse->item->ID, s.Text);
			parse = parse->next;
		}
	}

	/* Settings */
	SHORT	Port;
	bool	LowPriority;
	bool	AllowNegativeMacID;
	bool	AllowNewUsers;
	bool	AskNewUsers;
	bool	DisableHelp;
	bool	EncryptMode;
	LONG	KickOutDelay;
	LONG	MaxQueueForLogin;
	LONG	PeriodicSave;
	DWORD	PacketLossLimit;
	char  * BillingPassword;
	char  * SysopPassword;
	LONG	AccountReclaimDelay;
	char	BNews[5][80];

	bool ValidateBillingPassword(LONG ScoreID, BYTE * Password);
	Operator * ValidateSysopPassword(LUserNode * Player, BYTE * Password);

	void LoadSettings();

	Database()
	{
		Rnd.Seed(GetTickCount());
		BillingPassword	= NULL;
		SysopPassword	= NULL;

		// Set up an account used only by banned players
		BYTE Name[32];
		BYTE Password[32];
		memset(Name, 0, 32);
		memset(Password, 0, 32);
		strcpy((char*)&Name[0], "^Banned");
		BannedGuy = new User(Name, Password);
	}
	~Database()
	{
		if (BillingPassword)	delete BillingPassword;
		if (SysopPassword)		delete SysopPassword;

		delete BannedGuy;
	}

	/* Chat database */
	Chat * FindChat(BYTE * Name);
} DATABASE;		// Master database


// Chunks! (Recving)

class IncomingChunk
{
public:
	char  * Contents;
	SHORT	Length;

	IncomingChunk()
	{
		Contents = NULL;
		Length = 0;
	}

	~IncomingChunk()
	{
		if (Contents)
			Clear();
	}

	void Set(char * Message, SHORT mLen)
	{
		Length = mLen;
		Contents = new char[mLen];
		memcpy(Contents, Message, mLen);
	}

	void Append(char * Message, SHORT mLen)
	{
		if (Contents)
		{
			char * OldContents = Contents;
			SHORT NewLength = Length + mLen;
			Contents = new char[NewLength];
			memcpy(Contents, OldContents, Length);
			memcpy(&Contents[Length], Message, mLen);
			Length = NewLength;
		}
		else
		{
			Set(Message, mLen);
		}
	}

	void Clear()
	{
		delete Contents;
		Contents = NULL;
		Length = 0;
	}
};


// Host

class Host
{
	/* Handlers */
	friend bool HandleUnknown			(PacketStruct * Packet);	// Unknown message type (debug output)
	friend bool HandleSpecial			(PacketStruct * Packet);	// Privelaged types
	friend bool HandlePing				(PacketStruct * Packet);	// Ping packet
	friend bool HandleLogin				(PacketStruct * Packet);	// Enter the network
	friend bool HandlePlayerEntering	(PacketStruct * Packet);	// Password packet
	friend bool HandlePlayerLeaving		(PacketStruct * Packet);	// Score update
	friend bool HandleRemotePrivate		(PacketStruct * Packet);	// :Remote: & **net-wide commands
	friend bool HandleChatMessage		(PacketStruct * Packet);	// ;X; command
	friend bool HandleLogMessage		(PacketStruct * Packet);	// [SERVER.INI]LogMessages=1 packets
	friend bool HandleServerError		(PacketStruct * Packet);	// Error logging service
	friend bool HandleChangeBanner		(PacketStruct * Packet);	// Banner update
	friend bool HandleStatus			(PacketStruct * Packet);	// Player information(?)
	friend bool HandleCommand			(PacketStruct * Packet);	// ?*** commands
	friend bool HandleRegistration		(PacketStruct * Packet);	// Registration form data
	friend bool HandleSpecial			(PacketStruct * Packet);	// Special session types

	friend bool HandleUnknownSpecial	(PacketStruct * Packet);	// Unhandled packet
	friend bool HandleEncryptRequest	(PacketStruct * Packet);	// Request to encrypt communication
	friend bool HandleEncryptResponse	(PacketStruct * Packet);	// Session key
	friend bool HandleReliable			(PacketStruct * Packet);	// Message with a reliable header
	friend bool HandleACK				(PacketStruct * Packet);	// ACK message
	friend bool HandleSyncRequest		(PacketStruct * Packet);	// Synchronization request
	friend bool HandleSyncResponse		(PacketStruct * Packet);	// Synchronization response
	friend bool HandleDisconnect		(PacketStruct * Packet);	// Bye!
	friend bool HandleCluster			(PacketStruct * Packet);	// Cluster packet
	friend bool HandleSmallChunkBody	(PacketStruct * Packet);	// Host is blowing chunks
	friend bool HandleSmallChunkTail	(PacketStruct * Packet);	// More chunks
	friend bool HandleBigChunk			(PacketStruct * Packet);	// Host is blowing really big chunks

	friend Database;
	friend LUserNode;

	/* Identifiers */
	BYTE	Name[32];		// Server name
	LONG	TimeStamp;		// Used to find 'ticks since the last recv'd packet
	LONG	SyncTime;		// If this is non-zero, SyncResponses will be checked for validity
	bool	BroadcastChat;	// Allow inter-zone chat?

	void SetName(BYTE * NewName, LONG NewGroupID, LONG NewScoreID)
	{
		memset(Name, 0, 32);
		memcpy(Name, NewName, 24);
		ScoreID = NewScoreID;
		GroupID = NewGroupID;
	}

	/* Users */
	_linkedlist <LUserNode> UserList;		// Currently logged in (Losers)
	_linkedlist <Score> ScoreDatabase;		// Set of players with scores (D�zers)
	bool AcceptScores;						// Accept score update requests?

	_listnode <LUserNode> * FindByID(LONG PlayerID)
	{
		_listnode <LUserNode> * parse = UserList.head;

		while (parse)
		{
			if (parse->item->PlayerID == PlayerID)
				return parse;

			parse = parse->next;
		}

		return NULL;
	}

	_listnode <LUserNode> * Find(char * Name, bool UseExactLength = false)
	{
		_listnode <LUserNode> * parse = UserList.head;

		if (UseExactLength)
		{
			while (parse)
			{
				if (_strnicmp((char*)&parse->item->player->Name[0], Name, (strlen((char*)&parse->item->player->Name[0]) > strlen(Name) ? strlen((char*)&parse->item->player->Name[0]) : strlen(Name))) == 0)
					return parse;

				parse = parse->next;
			}
		}
		else
		{
			while (parse)
			{
				if (_strnicmp((char*)&parse->item->player->Name[0], Name, strlen(Name)) == 0)
					return parse;

				parse = parse->next;
			}
		}

		return NULL;
	}

	/* Session */
	DWORD	LastTicks;	// Used to calculate time since the last packet was recv'd
	DWORD	Ping;		// Mean ping-time
	DWORD	DiffTime;	// Difference in local and remote 'tick counts
	LONG	CloseID;	// When this ACK_ID is recv'd, the subbill closes the session

	void SetTicks()
	{
		LastTicks = GetTickCount();
	}

	/* Encryption */
	SSET SSEncrTable;
	LONG SentKey;

	void _cdecl InitializeEncryption(LONG Key);

	/* Reliable packet system */
	LONG						RemoteStep;	// Next packet number to-be-processed
	_linkedlist <PacketStruct>	Backlog;	// List of packets-to-be-processed

	LONG						LocalSend;	// Last packet to be sent
	_linkedlist <PostlogNode>	Sendlog;	// List of packets being sent

	LONG						LocalPost;	// Next packet number queued for delivery
	_linkedlist <PostlogNode>	Postlog;	// List of packets queued for sending

	void CheckPostlog(LONG PacketID);

	bool CheckBacklog(PacketStruct * Packet)
	{
		// ACK & Process backlog
		BYTE buffer[6];

		// Acknowledge message
		MakeACK(buffer, GetLong(Packet->Message, 2));
		Send(buffer, 6, false);

		// Process if expected
		if (GetLong(Packet->Message, 2) == RemoteStep)
		{
			// Handle given packet
			Router.CallHandler(Packet->Message[6], new PacketStruct(this, &Packet->Message[6], Packet->Length - 6));

			// Increment expected packet ID
			RemoteStep++;

			_listnode <PacketStruct> * parse = Backlog.head;

			// Search for next packet in sequence
			while (parse)
			{
				PacketStruct * ps = parse->item;

				// Check if found packet was expected
				if (GetLong(ps->Message, 2) == RemoteStep)
				{
					// Handle logged packet
					Router.CallHandler(ps->Message[6], new PacketStruct(this, &ps->Message[6], ps->Length - 6));

					// Delete backlog entry
					Backlog.Delete(parse, false);

					// Increment expected packet ID
					RemoteStep++;

					// Reset node pointer, and search again
					parse = Backlog.head;
				}
				else
					parse = parse->next;
			}

			return true;
		}

		// Add if a packet has been lost
		if (GetLong(Packet->Message, 2) > RemoteStep)
		{
			_listnode <PacketStruct> * parse = Backlog.head;

			// Search for same packet in sequence
			while (parse)
			{
				// Ignore if sent packet has been recv'd
				if (GetLong(parse->item->Message, 2) == GetLong(Packet->Message, 2))
					return true;

				parse = parse->next;
			}

			// Log recv'd packet otherwise
			Backlog.Append(Packet);
			return false;
		}

		// Ignore if already seen
		return true;
	}

	/* Chunks */
	IncomingChunk IncomingChunks;	// 08 - 09
	IncomingChunk BIGChunks;		// 0a <Length>

	/* Mode */
	bool	ClientMode;
	DWORD	KickOffLimit;
	bool	HasSession;

	/* Handlers */
	_jumptable <PacketStruct> Router;
	_jumptable <PacketStruct> SpecialRouter;

public:
	/* Touch these and you will die a horrible, slow and painful death */
	LONG ScoreID;						// Score ID
	LONG GroupID;						// Group ID
	sockaddr Remote;					// Host address
	WinsockSession * Socket;			// Session with winsock

	friend class WinsockSession;		// For broadcast abilities and stuff

	/* API */
	void Encrypt(BYTE * Message, SHORT Length);
	void Decrypt(BYTE * Message, SHORT Length);

	void SetHandler(BYTE Type, bool (*function)(PacketStruct *))
	{
		Router.RegisterHandler(Type, function);
	}

	void SetSpecialHandler(BYTE Type, bool (*function)(PacketStruct *))
	{
		SpecialRouter.RegisterHandler(Type, function);
	}

	void ClearHandlers()
	{
		Router.ClearHandlers();
		Router.RegisterHandler(0, HandleSpecial);
	}

	void SetSocket(WinsockSession * NewSocket)
	{
		Socket = NewSocket;
	}

	void SetHost(sockaddr * NewRemote)
	{
		Remote = *NewRemote;
	}

	LONG SetKey(LONG Key)
	{
		if (!Key) return 0;

		InitializeEncryption(Key);

		return Key;
	}

	void SetMode(bool NewClientMode, LONG NewKickOffLimit)
	{
		ClientMode		= NewClientMode;
		KickOffLimit	= NewKickOffLimit;
	}

	void Route(BYTE * Message, SHORT Length)
	{
		Router.CallHandler(Message[0], new PacketStruct(this, Message, Length));
		SetTicks();
	}

	void Recycle()
	{
		BYTE buffer[9];

		MakeZoneRecycle(buffer);

		Send(buffer, 9, true);
	}

	void Shutdown()
	{
		BYTE buffer[9];

		MakeZoneShutdown(buffer);

		Send(buffer, 9, true);
	}

	void Disconnect(bool Wait);

	void Send(BYTE * Message, SHORT Length, bool Reliable);

	bool ResendQueue();

	bool PollKickoff()
	{
		// Check if players have been scheduled for kickoff
		_listnode <LUserNode> * uparse = UserList.head;
		while (uparse)
		{
			uparse->item->CheckKick();
			uparse = uparse->next;
		}

		// Check if zone is scheduled for kickoff
		if ((HasSession == false) && (GetTickCount() - LastTicks) >= (KickOffLimit))
			return true;
		else
			return false;
	}

	bool ValidateSource(sockaddr * Source)
	{
		if (((*(sockaddr_in*)Source).sin_addr.S_un.S_addr == (*(sockaddr_in*)&Remote).sin_addr.S_un.S_addr) &&
			((*(sockaddr_in*)Source).sin_port			  == (*(sockaddr_in*)&Remote).sin_port))
			return true;
		else
			return false;
	}

	Host()
	{
		SSEncrTable.SSKey	= 0;
		RemoteStep			= 0;
		LocalSend			= 0;
		LocalPost			= 0;
		ClientMode			= false;
		KickOffLimit		= 60000;
		GroupID				= -1;
		ScoreID				= -1;
		CloseID				= 0xFFFFFFFF;		// extend this time somehow
		Ping				= 0;
		AcceptScores		= true;
		SyncTime			= 0;
		BroadcastChat		= true;
		HasSession			= false;

		for (int i = 0; i < 256; i++)
		{
			SetSpecialHandler(i, HandleUnknownSpecial);
			SetHandler(i, HandleUnknown);
		}

		SetSpecialHandler(1,	HandleEncryptRequest);
		SetSpecialHandler(7,	HandleDisconnect);

		SetHandler(0,	HandleSpecial);
	}

	~Host()
	{
		ScoreDatabase.Clear(true);
	}
};


// Session with winsock

class WinsockSession : public Mutex
{
	/* Threads */
	HANDLE hResend,		// Handle to the resend thread
		   hReceive;	// Handle to the receive thread

	void ResendThread()
	{
		DWORD SourceCounters = GetTickCount();

		// Resend queued messages
		while (hResend)
		{
			Sleep((DATABASE.LowPriority ? 100 : 50));

			// Surf the server list
LockClass();

			// Remove one counter from each session limiter
			if ((GetTickCount() - SourceCounters) > 45000)
			{
				DATABASE.DecrementSourceCounters();
				SourceCounters = GetTickCount();
			}

			// Resend messages and Poll for kickoff
			_listnode <Host> * parse = HostList.head;

			while (parse)
			{
				Host * host = parse->item;
				parse = parse->next;			// Hop destruction

				if (host->PollKickoff())
				{
					// Display connection termination
					long IP = (*(sockaddr_in*)&host->Remote).sin_addr.S_un.S_addr;
					int Port = htons((*(sockaddr_in*)&host->Remote).sin_port);
					printf("Session timeout: %i.%i.%i.%i:%i\n",			(int)(((BYTE*)&IP)[0]),
																		(int)(((BYTE*)&IP)[1]),
																		(int)(((BYTE*)&IP)[2]),
																		(int)(((BYTE*)&IP)[3]),
																		Port);

					// Boot 'im
					host->Disconnect(false);
				}
				else
				{
					// Resend messages
					host->ResendQueue();
				}
			}

UnlockClass();
		}
	}

	void RecvThread()
	{
		// Receive queued messages
		sockaddr SockAddy;
		int FromLen = 16;
		BYTE buffer[520];

		while (hReceive)
		{
			if (DATABASE.LowPriority)
				Sleep(10);

			int datalen = recvfrom(SocketID, (char*)buffer, 520, 0, &SockAddy, &FromLen);

			if (datalen > 0)
			{
				// Find or Create host
LockClass();

				Host * zone = FindHost(&SockAddy);

				if (zone)
				{
					zone->Decrypt(buffer, datalen);
					zone->Route(buffer, datalen);
				}
				else
					printf("Connection request ignored: Host slots full.\n");

UnlockClass();
			}
		}
	}

	friend DWORD WINAPI CallResendThread(WinsockSession * ThisInstance)
	{
		ThisInstance->ResendThread();

		return 0;
	}

	friend DWORD WINAPI CallRecvThread(WinsockSession * ThisInstance)
	{
		ThisInstance->RecvThread();

		return 0;
	}

public:
	/* Description */
	SOCKET	SocketID;	// Session with winsock
	SHORT	LocalPort;	// Local port

	/* Connected servers */
	_linkedlist <Host> HostList;	// Host list

	void PrintHosts()
	{
		_listnode <Host> * parse = HostList.head;

		printf("Host list:\n");
		while (parse)
		{
			long IP = (*(sockaddr_in*)&parse->item->Remote).sin_addr.S_un.S_addr;
			int Port = htons((*(sockaddr_in*)&parse->item->Remote).sin_port);
			printf("%i.%i.%i.%i:%i\n",	(int)(((BYTE*)&IP)[0]),
										(int)(((BYTE*)&IP)[1]),
										(int)(((BYTE*)&IP)[2]),
										(int)(((BYTE*)&IP)[3]),
										Port);

				_listnode <LUserNode> * player = parse->item->UserList.head;

				while (player)
				{
					printf("%s, ", player->item->player->Name);

					player = player->next;
				}

				printf("\n");

			parse = parse->next;
		}
		printf("\n");
	}

	void Broadcast(BYTE * Message, int Length, bool Reliable)
	{
		_listnode <Host> * parse = HostList.head;

		while (parse)
		{
			parse->item->Send(Message, Length, Reliable);

			parse = parse->next;
		}
	}

	Host * FindHost(sockaddr * Source)
	{
		_listnode <Host> * parse = HostList.head;

		while (parse)
		{
			// Validate by source route
			if (parse->item->ValidateSource(Source))
				return (parse->item);

			parse = parse->next;
		}

		// Don't make a session if we are full
		if (HostList.Total > DATABASE.MaxQueueForLogin)
			return (NULL);

		// Create a new zone
		Host * zone_link = new Host;
		zone_link->SetHost(Source);
		zone_link->SetSocket(this);
		zone_link->SetMode(false, DATABASE.KickOutDelay);
		HostList.Append(zone_link);

		// Return pointer to host
		return (zone_link);
	}

	Host * FindHost(LONG ScoreID)
	{
		_listnode <Host> * parse = HostList.head;

		while (parse)
		{
			if (parse->item->ScoreID == ScoreID)
				return (parse->item);

			parse = parse->next;
		}

		return NULL;
	}

	/* Initialization */
	void Initialize(SHORT NewPort)
	{
LockClass();
		// Kill old session
		if (hResend || hReceive)
			Terminate();

		// Create session with Winsock
		WSADATA wsaData;
		WSAStartup(0x202, &wsaData);

		// Register a socket
		LocalPort = NewPort;

		sockaddr_in sa;
					sa.sin_addr.S_un.S_addr	= 0;
					sa.sin_family			= 2;
					sa.sin_port				= htons(NewPort);

		SocketID = socket(2, 2, 0);
		bind(SocketID, (sockaddr *) &sa, sizeof(sockaddr));

		// Start threads
		unsigned long iThread;

		hResend		= CreateThread(NULL, 0, (unsigned long (__stdcall *)(void *))&CallResendThread,	this, 0, &iThread);
		hReceive	= CreateThread(NULL, 0, (unsigned long (__stdcall *)(void *))&CallRecvThread,	this, 0, &iThread);
UnlockClass();
	}

	void Terminate()
	{
LockClass();
		// Kill threads
		unsigned long ExitCode;

		GetExitCodeThread(hResend, &ExitCode);
		TerminateThread(hResend, ExitCode);

		GetExitCodeThread(hReceive, &ExitCode);
		TerminateThread(hReceive, ExitCode);

		// Unregister threads
		CloseHandle(hResend);
		CloseHandle(hReceive);

		BYTE buffer[2];
		buffer[0] = 0;
		buffer[1] = 7;
		Broadcast(buffer, 2, false);

		HostList.Clear(false);

		// Unregister socket
		closesocket(SocketID);

		// Destroy session with Winsock
		WSACleanup();

		hResend = hReceive = 0;
UnlockClass();
	}

	WinsockSession() : Mutex()
	{
		hResend = hReceive = 0;
	}

	~WinsockSession()
	{
		if (hResend || hReceive)
			Terminate();
	}
};


// SubGame host implementation

bool Host::ResendQueue()
{
	_listnode <PostlogNode> * parse = Sendlog.head;
	DWORD Ticks = GetTickCount();

	// Spider the Sendlogs
	while (parse)
	{
		PostlogNode * post = parse->item;

		// Make sure no reliable packets have been lost
		if ((Ticks - post->FirstSend) >= DATABASE.PacketLossLimit)
		{
			printf("%i - Connection broken: Reliable packet lost\n", ScoreID);

			Disconnect(false);

			return true;
		}

		// Validate that over 1 second has passed since last send
		if ((Ticks - post->LastSend) >= (Ping << 1))
		{
			// Resend
			sendto(Socket->SocketID, (char*)post->Message, post->Length, 0, &Remote, 16);

			// Set 'ticks for last resend
			post->SetTime();
		}

		parse = parse->next;
	}

	return false;
}

void Host::Disconnect(bool Wait)
{
	BYTE buffer[2];
	buffer[0] = 0;
	buffer[1] = 7;
	Send(buffer, 2, true);

	if (Wait)
		CloseID = LocalPost;
	else
	{
		if (ScoreID != -1)
		{
			_string s;
			s  = (char*)&Name[0];
			s += "(";
			s += ScoreID;
			s += ") - Disconnected.";

			// Broadcast notification
			_listnode <Host> * hparse = Socket->HostList.head;

			while (hparse)
			{
				_listnode <LUserNode> * uparse = hparse->item->UserList.head;

				if (hparse->item != this)
				{
					while (uparse)
					{
						LUserNode * user = uparse->item;

						if (user->ZoneNotify && user->OpStatus && (user->OpStatus->Level <= 2))
						{
							if (user->OpStatus->GroupID == 0)
								user->SendChat(0, s.Text);
							else if (user->OpStatus->GroupID == hparse->item->GroupID)
							{
								if (user->OpStatus->ScoreID == 0)
									user->SendChat(0, s.Text);
								else if (user->OpStatus->ScoreID == hparse->item->ScoreID)
									user->SendChat(0, s.Text);
							}
						}

						uparse = uparse->next;
					}
				}
				else
				{
					while (uparse)
					{
						LUserNode * user = uparse->item;

						if (user->OpStatus)
							user->OpStatus->RemoveLogin();

						user->ExitAllChats(true);

						uparse = uparse->next;
					}
				}

				hparse = hparse->next;
			}
		}

		Socket->HostList.Delete(this, true);
	}
}

void Host::Send(BYTE * Message, SHORT Length, bool Reliable)
{
	if (Reliable)
	{
		SHORT	BufferLength	= Length + 6;
		BYTE  * buffer			= new BYTE[BufferLength];

		// 00 03 ID ID ID ID <Message>
		buffer[0]			= 0;
		buffer[1]			= 3;
		*(long*)&buffer[2]	= LocalPost;
		memcpy(&buffer[6], Message, Length);

		// Encrypt packet
		Encrypt(buffer, BufferLength);

		if (Sendlog.Total < 3)
		{
			// Send it
			sendto(Socket->SocketID, (char*)buffer, BufferLength, 0, &Remote, 16);

			// Add encrypted packet to the resend list
			LocalSend = LocalPost;
			Sendlog.Append(new PostlogNode(buffer, BufferLength, LocalPost));
		}
		else
		{
			// Add encrypted packet to the message queue
			Postlog.Append(new PostlogNode(buffer, BufferLength, LocalPost));
		}

		// Set next ID
		LocalPost++;

		// Delete temporary workspace
		delete buffer;
	}
	else
	{
		// Encrypt packet
		Encrypt(Message, Length);

		// Send it
		sendto(Socket->SocketID, (char*)Message, Length, 0, &Remote, 16);
	}
}

void Host::CheckPostlog(LONG PacketID)
{
	if (PacketID == CloseID)
	{
		// This host was scheduled for disconnection
		Disconnect(false);
	}

	if (PacketID > LocalSend)
	{
		// Someone is toying with his source route!
		long IP = (*(sockaddr_in*)&Remote).sin_addr.S_un.S_addr;
		short Port = htons((*(sockaddr_in*)&Remote).sin_port);
		printf("Malformed ACK from %i.%i.%i.%i:%i\n",			(int)(((BYTE*)&IP)[0]),
																(int)(((BYTE*)&IP)[1]),
																(int)(((BYTE*)&IP)[2]),
																(int)(((BYTE*)&IP)[3]),
																Port);

		Disconnect(false);

		return;
	}

	// Clear a SendLog entry
	_listnode <PostlogNode> * parse = Sendlog.head;

	while (parse)
	{
		if (parse->item->PacketID == PacketID)
		{
			// Update ping time
			if (Ping)
				Ping = (Ping + (GetTickCount() - parse->item->LastSend)) >> 1;
			else
				Ping = GetTickCount() - parse->item->LastSend;

			// Clear the entry
			Sendlog.Delete(parse, false);

			// Queue up another packet
			_listnode <PostlogNode> * pparse = Postlog.head;

			++LocalSend;

			while (pparse)
			{
				PostlogNode * post = pparse->item;

				if (post->PacketID == LocalSend)
				{
					// Send it if it is next
					sendto(Socket->SocketID, (char*)post->Message, post->Length, 0, &Remote, 16);

					// Set 'ticks for last send
					post->SetTime();

					// Move it to the Sendlog
					Sendlog.Append(post);
					Postlog.Delete(pparse, true);

					return;
				}

				pparse = pparse->next;
			}

			return;
		}

		parse = parse->next;
	}
}

// Implementations

#include "encryption.h"
#include "rpc.h"
#include "command.h"
#include "dbase.h"
