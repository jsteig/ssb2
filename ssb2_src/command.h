//#define ERROR_TRAPPER

#include "globals.h"

bool HandleCommand			(PacketStruct * Packet)
{
	_listnode <LUserNode> * user = Packet->Source->FindByID(GetLong(Packet->Message, 1));	// Trade DBase ID for Adress-space ID

	if (user)
	{
		// Exists

		// Generate message processing parameters
		LUserNode * player	= user->item;
		char	  * command	= (char*)&Packet->Message[5];
		SHORT		length	= Packet->Length - 6;

		printf("%i - %s>%s\n", Packet->Source->ScoreID, player->player->Name, command);

		// Check for command floods
		switch (player->CheckFlood())
		{
		case 2:
			// Flood detected
			{
				printf("%i - Command flooder kicked: %s\n", Packet->Source->ScoreID, player->player->Name);
				player->SendArena("NOTICE: You are being disconnected for command flooding!");
				player->Kick(0, KICK_FLOOD, -1);

				return true;
			}
			break;
		case 1:
			// Flood warning
			{
				player->SendArena("WARNING: No command flooding!");
			}
			break;
		};

/* Error trapper */
#ifdef ERROR_TRAPPER

try
{

#endif
/* Error trapper */
			if (MatchString(command, "?version", 8))
			{
				_string s;
				s = "Current biller version: ";
				s += VERSION;
				player->SendArena(s.Text);
				return true;				
			}

			if (MatchString(command, "?squadownlist", 13))
			{
				_string s;
				bool Found = false;
				_listnode <Squad> * parse = DATABASE.SquadDatabase.head;
				
				while (parse)
				{
					if (parse->item->Owner->LoadID == player->player->LoadID)
					{
						Found = true;

						if (s.Length > 14) s += ", "; else s = "Owned squads: ";
						s += (char*)&parse->item->Name[0];
						
						if (s.Length > 96)
						// Send line
						{
							player->SendArena(s.Text);
							s = "";
						}
					}
					parse = parse->next;
				}

				if (s.Length > 14) player->SendArena(s.Text);

				if (!Found)
				{
					player->SendArena("You do not own any squads");
					return true;
				}

				return true;
			}

			if (MatchString(command, "?squadonline", 12))
			{
				Squad * squad;

				if (length < 14)
				// Users own squad
				{
					if (!player->player->squad)
					// Player got no squad
					{
						player->SendArena("You are not in a squad");
						return true;
					}
					squad = player->player->squad;
				}
				else
				// Specified squad
				{
					BYTE Name[32];
					memset(Name, 0, 32);
					SpaceTrim(Name, &command[13], 31);

					if (Name[0] == 0)
					{
						player->SendArena("This commands requires parameters. Type ?man squadonline for details");
						return true;
					}

					if (InvalidUsername((char*)&Name[0]))
					{
						player->SendArena("Invalid squad name");
						return true;
					}

					squad = DATABASE.FindSquad(Name);
					if (!squad)
					{
						player->SendArena("Squad not found");
						return true;
					}
				}

				_string s;
				bool Found = false;

				_listnode <User> * parse = squad->Participants.head;
				while (parse)
				{
					_listnode <Host> * hparse = (Packet->Source->Socket)->HostList.head;
					while (hparse)
					{
						// Find user
						_listnode <LUserNode> * user = hparse->item->Find((char*)&parse->item->Name[0], true);

						if (user && !user->item->Ghost)
						{
							Found = true;
							if (s.Length != 0) s += ", ";
							s += (char*)&user->item->player->Name[0];

							if (s.Length >= 96)
							// Send line
							{
								player->SendArena(s.Text);
								s = "";
							}
							break;
						}

						// Try next zone
						hparse = hparse->next;
					}
					parse = parse->next;
				}

				if (s.Length != 0) player->SendArena(s.Text);

				if (!Found)
				{
					// All ghosted or not online
					player->SendArena("Nobody in that squad is online");
					return true;
				}

				return true;
			}

			if (MatchString(command, "?c-own", 6))	// Own a chat
			{
				if (length < 8)
				{
					player->SendArena("This commands requires parameters. Type ?man c-own for details");
					return true;
				}

				BYTE Counter = 0;
				_listnode <Chat> * parse = DATABASE.ChatDatabase.head;
				while (parse)
				{
					if (parse->item->Owner == player->player->LoadID) Counter++;
					parse = parse->next;
				}

				if (Counter >= 9)
				{
					player->SendArena("You cannot own more than 9 chat channels");
					printf("c-own : 9 chats already owned");
					return true;
				}

				BYTE Name[32];
				memset(Name, 0, 32);
				SpaceTrim(Name, &command[7], 31);

				if (Name[0] == 0)
				{
					player->SendArena("This commands requires parameters. Type ?man c-own for details");
					return true;
				}

				if (InvalidUsername((char*)&Name[0]))
				{
					player->SendArena("Invalid chat name");
					return true;
				}

				parse = DATABASE.ChatDatabase.head;
				while (parse)
				{
					if (_strnicmp((char*)&parse->item->Name[0], (char*)Name, 31) == 0)	// Ah, the channel already exists... too bad. Can�t own it
					{
						if (parse->item->Owner)
						{
							player->SendArena("A chat with that name is already owned");
							return true;
						}
						else
						{
							if (parse->item->Participants.Total)
							{
								player->SendArena("Kicking users from chat...");
								
								_listnode <Chatter> * kickparse = parse->item->Participants.head;
								while (kickparse)
								{									
									DATABASE.BroadcastLeaving(kickparse->item->Player, kickparse->item->Player->channels[kickparse->item->ID]);
									kickparse->item->Player->channels[kickparse->item->ID] = NULL;
									parse->item->Participants.Delete(kickparse, false);
									kickparse = parse->item->Participants.head;
								}
								DATABASE.ChatSys.DestroyChat(parse->item);

								break;
							}
							else
							{
								player->SendArena("WARNING: An error has occured! The chat channel does not have players in it, but it exists. Attempting to delete channel. Please retry your request");
								DATABASE.ChatSys.DestroyChat(parse->item);
								return true;
							}
						}
						return true;	//Should never reach this point
					}
					parse = parse->next;
				}
				
				_string s;
				if (DATABASE.ChatSys.CreateChat(Name, player->player->LoadID))
				{
					s = (char*)&Name[0];
					s += " grabbed successfully!";
				}
				else
				{
					s = "Failed to grab ";
					s += (char*)&Name[0];
				}

				player->SendArena(s.Text);
				return true;
			}

			if (MatchString(command, "?c-plist", 8))
			{
				if (length < 10)
				{
					player->SendArena("This command requires parameters. Type ?man c-plist for details");
					return true;
				}

				BYTE Name[32];
				memset(Name, 0, 32);
				SpaceTrim(Name, &command[9], 31);

				if (Name[0] == 0)
				{
					player->SendArena("This command requires parameters. Type ?man c-plist for details");
					return true;
				}

				Chat * chat = DATABASE.FindChat(Name);

				if (!chat)
				{
					player->SendArena("Unknown chat channel");
					return true;
				}

				if (!chat->Owner)
				{
					player->SendArena("The chat channel is not owned");
					return true;
				}

				if (chat->Owner != player->player->LoadID)
				{
					player->SendArena("Only the chat channel's owner can view allowed users");
					return true;
				}

				_string s;
				BYTE Counter = 0;
				bool Found = false;

				_listnode <ChatPremitList> * parse = chat->Allowed.head;
				while (parse)
				{
					Found = true;
					_listnode <User> * uparse = DATABASE.UserDatabase.head;
					while (uparse)
					{
						if (uparse->item->LoadID == parse->item->UserID)
						{
							if (s.Length) s += ", ";
							s += (char*)&uparse->item->Name[0];
							Counter++;
							if (Counter >= 4)
							{
								player->SendArena(s.Text);
								s = "";
								Counter = 0;
							}

							break;
						}
						uparse = uparse->next;
					}

					parse = parse->next;
				}

				if (Counter)
				{
					player->SendArena(s.Text);
				}

				if (!Found)
				{
					player->SendArena("There is nobody allowed on the chat channel");
					return true;
				}
				return true;
			}

			if (MatchString(command, "?c-release", 10))
			{
				if (length < 12)
				{
					player->SendArena("This command requires parameters. Type ?man c-release for details");
					return true;
				}

				BYTE Name[32];
				memset(Name, 0, 32);
				SpaceTrim(Name, &command[11], 31);

				if (Name[0] == 0)
				{
					player->SendArena("This command requires parameters. Type ?man c-release for details");
					return true;
				}
				
				Chat * chat = DATABASE.FindChat(Name);
				if (chat)
				{
					if (!chat->Owner)
					{
						player->SendArena("That chat channel is not owned");
						return true;
					}
					
					if (chat->Owner != player->player->LoadID)
					{
						if (player->OpStatus)
						{
							if (player->OpStatus->Level <= 2)
							{
								if (player->OpStatus->Level != 0)
								{
									_listnode <Operator> * oparse = DATABASE.OpDatabase.head;
									while (oparse)
									{
										if (oparse->item->Owner)
										{
											if ((oparse->item->Owner->LoadID == chat->Owner) && (oparse->item->Level <= player->OpStatus->Level))
											{
												player->SendArena("The chat is owned by an operator with an accesslevel higher or equal to yours");
												return true;
											}
										}
										oparse = oparse->next;
									}
								}
							}
							else
							{
								player->SendArena("You need at least level 2 access to release chats owned by other players");
								return true;
							}
						}
						else
						{
							player->SendArena("You are not the owner of that chat channel");
							return true;
						}
					}

					chat->Owner = 0;
					chat->Allowed.Clear(false);

					player->SendArena("Chat channel released");

					if (chat->Participants.Total)
					{
						_listnode <Chatter> * parse = chat->Participants.head;
						while (parse)
						{
							parse->item->Player->SendChat(parse->item->ID, "This chat channel is no longer owned");
							parse = parse->next;
						}
					}
					else
					{
						DATABASE.ChatSys.DestroyChat(chat);
					}
					return true;
				}
				else
				{
					player->SendArena("Unknown chat channel");
					return true;
				}
				return true;
			}

			if (MatchString(command, "?c-welcome", 10))
			{
				if (length < 12)
				{
					player->SendArena("This command requires parameters. Type ?man c-welcome for details");
					return true;
				}

				BYTE WelcomeMsg[128];
				BYTE ChatName[32];
				memset(WelcomeMsg, 0, 128);
				memset(ChatName, 0, 32);
				
				SHORT LeftLength;
				char * RightText;

				if (ColonParse(&command[11], length - 11, LeftLength, RightText))
				{
					SpaceTrim(WelcomeMsg, RightText, 127);
					SpaceTrim(ChatName, &command[11], LeftLength);

					if (ChatName[0] == 0)
					{
						player->SendArena("This command requires parameters. Type ?man c-welcome for details");
						return true;
					}


					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						player->SendArena("The chat channel is not owned");
						return true;
					}

					if (cparse->Owner != player->player->LoadID)
					{
						player->SendArena("You are not the owner of the chat channel");
						return true;
					}

					memset(cparse->WelcomeMsg, 0, 128);
					memcpy(cparse->WelcomeMsg, WelcomeMsg, 128);

					_string s;
					s = (char*)&cparse->Name[0];
					s += " welcome message changed successfully";
					
					player->SendArena(s.Text);

					return true;
				}
				else
				{
					SpaceTrim(ChatName, &command[11], 31);

					if (ChatName[0] == 0)
					{
						player->SendArena("This command requires parameters. Type ?man c-welcome for details");
						return true;
					}

					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						player->SendArena("The chat channel is not owned");
						return true;
					}

					if (cparse->Owner != player->player->LoadID)
					{
						player->SendArena("You are not the owner of the chat channel");
						return true;
					}

					if (cparse->WelcomeMsg)
					{
						_string s;
						s = (char*)&cparse->Name[0];
						s += " welcome message: ";
						s += cparse->WelcomeMsg;
						player->SendArena(s.Text);
					}
					else
					{
						_string s;
						s = (char*)&cparse->Name[0];
						s += " does not have a welcome message";
					}

					return true;
				}
				return true;
			}
			
			if (MatchString(command, "?c-allow", 8))
			{
				if (length < 10)
				{
					player->SendArena("This command requires parameters. Type ?man c-allow for details");
					return true;
				}

				BYTE Name[32];
				BYTE ChatName[32];
				memset(Name, 0, 32);
				memset(ChatName, 0, 32);
				
				SHORT LeftLength;
				char * RightText;

				if (ColonParse(&command[9], length - 9, LeftLength, RightText))
				{
					SpaceTrim(Name, RightText, 31);
					SpaceTrim(ChatName, &command[9], LeftLength);

					if ((Name[0] == 0) || (ChatName[0] == 0))
					{
						player->SendArena("This command requires parameters. Type ?man c-allow for details");
						return true;
					}

					_listnode <User> * uparse = DATABASE.FindUser(Name);

					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!uparse)
					{
						player->SendArena("Unknown user");
						return true;
					}

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						player->SendArena("The chat channel is not owned");
						return true;
					}

					if (cparse->Owner != player->player->LoadID)
					{
						player->SendArena("You are not the owner of the chat channel");
						return true;
					}

					if (!cparse->NeedPremission)
					{
						player->SendArena("Players no not need premission to enter the channel");
						return true;
					}

					if (cparse->PlayerAllowed(uparse->item->LoadID))
					{
						player->SendArena("The player is already allowed on that chat channel");
						return true;
					}
					
					cparse->Allowed.Append(new ChatPremitList(uparse->item->LoadID));

					_string s;
					s = (char*)&uparse->item->Name[0];
					s += " is now allowed in ";
					s += (char*)&cparse->Name[0];

					player->SendArena(s.Text);
					return true;
				}
				else
				{
					player->SendArena("This command requires parameters. Type ?man c-allow for details");
					return true;					
				}
				return true;
			}

			if (MatchString(command, "?c-grant", 8))
			{
				if (length < 10)
				{
					player->SendArena("This command requires parameters. Type ?man c-grant for details");
					return true;
				}

				BYTE Name[32];
				BYTE ChatName[32];
				memset(Name, 0, 32);
				memset(ChatName, 0, 32);
				
				SHORT LeftLength;
				char * RightText;

				if (ColonParse(&command[9], length - 9, LeftLength, RightText))
				{
					SpaceTrim(Name, RightText, 31);
					SpaceTrim(ChatName, &command[9], LeftLength);

					if ((Name[0] == 0) || (ChatName[0] == 0))
					{
						player->SendArena("This command requires parameters. Type ?man c-grant for details");
						return true;
					}

					_listnode <User> * uparse = DATABASE.FindUser(Name);

					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!uparse)
					{
						player->SendArena("Unknown user");
						return true;
					}

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						player->SendArena("The chat channel is not owned");
						return true;
					}

					if (cparse->Owner != player->player->LoadID)
					{
						player->SendArena("You are not the owner of the chat channel");
						return true;
					}

					// Check if target already owns 9 chats
					LONG Counter = 0;
					_listnode <Chat> * parse = DATABASE.ChatDatabase.head;
					while (parse)
					{
						if (parse->item->Owner == uparse->item->LoadID) Counter++;
						parse = parse->next;
					}

					if (Counter >= 9)
					{
						player->SendArena("The target player already owns 9 chats. Cannot add more");
						return true;
					}

					cparse->Owner = uparse->item->LoadID;

					_listnode <Chatter> * chparse = cparse->Participants.head;
					while (chparse)
					{
						_string s;
						s = "This chat is now owned by ";
						s += (char*)&uparse->item->Name[0];
						chparse->item->Player->SendChat(chparse->item->ID, s.Text);
						chparse->item->Player->SendChat(chparse->item->ID, "Please re-enter chat");
						chparse->item->Player->ExitChat(chparse->item->ID, false);
						chparse = cparse->Participants.head;
					}

					player->SendArena("Chat ownership changed. Everyone kicked from chat");
					return true;
				}
				else
				{
					player->SendArena("This command requires parameters. Type ?man c-grant for details");
					return true;					
				}
				return true;
			}
			
			if (MatchString(command, "?c-needp", 8))
			{
				if (length < 10)
				{
					player->SendArena("This command requires parameters. Type ?man c-needp for details");
					return true;
				}

				BYTE Value[32];
				BYTE ChatName[32];
				memset(Value, 0, 32);
				memset(ChatName, 0, 32);
				
				SHORT LeftLength;
				char * RightText;

				if (ColonParse(&command[9], length - 9, LeftLength, RightText))
				{
					SpaceTrim(Value, RightText, 31);
					SpaceTrim(ChatName, &command[9], LeftLength);

					if (ChatName[0] == 0)
					{
						player->SendArena("This command requires parameters. Type ?man c-needp for details");
						return true;
					}

					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						player->SendArena("The chat channel is not owned");
						return true;
					}

					if (cparse->Owner != player->player->LoadID)
					{
						player->SendArena("You are not the owner of the chat channel");
						return true;
					}

					switch (Value[0])
					{
					case 'o':
					case 'O':
						if (cparse->NeedPremission = (Value[1] != 'f') && (Value[1] != 'F'))
							player->SendArena("Players now need premission to enter the channel");
						else
						{
							cparse->Allowed.Clear(false);
							player->SendArena("Players now do not need premission to enter the channel. The allowed player list has been cleared");
						}
						return true;
					case 'y':
					case 'Y':
					case '1':
						{
							cparse->NeedPremission = true;
							player->SendArena("Players now need premission to enter the channel");
						}
						return true;
					case 'n':
					case 'N':
					case '0':
						{
							cparse->NeedPremission = false;
							cparse->Allowed.Clear(false);
							player->SendArena("Players now do not need premission to enter the channel. The allowed player list has been cleared");
						}
						return true;
					};

					player->SendArena("Invalid value");

					return true;
				}
				else
				{
					SpaceTrim(ChatName, &command[9], 31);

					if (ChatName[0] == 0)
					{
						player->SendArena("This command requires parameters. Type ?man c-needp for details");
						return true;
					}

					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						player->SendArena("The chat channel is not owned");
						return true;
					}

					if (cparse->NeedPremission)
						player->SendArena("Premission is needed to enter the channel");
					else
						player->SendArena("Premission is not needed to enter the channel");
					return true;
				}
				return true;
			}

			if (MatchString(command, "?c-kick", 7))
			{
				if (length < 9)
				{
					player->SendArena("This command requires parameters. Type ?man c-kick for details");
					return true;
				}

				BYTE Name[32];
				BYTE ChatName[32];
				memset(Name, 0, 32);
				memset(ChatName, 0, 32);
				
				SHORT LeftLength;
				char * RightText;

				if (ColonParse(&command[8], length - 8, LeftLength, RightText))
				{
					SpaceTrim(Name, RightText, 31);
					SpaceTrim(ChatName, &command[8], LeftLength);

					if ((Name[0] == 0) || (ChatName[0] == 0))
					{
						player->SendArena("This command requires parameters. Type ?man c-kick for details");
						return true;
					}

					_listnode <LUserNode> * uparse = DATABASE.FindLUser(Name, Packet->Source->Socket->HostList.head, true);

					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!uparse)
					{
						player->SendArena("Unknown user");
						return true;
					}

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						if (player->OpStatus)
						{
							if (player->OpStatus->Level > 2)
							{
								player->SendArena("The chat channel is not owned. You must be a level 2 or higher operator to kick people off unowned chats");
								return true;
							}
						}
						else
						{
							player->SendArena("The chat channel is not owned. You must be a level 2 or higher operator to kick people off unowned chats");
							return true;
						}
					}

					if ((cparse->Owner) && (cparse->Owner != player->player->LoadID))
					{
						if (player->OpStatus)
						{
							if (player->OpStatus->Level > 2)
							{
								player->SendArena("You are not the owner of the chat channel. You must be a level 2 or higher operator to kick people off chats that you do not own");
								return true;
							}
						}
						else
						{
							player->SendArena("You are not the owner of the chat channel. You must be a level 2 or higher operator to kick people off chats that you do not own");
							return true;
						}
					}

					if (!cparse->PlayerInChat(uparse->item))
					{
						player->SendArena("The player is not in the chat channel");
						return true;
					}

					_listnode <Chatter> * parse = cparse->Participants.head;
					while (parse)
					{
						if (parse->item->Player->player->LoadID == uparse->item->player->LoadID)
						{
							bool Allow = true;
							if (player->OpStatus)
							{
								if ((parse->item->Player->OpStatus) && (player->OpStatus->Level))
								{
									if (parse->item->Player->OpStatus->Level <= player->OpStatus->Level) Allow = false;
								}
							}
							else
							{
								if (parse->item->Player->OpStatus) Allow = false;
							}

							if (!Allow)
							{
								player->SendArena("You do not have the authority to kick that player off the chat");
								char buffer[128];
								memset(buffer, 0, 128);
								strcat(buffer, (char*)&player->player->Name[0]);
								strcat(buffer, " tried to kick you off the chat");

								parse->item->Player->SendChat(parse->item->ID, buffer);
								return true;
							}

							char buffer[128];
							memset(buffer, 0, 128);
							strcat(buffer, "You are being kicked off the chat by ");
							strcat(buffer, (char*)&parse->item->Player->player->Name[0]);

							parse->item->Player->SendChat(parse->item->ID, buffer);
							parse->item->Player->ExitChat(parse->item->ID, true);

							_string s;
							s = (char*)&uparse->item->player->Name[0];
							s += " kicked off the chat";
							player->SendArena(s.Text);
							return true;
						}
						parse = parse->next;
					}
					return true;
				}
				else
				{
					player->SendArena("This command requires parameters. Type ?man c-kick for details");
					return true;					
				}
				return true;
			}

			if (MatchString(command, "?c-revoke", 9))
			{
				if (length < 11)
				{
					player->SendArena("This command requires parameters. Type ?man c-revoke for details");
					return true;
				}

				BYTE Name[32];
				BYTE ChatName[32];
				memset(Name, 0, 32);
				memset(ChatName, 0, 32);
				
				SHORT LeftLength;
				char * RightText;

				if (ColonParse(&command[9], length - 9, LeftLength, RightText))
				{
					SpaceTrim(Name, RightText, 31);
					SpaceTrim(ChatName, &command[9], LeftLength);

					if ((Name[0] == 0) || (ChatName[0] == 0))
					{
						player->SendArena("This command requires parameters. Type ?man c-revoke for details");
						return true;
					}

					_listnode <User> * uparse = DATABASE.FindUser(Name);

					Chat * cparse = DATABASE.FindChat(ChatName);

					if (!uparse)
					{
						player->SendArena("Unknown user");
						return true;
					}

					if (!cparse)
					{
						player->SendArena("Unknown chat channel");
						return true;
					}

					if (!cparse->Owner)
					{
						player->SendArena("The chat channel is not owned");
						return true;
					}

					if (cparse->Owner != player->player->LoadID)
					{
						player->SendArena("You are not the owner of the chat channel");
						return true;
					}

					if (!cparse->PlayerAllowed(uparse->item->LoadID))
					{
						player->SendArena("The player already cannot access the chat channel");
						return true;
					}

					if (uparse->item == player->player)
					{
						player->SendArena("You cannot revoke your own access");
						return true;
					}

					_listnode <ChatPremitList> * parse = cparse->Allowed.head;
					while (parse)
					{
						if (parse->item->UserID == uparse->item->LoadID)
						{
							cparse->Allowed.Delete(parse, false);
							break;
						}
						parse = parse->next;
					}

					_string s;
					s = (char*)&uparse->item->Name[0];
					s += " can no longer access ";
					s += (char*)&cparse->Name[0];

					player->SendArena(s.Text);

					_listnode <Chatter> * chparse = cparse->Participants.head;
					while (chparse)
					{
						if (chparse->item->Player->player == uparse->item)
						{
							chparse->item->Player->SendChat(chparse->item->ID, "You are no longer allowed on this channel");
							chparse->item->Player->ExitChat(chparse->item->ID, true);
							break;
						}
						chparse = chparse->next;
					}

					return true;
				}
				else
				{
					player->SendArena("This command requires parameters. Type ?man c-allow for details");
					return true;					
				}
				return true;
			}

			if (MatchString(command, "?c-list", 7))
			{
				_string s = "Owned chats: ";
				BYTE Counter = 0;

				_listnode <Chat> * parse = DATABASE.ChatDatabase.head;
				while (parse)
				{
					if (parse->item->Owner == player->player->LoadID)
					{
						if (Counter) s += ", ";
						s += (char*)&parse->item->Name[0];
						Counter++;
					}
					parse = parse->next;
				}

				if (Counter == 0) s = "You have no owned chats";
				player->SendArena(s.Text);

				if (Counter != 0)
				{
					s = "Total chats owned: ";
					s += Counter;
					player->SendArena(s.Text);
				}
				return true;
			}
	
			if (MatchString(command, "?coin", 5))
			{
				if (DATABASE.Rnd.GetNextNumber() % 2)
				{
					player->SendArena("Heads!");
				}
				else
				{
					player->SendArena("Tails!");
				}
				return true;
			}

			if (MatchString(command, "?top10", 6))
			{
				Top10Type TypeSelect = TOP10_Score;
				if (MatchString(command, "?top10 a", 8))
				{
					TypeSelect = TOP10_Average;
				}
				else if (MatchString(command, "?top10 r", 8))
				{
					TypeSelect = TOP10_Ratio;
				}
				else if (MatchString(command, "?top10 w", 8))
				{
					TypeSelect = TOP10_Wins;
				}
				else if (MatchString(command, "?top10 l", 8))
				{
					TypeSelect = TOP10_Losses;
				}

				Top10ListEntry TList[10];
				char i = 0;
				for (i = 0; i < 10; i++)
				{
					TList[i].Average = 0;
					TList[i].Losses = 0;
					memset(TList[i].Name, 0, 32); //45 = '-'
					memset(TList[i].Name, 45, 4);
					TList[i].Points = 0;
					TList[i].Wins = 0;
					TList[i].Ratio = 0;
				}

				_listnode <Score> * parse = DATABASE.ScoreDatabase.head;
				while (parse)
				{
					if (parse->item->ScoreID != Packet->Source->ScoreID)
					{
						parse = parse->next;
						continue;
					}
					char EntryID = -1;
					LONG i2 = 0;
					for (i = 9; i >= 0; i--)
					{
						switch (TypeSelect)
						{
						case TOP10_Average:
							if (!parse->item->Wins)
							{
								i2 = 0;
							}
							else
							{
								i2 = parse->item->Points / parse->item->Wins;
							}
							if (i2 >= TList[i].Average) EntryID = i;
							break;
						case TOP10_Losses:
							if (parse->item->Losses >= TList[i].Losses) EntryID = i;
							break;
						case TOP10_Ratio:
							if (!parse->item->Losses)
							{
								break;
							}
							else if ((parse->item->Losses) && (TList[i].Losses))
							{
								double D1, D2;
								D1 = (double)parse->item->Wins / (double)parse->item->Losses;
								D2 = (double)TList[i].Wins / (double)TList[i].Losses;
								if (D1 >= D2) EntryID = i;
							}
							else
							{
								EntryID = i;
							}
							break;
						case TOP10_Score:
							if ((parse->item->EventPoints + parse->item->Points) >= TList[i].Points) EntryID = i;
							break;
						case TOP10_Wins:
							if (parse->item->Wins >= TList[i].Wins) EntryID = i;
							break;
						};
					}
					if (EntryID == -1)
					{
						parse = parse->next;
						continue;
					}

					for (i = 9; i >= EntryID; i--)
					{
						if (EntryID == i)
						{
							if (parse->item->Wins)
							{
								TList[i].Average = (parse->item->Points / parse->item->Wins);
							}
							else
							{
								TList[i].Average = 0;
							}
							TList[i].Losses = parse->item->Losses;
							memcpy(TList[i].Name, parse->item->Owner->Name, 32);
							TList[i].Points = (parse->item->EventPoints + parse->item->Points);
							TList[i].Wins = parse->item->Wins;
							if (parse->item->Losses)
							{
								TList[i].Ratio = (double)parse->item->Wins / (double)parse->item->Losses;
							}
							else
							{
								TList[i].Ratio = 0;
							}
						}
						else
						{
							if (i == 9)
							{
								TList[i] = TList[i - 1];
							}
							else
							{
								TList[i + 1] = TList[i];
								TList[i] = TList[i - 1];
							}
						}
					}
					parse = parse->next;
				}

				_string s;
				s = "TOP10 PLAYERS: Sorting by ";
				switch (TypeSelect)
				{
				case TOP10_Score:
					s += "score:";
					break;
				case TOP10_Average:
					s += "average:";
					break;
				case TOP10_Losses:
					s += "losses:";
					break;
				case TOP10_Wins:
					s += "wins:";
					break;
				case TOP10_Ratio:
					s += "ratio:";
					break;
				};
				player->SendArena(s.Text);

				for (i = 0; i < 10; i++)
				{
					_string s;
					s = i + 1;
					s += ") ";
					s = FillSpace(s.Text, 4);
					s += TList[i].Name;
					s = FillSpace(s.Text, 25);
					s += "  P: ";
					s += TList[i].Points;
					s = FillSpace(s.Text, 40);
					s += "  W: ";
					s += TList[i].Wins;
					s = FillSpace(s.Text, 54);
					s += "  L: ";
					s += TList[i].Losses;
					s = FillSpace(s.Text, 70);
					s += "  A: ";
					s += TList[i].Average;
					s = FillSpace(s.Text, 85);
					s += "  R: ";
					if (TList[i].Losses)
					{
						char buffer[16];
						gcvt(TList[i].Ratio, 4, buffer);
						s += buffer;
					}
					else
					{
						s += "N/A";
					}

					player->SendArena(s.Text);
				}

				return true;
			}

			if (MatchString(command, "?msgclear", 9))
			{
				_listnode <GMessage> * parse = DATABASE.GMessageDatabase.head;
				while (parse)
				{
					if (parse->item->UserID == player->player->LoadID)
					{
						DATABASE.GMessageDatabase.Delete(parse, false);
						parse = DATABASE.GMessageDatabase.head;
						continue;
					}
					parse = parse->next;
				}

				player->SendArena("Messagebox cleared");
				return true;
			}

			if (MatchString(command, "?msglist", 8))
			{
				bool Found = false;
				BYTE Counter = 0;

				_listnode <GMessage> * parse = DATABASE.GMessageDatabase.head;
				while (parse)
				{
					if (parse->item->UserID == player->player->LoadID)
					{
						Counter++;
						Found = true;
						_string s;
						tm * CDate;
						
						if (!parse->item->Special) s += "*NEW* ";
						s += "Message #";
						s += parse->item->MsgID;
						s += " sent by ";
						
						char s2[32] = "(INVALID)";

						_listnode <User> * uparse = DATABASE.UserDatabase.head;
						while (uparse)
						{
							if (uparse->item->LoadID == parse->item->FromID)
							{
								memcpy(s2, uparse->item->Name, 32);
								break;
							}
							uparse = uparse->next;
						}

						s += s2;
						s += " on ";

						CDate = Localize(&parse->item->PostTime, player->player->TimeZoneBias);
						if (CDate)
						{
							s += CDate->tm_mon + 1;
							if (CDate->tm_mday < 10)
								s += "-0";
							else
								s += "-";
								s += CDate->tm_mday;
								s += "-";
								s += GetYear(CDate);
						}
						else
						{
							s += "(Invalid date)";
						}

						player->SendArena(s.Text);

					}
					parse = parse->next;
				}
				if (!Found)
				{
					player->SendArena("There are no messages for you");
					return true;
				}
				if (Counter >= 10) player->SendArena("WARNING: Your message box is full. You cannot receive any more messages!");
				player->SendArena("Report messaging abuse to moderators");
				return true;
			}

			if (MatchString(command, "?msgdel", 7))
			{
				if (length < 9)
				{
					player->SendArena("This command requires parameters. Type ?man msgdel for details");
					return true;
				}
				
				LONG MsgID;
				MsgID = atol(&command[8]);
				if (MsgID < 0)
				{
					player->SendArena("Invalid message number");
					return true;
				}

				_listnode <GMessage> * parse = DATABASE.GMessageDatabase.head;
				while (parse)
				{
					if (parse->item->MsgID == MsgID)
					{
						if (parse->item->UserID == player->player->LoadID)
						{
							DATABASE.GMessageDatabase.Delete(parse, false);
							player->SendArena("Message deleted");
							return true;
						}
						else
						{
							player->SendArena("Invalid message number");
							return true;
						}
					}
					parse = parse->next;
				}
				
				player->SendArena("Invalid message number");

				return true;
			}

			if (MatchString(command, "?msgpost", 8))
			{
				if (length < 10)
				{
					player->SendArena("This command requires parameters. Type ?man msgpost for details");
					return true;
				}

				BYTE Name[32];
				char Message[256];
				memset(Name, 0, 32);
				memset(Message, 0, 256);

				SHORT	LeftLength;
				char  * RightText;

				LONG UserID;

				if (ColonParse(&command[9], length - 9, LeftLength, RightText))
				{
					memcpy(Name, &command[9], LeftLength);

					_listnode <User> * user = DATABASE.FindUser(Name);
	
					if (user)
					{
						UserID = user->item->LoadID;
						memcpy(Name, user->item->Name, 32);
					}
					else
					{
						player->SendArena("Unknown user");
						player->SendArena(Name);
						return true;
					}

					memcpy(Message, RightText, length - LeftLength - 10);
					
					time_t CDate;
					time(&CDate);

					GMessage * gmsg = new GMessage(UserID, player->player->LoadID, ++DATABASE.NextUniversalMsgID, CDate, 0, Message);
					
					_listnode <GMessage> * parse = DATABASE.GMessageDatabase.head;

					LONG Counter = 0;

					while (parse)
					{
						if (parse->item->UserID == UserID)
						{
							Counter++;
						}
						parse = parse->next;
					}

					if (Counter >= 10)
					{
						player->SendArena("Cannot send message! Recipient's messagebox is full!");
						return true;
					}

					DATABASE.GMessageDatabase.Append(gmsg);

					player->SendArena("Message sucessfully sent");
					return true;

				}
				else
				{
					player->SendArena("SYNTAX: ?msgpost <player>:<message>");
					return true;
				}
				
				return true;
			}

			if (MatchString(command, "?msgview", 8))
			{
				if (length < 10)
				{
					player->SendArena("This command requires parameters. Type ?man msgview for details");
					return true;
				}

				LONG MsgID;

				MsgID = atol(&command[9]);
				if (MsgID < 0)
				{
					player->SendArena("Invalid message number");
					return true;
				}

				_listnode <GMessage> * parse = DATABASE.GMessageDatabase.head;

				while (parse)
				{
					if (parse->item->MsgID == MsgID)
					{
						if (parse->item->UserID != player->player->LoadID)
						{
							player->SendArena("Invalid message number");
							return true;
						}
						_string s;
						s = "Displaying message #";
						s += MsgID;
						player->SendArena(s.Text);
						player->SendArena(parse->item->Message);
						parse->item->Special = 1;
						return true;
					}
					parse = parse->next;
				}
				player->SendArena("Invalid message number");
				return true;
			}

			if (MatchString(command, "?details", 8))
			{
				if (player->player->regform)
				{
					if (length >= 9)
					{
						if (command[8] == '=')
						{
							// Set details
							char Comment[128];

							memset(Comment, 0, 128);
							memcpy(Comment, &command[9], TrimInt(length - 9, 127));
							memcpy(player->player->regform->Comment, Comment, 128);

							player->SendArena("Details changed successfully");
						}
						else
						{
							BYTE Name[32];

							memset(Name, 0, 32);
							SpaceTrim(Name, &command[9], 31);

							_listnode <User> * parse = DATABASE.FindUser(Name);

							if (parse)
							{
								if (parse->item->regform)
								{
									_string s;
									s = "Name: ";
									s += parse->item->regform->Name;
										player->SendArena(s.Text);
									s = "Email: ";
									s += parse->item->regform->Email;
									player->SendArena(s.Text);
									s = "Details: ";
									s += parse->item->regform->Comment;
									player->SendArena(s.Text);
								}
								else
								{
									player->SendArena("Player has not sent registration form");
								}
							}
							else
							{
								player->SendArena("Unknown user");
							}
						}
					}
					else
					{	// View details
						_string s;
						s = "Name: ";
						s += player->player->regform->Name;
						player->SendArena(s.Text);
						s = "Email: ";
						s += player->player->regform->Email;
						player->SendArena(s.Text);
						s = "Details: ";
						s += player->player->regform->Comment;
						player->SendArena(s.Text);
					}
				}
				else
				{	// Registration data not found
					player->SendArena("This feature may only be used by registered players. Log in with Subspace to register(Continuum has registering disabled)");
				}

				return true;
			}

			if (MatchString(command, "?setname", 8))
			{
				if (player->player->regform)
				{
					if (length >= 9)
					{
						char Name[32];

						memset(Name, 0, 32);
						memcpy(Name, &command[9], TrimInt(length - 9, 31));
						memcpy(player->player->regform->Name, Name, 32);

						player->SendArena("Registered name changed");
					}
					else
					{
						memset(player->player->regform->Name, 0, 32);
						player->SendArena("Registered name changed");
					}
				}
				else
				{
					player->SendArena("This feature may only be used by registered players. Log in with Subspace to register(Continuum has registering disabled)");
				}

				return true;
			}

			if (MatchString(command, "?setemail", 9))
			{
				if (player->player->regform)
				{
					if (length >= 10)
					{
						char Email[32];

						memset(Email, 0, 32);
						memcpy(Email, &command[10], TrimInt(length - 10, 31));
						memcpy(player->player->regform->Email, Email, 32);

						player->SendArena("Registered email address changed");
					}
					else
					{
						memset(player->player->regform->Email, 0, 32);
						player->SendArena("Registered email address changed");
					}
				}
				else
				{
					player->SendArena("This feature may only be used by registered players. Log in with Subspace to register(Continuum has registering disabled)");
				}

				return true;
			}


			/* Chat channel commands */

			if (MatchString(command, "?notifychat", 11))
			{
				// Check if there is a request for a certain state.
				if (length >= 13)
				{
					switch (command[12])
					{
					case 'o':
					case 'O':
						if (length >= 14)
						{
							if (player->ChatNotify = (command[13] != 'f') && (command[13] != 'F'))
							{
								player->SendArena("Chat notify ENABLED");
								if (player->player->regform)
									player->player->regform->NotifyChat = 1;
							}
							else
							{
								player->SendArena("Chat notify DISABLED");
								if (player->player->regform)
									player->player->regform->NotifyChat = 0;
							}

							return true;
						}
					case 'y':
					case 'Y':
					case '1':
						{
							player->SendArena("Chat notify ENABLED");
							player->ChatNotify = true;
							if (player->player->regform)
								player->player->regform->NotifyChat = 1;
						}
						return true;
					case 'n':
					case 'N':
					case '0':
						{
							player->SendArena("Chat notify DISABLED");
							player->ChatNotify = false;
							if (player->player->regform)
								player->player->regform->NotifyChat = 0;
						}
						return true;
					};
				}

				if (player->ChatNotify == false)
				{
					player->SendArena("Chat notify is currently DISABLED");
				}
				else
				{
					player->SendArena("Chat notify is currently ENABLED");
				}

				return true;
			}

			if (MatchString(command, "?ghost", 6))
			{

				// Check if there is a request for a certain state.
				if (length >= 8)
				{
					switch (command[7])
					{
					case 'o':
					case 'O':
						if (length >= 9)
						{
							if (player->Ghost = (command[8] != 'f') && (command[8] != 'F'))
								player->SendArena("Ghosting ENABLED");
							else
								player->SendArena("Ghosting DISABLED");

							return true;
						}
					case 'y':
					case 'Y':
					case '1':
						{
							player->SendArena("Ghosting ENABLED");
							player->Ghost = true;
						}
						return true;
					case 'n':
					case 'N':
					case '0':
						{
							player->SendArena("Ghosting DISABLED");
							player->Ghost = false;
						}
						return true;
					};
				}

				if (player->Ghost == false)
				{
					player->SendArena("Ghosting is currently DISABLED");
				}
				else
				{
					player->SendArena("Ghosting is currently ENABLED");
				}

				return true;
			}

			if (MatchString(command, "?password", 9))
			{
				if (length <= 10)
				{
					player->SendArena("This command requires parameters. Type ?man password for details");
					return true;
				}

				char buffer[32];
				memset(buffer, 0, 32);
				memcpy(buffer, &command[10], length - 10);
				HashPassword((BYTE*)buffer);
				memcpy(player->player->Password, buffer, 32);
				player->SendArena("Password changed");

				return true;
			}

			if (MatchString(command, "?chat", 5))
			{
				BYTE buffer[512];
				bool IgnoreMore = false;

				if ((length >= 6) && ((command[5] == ' ') || (command[5] == '=')) )
				{
					// ?chat chan, chan	- Add channels
					Chat * Channels[10];
					for (SHORT i = 0; i < 10; i++)
						if (player->channels[i] && (player->channels[i]->Participants.Total > 1))
							Channels[i] = player->channels[i];
						else
							Channels[i] = NULL;

					if (length == 6)
					{
						// The user is just closing all his channels
						player->ExitAllChats(true);

						return true;
					}
					else
					{
						player->ExitAllChats(false);
					}

					// Parse list of channels
					int offset		= 0,
						chatindex	= 1;		// There is no channel 0

					for (i = 6; i < length; i++)
					{
						// These characters designate a new entry starting
						if (command[i] == ',')
						{
							IgnoreMore = false;

							// Ignore ",,,,,,,________"
							if (offset > 0)
							{
								// Add a chat
								buffer[offset] = 0;
								BYTE Name[32];
								SpaceTrim(Name, (char*)&buffer[0], 31);

								if (InvalidUsername((char*)&Name[0]))
								{
									if (Name[0] == '$' && Name[1] == '$')
									{
										player->SendArena("This billing server does not support the staff chat function. Use the chat owning system instead");
									}
									else
									{
										_string s;
										s = "Invalid chat name: ";
										s += (char*)&Name[0];
										player->SendArena(s.Text);
									}
								}
								else
								{								
									Chat * chan = player->JoinChat(Name, chatindex, false);
									if (chan)
									{
										// Broadcast if it's a new chat, note if it is not
										bool OnList = false;
										for (SHORT x = 1; x < 10; x++)
											if (Channels[x] == chan)
											{
												Channels[x] = NULL;
												OnList = true;
												break;
											}
										if (OnList == false)
											DATABASE.BroadcastEntering(player, chan);
										// Stop parsing if all channels have been filled
										if ((chatindex++) == 9)
										{
											offset = 0;
	
											break;
										}
									}
								}

								// Reset string parser
								offset = 0;
							}
						}
						else if (IgnoreMore == false)
						{
							// Store a character
							buffer[offset++] = command[i];

							// Limit chat channel lengths to 31 characters
							if (offset == 32)
								IgnoreMore = true;
						}
					}

					// Add whatever is left in the buffer
					if (offset > 0)
					{
						// Add a chat
						buffer[offset] = 0;
						BYTE Name[32];
						SpaceTrim(Name, (char*)&buffer[0], 31);

						if (InvalidUsername((char*)&Name[0]))
						{
							if (Name[0] == '$' && Name[1] == '$')
							{
								player->SendArena("This billing server does not support the staff chat function. Use the chat owning system instead");
							}
							else
							{
								_string s;
								s = "Invalid chat name: ";
								s += (char*)&Name[0];
								player->SendArena(s.Text);
							}
						}
						else
						{
							Chat * chan = player->JoinChat(Name, chatindex, false);

							if (chan)
							{
								bool OnList = false;
								for (SHORT x = 1; x < 10; x++)
									if (Channels[x] == chan)
									{
										Channels[x] = NULL;
										OnList = true;
										break;
									}
								if (OnList == false)
									DATABASE.BroadcastEntering(player, chan);
							}
						}
					}
					for (i = 1; i < 10; i++)
						if (Channels[i] != NULL)
							DATABASE.BroadcastLeaving(player, Channels[i]);
				}
				else
				{
					// ?chat - List players on channels

					bool Found = false;

					for (int id = 1; id < 10; id++)
						if (player->channels[id])
						{
							Found = true;
							// Write channel header
							buffer[0] = id + 48;
							buffer[1] = ')';
							buffer[2] = 0;
							strcat((char*)&buffer[0], (char*)&player->channels[id]->Name[0]);
							strcat((char*)&buffer[0], ": ");

							int count = 0;

							// Write channel body
							_listnode <Chatter> * parse = player->channels[id]->Participants.head;

							while (parse)
							{
								if (parse->item->Player->Ghost && parse->item->Player->OpStatus)
								{
									parse = parse->next;
								}
								else
								{
									strcat((char*)&buffer[0], (char*)&parse->item->Player->player->Name[0]);

									parse = parse->next;

									count++;
									if (count == 10)
									{
										player->SendArena(buffer);
										buffer[0]	= 0;
										count		= 0;
									}
									else if (parse)
										strcat((char*)&buffer[0], ",");
								}
							}

							if (count)
								player->SendArena(buffer);
						}
					if (!Found)
					{
						player->SendArena("You are not in any chat channels");
					}
				}

				return true;
			}

			if (MatchString(command, "?squadowner", 11))
			{
				if (length <= 12)
				{
					player->SendArena("This command requires parameters. Type ?man squadowner for details");
					return true;
				}

				Squad * squad = DATABASE.FindSquad((BYTE*)&command[12]);

				if (squad)
				{
					char buffer[512];

					strcpy(buffer, (char*)&squad->Name[0]);
					strcat(buffer, " owner: ");
					if (squad->Owner)
						strcat(buffer, (char*)&squad->Owner->Name[0]);
					else
						strcat(buffer, "(NO OWNER)");

					player->SendArena(buffer);
				}
				else	
				{
					player->SendArena("Specified squad does not exist");
				}

				return true;
			}

			if (MatchString(command, "?seen", 5))
			{
				User * user;

				if (length <= 6)
				{
					user = player->player;
				}
				else
				{
					// Check player squad
					_listnode <User> * parse = DATABASE.FindUser((BYTE*)&command[6]);
					if (parse)
						user = parse->item;
					else
						user = NULL;
				}

				if (user)
				{
					_string s;

					s  = (char*)&user->Name[0];
					s += " last seen: ";
					tm * date = Localize(&user->LastLogin, player->player->TimeZoneBias);
					if (date)
					{
						s += date->tm_mon + 1;
						if (date->tm_mday < 10)
							s += "-0";
						else
							s += "-";
						s += date->tm_mday;
						s += "-";
						s += GetYear(date);
						s += " ";
							s += date->tm_hour;
						if (date->tm_min < 10)
							s += ":0";
						else
							s += ":";
						s += date->tm_min;
						if (date->tm_sec < 10)
							s += ".0";
						else
							s += ".";
						s += date->tm_sec;
					}
					else
					{
						s += "(Invalid date)";
					}

					player->SendArena(s.Text);
				}
				else
				{
					player->SendArena("Unknown user");
				}

				return true;
			}

			if (MatchString(command, "?squadjoin", 10))
			{
				if (length <= 11)
				{
					player->SendArena("This command requires parameters. Type ?man squadjoin for details");
					return true;
				}

				BYTE Name[32];
				BYTE Password[32];
				memset(Name, 0, 32);
				memset(Password, 0, 32);

				// Parse input
				SHORT	LeftLength;
				char  * RightText;

				if (ColonParse(&command[11], length - 11, LeftLength, RightText))
				{
					// Find squad
					memcpy(Name, &command[11], LeftLength);

					Squad * squad = DATABASE.FindSquad(Name);

					if (squad)
					{
						// Check password
						memcpy(Password, RightText, strlen(RightText));

						if ((squad->Owner == player->player) || DATABASE.ValidatePassword(squad, Password))
						{
							// Join squad
							if (player->player->squad)
								player->player->squad->Participants.Delete(player->player, true);
							player->player->squad = squad;
							squad->Participants.Append(player->player);

							player->SendArena("Squad successfully joined. Reenter the zone for it to take effect");
						}
						else
						{
							player->SendArena("Invalid password for specified squad");
						}
					}
					else	
					{
						player->SendArena("Specified squad does not exist");
					}
				}
				else
				{
					memcpy(Name, &command[11], TrimInt(length - 11, 31));

					Squad * squad = DATABASE.FindSquad(Name);

					if (squad)
					{
						if (squad->Owner == player->player)
						{
							if (player->player->squad)
								player->player->squad->Participants.Delete(player->player, true);
							player->player->squad = squad;
							squad->Participants.Append(player->player);

							player->SendArena("Squad successfully joined. Reenter the zone for it to take effect");
						}
						else
						{
							player->SendArena("You are not squad owner. Only squad owners can join squads without a password");
						}
					}
					else
					{
						player->SendArena("Specified squad does not exist");
					}
				}

				return true;
			}

			if (MatchString(command, "?squadleave", 9))
			{
				if (player->player->squad)
				{
					// Remove player from squad
					player->player->squad->Participants.Delete(player->player, true);

					// Unregister squad from player
					player->player->squad = NULL;
				}

				player->SendArena("Squad successfully left. Reenter the zone for it to take effect");

				return true;
			}

			if (MatchString(command, "?squadcreate", 12))
			{
				if (length <= 13)
				{
					player->SendArena("This command requires parameters. Type ?man squadcreate for details");
					return true;
				}

				BYTE Name[32];
				BYTE Password[32];
				memset(Name, 0, 32);
				memset(Password, 0, 32);

				// Parse input
				SHORT	LeftLength;
				char  * RightText;

				if (ColonParse(&command[13], length - 13, LeftLength, RightText))
				{
					// Find squad
					SpaceTrim(Name, &command[13], TrimInt(LeftLength, 31));

					if (InvalidSquadname((char*)&Name[0]))
					{
						player->SendArena("All characters in your squad's name must be printable");

						return true;
					}

					Squad * squad = DATABASE.FindSquad(Name);

					if (!squad)
					{
						memcpy(Password, RightText, length - LeftLength - 14);
						if (player->player->squad)
							player->player->squad->Participants.Delete(player->player, true);
						DATABASE.AddSquad(Name, Password, player->player);

						player->SendArena("Squad successfully created and joined. Reenter the zone for it to take effect");
					}
					else
					{
						player->SendArena("Specified squad already exists");
					}
				}
				else
				{
					player->SendArena("This command requires parameters. Type ?man squadcreate for details");
					return true;
				}

				return true;
			}

			if (MatchString(command, "?squadlist", 8))
			{
				// List squaddies
				if (player->player->squad)
				{
//					if (player->player->squad->Owner == player->player)		No longer a squad owner command
					{
						char buffer[512];
						int count = 0;
						strcpy(buffer, "Squadmates: ");

						_listnode <User> * parse = player->player->squad->Participants.head;

						while (parse)
						{
							strcat(buffer, (char*)&parse->item->Name[0]);

							count++;

							if (parse->next)
							{
								strcat(buffer, ",");

								if (count == 10)
								{
									player->SendArena(buffer);

									buffer[0]	= 0;
									count		= 0;
								}
							}

							parse = parse->next;
						}

						if (count)
							player->SendArena(buffer);
					}
/*					else
					{
						player->SendArena("You are not squad owner.");
					}
*/				}
				else
				{
					player->SendArena("You are not on a squad");
				}

				return true;
			}

			if (MatchString(command, "?squaddissolve", 10))
			{
				// Delete squad
				if (player->player->squad)
				{
					if (player->player->squad->Owner == player->player)
					{
						DATABASE.DeleteSquad(player->player->squad);

						player->SendArena("Squad dissolved. Reenter the zone for it to take effect");
					}
					else
					{
						player->SendArena("You are not squad owner");
					}
				}
				else
				{
					player->SendArena("You are not on a squad");
				}

				return true;
			}

			if (MatchString(command, "?squadpassword", 14))
			{
				if (length <= 15)
				{
					player->SendArena("This command requires parameters. Type ?man squadpassword for details");
					return true;
				}

				// Change squad password
				if (player->player->squad)
				{
					if (player->player->squad->Owner == player->player)
					{
						player->player->squad->SetPassword(&command[15]);

						player->SendArena("Squad password changed");
					}
					else
					{
						player->SendArena("You are not squad owner");
					}
				}
				else
				{
					player->SendArena("You are not on a squad");
				}

				return true;
			}

			if (MatchString(command, "?squadname", 10))
			{
				if (length <= 11)
				{
					player->SendArena("This command requires parameters. Type ?man squadname for details");
					return true;
				}

				// Change squad name
				if (player->player->squad)
				{
					if (player->player->squad->Owner == player->player)
					{
						strcpy((char*)&player->player->squad->Name[0], &command[11]);

						player->SendArena("Squad name changed. Reenter the zone for it to take effect");
					}
					else
					{
						player->SendArena("You are not squad owner");
					}
				}
				else
				{
					player->SendArena("You are not on a squad");
				}

				return true;
			}

			if (MatchString(command, "?squadkick", 10))
			{
				if (length <= 11)
				{
					player->SendArena("This command requires parameters. Type ?man squadkick for details");
					return true;
				}

				// Delete squaddie
				if (player->player->squad)
				{
					if (player->player->squad->Owner == player->player)
					{
						_listnode <User> * parse = player->player->squad->FindUser(&command[11]);

						if (parse)
						{
							User * user = parse->item;
							player->player->squad->Participants.Delete(parse, true);
							user->squad = NULL;

							player->SendArena("Player removed from squad");
						}
						else
						{
							player->SendArena("Player is not a squad member");
						}
					}
					else
					{
						player->SendArena("You are not squad owner");
					}
				}
				else
				{
					player->SendArena("You are not on a squad");
				}

				return true;
			}

			if (MatchString(command, "?squadgrant", 11))
			{
				// Grant owner access to an other player
				if (player->player->squad)
				{
					if (player->player->squad->Owner == player->player)
					{
						_listnode <User> * parse = DATABASE.FindUser((BYTE*)&command[12]);

						if (parse)
						{
							player->player->squad->Owner = parse->item;

							player->SendArena("Squad owner changed");
						}
						else
						{
							player->SendArena("Unknown user");
						}
					}
					else
					{
						player->SendArena("You are not squad owner");
					}

				}
				else
				{
					player->SendArena("You are not on a squad");
				}

				return true;
			}

			if (MatchString(command, "?squad", 6))
			{
				if (length <= 8)
				{
					if (player->player->squad)
					{
						_string s;
						s  = (char*)&player->player->Name[0];
						s += "'s squad is ";
						s += (char*)&player->player->squad->Name[0];

						player->SendArena(s.Text);
					}
					else
					{
						player->SendArena("You have no squad");
					}

					return true;
				}

				// Check player squad
				_listnode <User> * parse = DATABASE.FindUser((BYTE*)&command[7]);

				if (parse)
				{
					char buffer[512];
					strcpy(buffer, (char*)&parse->item->Name[0]);

					Squad * squad = parse->item->squad;

					if (squad)
					{
						strcat(buffer, "'s squad is ");
						strcat(buffer, (char*)&squad->Name[0]);
					}
					else
					{
						strcat(buffer, " has no squad");
					}

					player->SendArena(buffer);
				}
				else
				{
					player->SendArena("Unknown player");
				}

				return true;
			}

			if (MatchString(command, "?login", 6))
			{
				if (length <= 7)
				{
					player->SendArena("This command requires parameters. Type ?man login for details");

					return true;
				}

				if (player->OpStatus == NULL)
				{
					player->OpStatus = DATABASE.ValidateSysopPassword(player, (BYTE*)&command[7]);

					if (player->OpStatus)
					{
						_string s;

						player->OpStatus->AddLogin();

						if (player->OpStatus->GroupID == 0)
							s  = "Successfully logged on as Network-wide level ";
						else
						{
							if (player->OpStatus->ScoreID == 0)
								s  = "Successfully logged on as Group-wide level ";
							else
								s  = "Successfully logged on as Zone-wide Operator level ";
						}
						s += (int) player->OpStatus->Level;

						player->SendArena(s.Text);

						if (player->OpStatus->ScoreID != 0) //Zone-wide op?
						{
							if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
							//No biller access in this zone?
							{
								player->SendArena("You only have limited powers in this zone");
							}
						}
						else //Not a zone-wide op
						{
							if (player->OpStatus->GroupID != 0)
							//Group-wide
							{
								if (player->OpStatus->GroupID != Packet->Source->GroupID)
								{
									player->SendArena("You only have limited powers in this zone");
								}
							}
						}

					}
				}
				else
					player->SendArena("Already logged on as an Operator. Use ?logout to lift Operator status");

				return true;
			}

			if ((MatchString(command, "?logout", 7)) || (MatchString(command,"?dop",4)))
			{
				if (player->OpStatus)
				{
					player->OpStatus->RemoveLogin();

					player->OpStatus = NULL;
					player->SendArena("Successfully logged out");
				}

				return true;
			}

			if (MatchString(command, "?whois", 6))
			{
				Operator * op = NULL;

				if (length >= 8)
				{
					_listnode <Host> * parse = (Packet->Source->Socket)->HostList.head;

					while (parse)
					{
						// Find user
						_listnode <LUserNode> * user = parse->item->Find(&command[7], true);

						if (user && !user->item->Ghost)
						{
							op = user->item->OpStatus;

							break;
						}

						// Try next zone
						parse = parse->next;
					}

				}
				else
				{
					op = player->OpStatus;
				}

				_string s;

				if (op)
				{
					s = "Logged on as ";

					if (op->GroupID)
					{
						if (op->ScoreID == 0)
						{
							s += "group-wide ";
						}
						else
						{
							s += "zone-wide ";
						}
					}
					else
					{
						s += "network-wide ";
					}

					s += "operator level ";

					switch (op->Level)
					{
					case 0:		s += "0 - God";			// Network Administrator";
						break;
					case 1:		s += "1 - Archangel";	// Group Manager";
						break;
					case 2:		s += "2 - Overlord";	// SysOp";
						break;
					case 3:		s += "3 - Keeper";		// Super Moderator";
						break;
					case 4:		s += "4 - Slave";		// Moderator";
						break;
					};
				}
				else
				{
					s = "User not logged in";
				}

				player->SendArena(s.Text);

				return true;
			}

		if (player->OpStatus)
		{
			switch (player->OpStatus->Level)
			{
			case 0:		///////  Level 0 Operators - Network Administrators  ///////
				if (MatchString(command, "?networkreset", 13))
				{
					if (player->OpStatus->GroupID != 0)
					{
						player->SendArena("You need netwowrk-wide access to use this command");
						return true;
					}

					player->SendArena("Recycling zones for the score reset");

					// Erase the scores
					DATABASE.ScoreDatabase.Clear(false);

					// Traverse the host list
					_listnode <Host> * parse = Packet->Source->Socket->HostList.head;

					while (parse)
					{
						Host * host = parse->item;
						parse = parse->next;

						// Erase references to the scores
						host->ScoreDatabase.Clear(true);

						_listnode <LUserNode> * uparse = host->UserList.head;

						while (uparse)
						{
							uparse->item->score = NULL;
							uparse = uparse->next;
						}

						// Recycle the host
						host->AcceptScores = false;
						host->Recycle();
					}
					return true;
				}

				if (MatchString(command, "?netbantext", 11))
				{
					if (length >= 12)
					{
						char Comment[128];

						memset(Comment, 0, 128);
						memcpy(Comment, &command[12], TrimInt(length - 12, 127));
						memcpy(DATABASE.NetworkBanText, Comment, 128);

						player->SendArena("Network ban text changed");
					}
					else
					{
						_string s;
						s = "Network ban text: ";
						s += DATABASE.NetworkBanText;
						player->SendArena(s.Text);
					}
					return true;
				}

				if (MatchString(command, "?block", 6))
				{	// ?block <name>
					User * user;

					if (length <= 7)
					{
						user = player->player;
					}
					else
					{
						_listnode <User> * parse = DATABASE.FindUser((BYTE*)&command[7]);
						if (parse)
							user = parse->item;
						else
							user = NULL;
					}

					if (user)
					{
						switch (user->PassBlock)
						{
						case BLOCK_OK:
							player->SendArena("PassBlock: Banfree");
							user->PassBlock = BLOCK_PASS;
							break;
						default:
							player->SendArena("PassBlock: Normal");
							user->PassBlock = BLOCK_OK;
							break;
						};
					}
					else
					{
						player->SendArena("Specified user was not found in the database");
					}

					return true;
				}

				if (MatchString(command, "?write", 5))
				{
					player->SendArena("Please wait...");
					DATABASE.SaveToDisk();
					player->SendArena("Database stored to disk");

					return true;
				}

				if (MatchString(command, "?read", 5))
				{
					player->SendArena("Please wait...");
					DATABASE.LoadSettings();
					player->SendArena("Settings loaded from disk");

					return true;
				}

				if (MatchString(command, "?changepassword", 15))
				{	// ?changepassword Name:Password
					if (length <= 16)
					{
						player->SendArena("This command requires parameters. Type ?man changepassword for details");
						return true;
					}

					BYTE Name[32];
					BYTE Password[32];
					memset(Name, 0, 32);
					memset(Password, 0, 32);

					// Parse input
					SHORT	LeftLength;
					char  * RightText;

					if (ColonParse(&command[16], length - 16, LeftLength, RightText))
					{
						// Find user
						memcpy(Name, &command[16], LeftLength);

						_listnode <User> * user = DATABASE.FindUser(Name);

						if (user)
						{
							if (player->OpStatus->Level)
							{
								_listnode <Operator> * parse = DATABASE.OpDatabase.head;

								while (parse)
								{
									if (parse->item->Level <= player->OpStatus->Level)
									{
										player->SendArena("You may not change player passwords of equal or lower operator status");

										return true;
									}

									parse = parse->next;
								}
							}

							memcpy(Password, RightText, length - LeftLength - 17);

							HashPassword(Password);

							memcpy(user->item->Password, Password, 32);

							player->SendArena("User password changed");
						}
						else
						{
							player->SendArena("Unknown user");
						}
					}

					return true;
				}

				if (MatchString(command, "?changesquad", 12))
				{	// ?changesquad Squad:Name
					if (length <= 13)
					{
						player->SendArena("This command requires parameters. Type ?man changesquad for details");
						return true;
					}

					BYTE squadname[32];
					BYTE name[32];
					memset(squadname, 0, 32);
					memset(name, 0, 32);

					// Parse input
					SHORT	LeftLength;
					char  * RightText;

					if (ColonParse(&command[13], length - 13, LeftLength, RightText))
					{
						// Find user
						memcpy(squadname, &command[13], LeftLength);

						Squad * squad = DATABASE.FindSquad(squadname);

						if (squad)
						{
							memcpy(name, RightText, length - LeftLength - 14);

							_listnode <User> * user = DATABASE.FindUser(name);

							if (user)
							{
								squad->Owner = user->item;

								player->SendArena("Squad owner changed");
							}
							else
							{
								player->SendArena("User not found");
							}
						}
						else
						{
							player->SendArena("Squad not found");
						}
					}

					return true;
				}

				if (MatchString(command, "?changename", 11))
				{	// ?changename Name:New Name
					if (length <= 12)
					{
						player->SendArena("This command requires parameters. Type ?man changename for details");
						return true;
					}

					BYTE Name[32];
					BYTE NewName[32];
					memset(Name, 0, 32);
					memset(NewName, 0, 32);

					// Parse input
					SHORT	LeftLength;
					char  * RightText;

					if (ColonParse(&command[12], length - 12, LeftLength, RightText))
					{
						// Find user
						memcpy(Name, &command[12], LeftLength);
						memcpy(NewName, RightText, length - LeftLength - 13);

						_listnode <User> * user;
						user = DATABASE.FindUser(NewName);

						if (user == NULL)
						{
							user = DATABASE.FindUser(Name);

							if (user)
							{
								if (player->OpStatus->Level)
								{
									_listnode <Operator> * parse = DATABASE.OpDatabase.head;

									while (parse)
									{
										if (parse->item->Level <= player->OpStatus->Level)
										{
											player->SendArena("You may not change player names of equal or lower operator status");

											return true;
										}

										parse = parse->next;
									}
								}

								memcpy(user->item->Name, NewName, 32);

								player->SendArena("User name changed");
							}
							else
							{
								player->SendArena("Unknown user");
							}
						}
						else
						{
							player->SendArena("User already exists");
						}
					}

					return true;
				}

			case 1:		///////  Level 1 Operators///////
				if (MatchString(command, "?resetscore", 11))
				{
					if (length < 13)
					{
						player->SendArena("This command requires parameters. Type ?man resetscore for details");
						return true;
					}

					char Number[8];
					LONG Index;

					memset(Number, 0, 8);
					memcpy(Number, &command[12], TrimInt(length - 12, 7));
					Index = atol(Number);

					if (Index <= 0)
					{
						player->SendArena("Invalid ScoreID");
						return true;
					}

					bool Allow = false;

					_listnode <Zone> * zparse = DATABASE.FindZone(Index);

					if (!zparse)
					{
						player->SendArena("A zone with the selected ScoreID was not found");
						return true;
					}

					if (player->OpStatus->GroupID != 0 && player->OpStatus->ScoreID == 0 && zparse->item->GroupID == player->OpStatus->GroupID) Allow = true;

					if (player->OpStatus->GroupID != 0)
					{
						if (player->OpStatus->ScoreID != 0)
						{
							if (player->OpStatus->ScoreID == Index) Allow = true;
						}
					}
					else Allow = true;

					if (!Allow)
					{
						player->SendArena("You do not have the necessary authority to reset the target zone's scores");
						return true;
					}

					player->SendArena("Resetting scores for seleced ScoreID");

					_listnode <Score> * sparse = DATABASE.ScoreDatabase.head;
					while (sparse)
					{
						if (sparse->item->ScoreID == Index)
						{
							DATABASE.ScoreDatabase.Delete(sparse, false);
							sparse = DATABASE.ScoreDatabase.head;
						}
						sparse = sparse->next;
					}

					_listnode <Host> * hparse = Packet->Source->Socket->HostList.head;
					while (hparse)
					{
						if (hparse->item->ScoreID == Index)
						{
							hparse->item->ScoreDatabase.Clear(true);

							_listnode <LUserNode> * uparse = hparse->item->UserList.head;
							while (uparse)
							{
								uparse->item->score = NULL;
								uparse = uparse->next;
							}

							hparse->item->AcceptScores = false;
							hparse->item->Recycle();
						}
						hparse = hparse->next;
					}
					return true;
				}

				if (MatchString(command, "?removezone", 11))
				{	// Unregister zone
					if (length > 12)
					{
						char Number[8];
						int Index;

						memset(Number, 0, 8);
						memcpy(Number, &command[12], TrimInt(length - 12, 7));
						Index = atoi(Number);

						_listnode <Zone> * parse = DATABASE.FindZone(Index);

						if (parse)
						{
							if ((player->OpStatus->GroupID == 0) || (player->OpStatus->GroupID == parse->item->GroupID && (!player->OpStatus->ScoreID || parse->item->ScoreID == player->OpStatus->ScoreID)))
							{
								DATABASE.ZoneDatabase.Delete(parse, false);
								player->SendArena("Zone unregistered. Recycle zone for it to take effect");
							}
							else
							{
								player->SendArena("You do not have the necessary authority to unregister the zone");
							}
						}
						else
						{
							player->SendArena("Specified zone slot is empty");
						}
					}
					else
					{
						player->SendArena("This command requires parameters. Type ?man removezone for details");
						return true;
					}

					return true;
				}

				if (MatchString(command, "?addzone", 8))
				{	// Register a zone
					if (length > 9)
					{
						if (player->OpStatus->ScoreID != 0)
						{
							player->SendArena("You require group-wide or net-wide acess to use this command");
							return true;
						}

						if ((player->OpStatus->GroupID) || (player->OpStatus->Level)) //Net-wide or level 0 has no restrictions
						{
							_listnode <Zone> * parse = DATABASE.ZoneDatabase.head;
							int Counter = 0;

							while (parse)
							{
								if (parse->item->GroupID == player->OpStatus->GroupID)
									Counter++;

								parse = parse->next;
							}

							if (Counter >= 10)
							{
								player->SendArena("You have exceeded your zone quota of 10 slots");

								return true;
							}
						}

						char Number[8];		// Zone number (ASCII buffer)
						int Offset,			// Offset to password
							Index;			// Zone number (Decimal form)

						memset(Number, 0, 8);
						for (Offset = 9; Offset < length; Offset++)
						{
							if (command[Offset] == ':')
							{
								memcpy(Number, &command[9], TrimInt(Offset - 9, 7));

								break;
							}
						}
						Index = atoi(Number);
						Offset++;

						if ((Offset > length) || (Index <= 0))
						{
							player->SendArena("SYNTAX: ?addzone <Score ID>:<Password>");
							return true;
						}

						_listnode <Zone> * parse = DATABASE.FindZone(Index);

						if (parse)
						{
							player->SendArena("Specified zone slot is already taken");
						}
						else
						{
							char Password[32];
							memset(Password, 0, 32);
							memcpy(Password, &command[Offset], TrimInt(length - Offset, 31));
							HashPassword((BYTE*)&Password[0]);

							char BanText[128];
							memset(BanText, 0, 128);
							if (player->OpStatus->GroupID == 0)
							{
								DATABASE.ZoneDatabase.Append(new Zone(Packet->Source->GroupID, Index, Password, BanText));
							}
							else
							{
								DATABASE.ZoneDatabase.Append(new Zone(player->OpStatus->GroupID, Index, Password, BanText));
							}

							_string s;
							s += "Zone registered under group ";
							if (player->OpStatus->GroupID == 0)
							{
								s += Packet->Source->GroupID;
							}
							else
							{
								s += player->OpStatus->GroupID;
							}
							player->SendArena(s.Text);
						}
					}
					else
					{
						player->SendArena("This command requires parameters. Type ?man addzone for details");
					}

					return true;
				}

				if (MatchString(command, "?changezone", 11))
				{
					if (length <= 12)
					{
						player->SendArena("This command requires parameters. Type ?man changezone for details");
						return true;
					}

					char Number[8];		// Zone number (ASCII buffer)
					int Offset,			// Offset to password
						Index;			// Zone number (Decimal form)

					memset(Number, 0, 8);
					for (Offset = 12; Offset < length; Offset++)
					{
						if (command[Offset] == ':')
						{
							memcpy(Number, &command[12], TrimInt(Offset - 12, 7));

							break;
						}
					}
					Index = atoi(Number);
					Offset++;

					if (Offset > length)
					{
						player->SendArena("SYNTAX: ?changezone <Score ID>:<Password>");
						return true;
					}

					_listnode <Zone> * parse = DATABASE.FindZone(Index);

					if (parse)
					{	// Validate operator privelages
						if ((player->OpStatus->GroupID == 0) || (player->OpStatus->GroupID == parse->item->GroupID && (!player->OpStatus->ScoreID || parse->item->ScoreID == player->OpStatus->ScoreID)))
						{
							char Password[32];

							memset(Password, 0, 32);
							memcpy(Password, &command[Offset], TrimInt(length - Offset, 31));

							HashPassword((BYTE*)&Password[0]);

							memcpy(parse->item->Password, Password, 32);

							player->SendArena("Zone password changed");
						}
						else
						{
							player->SendArena("You do not have the necessary authority to unregister the zone");
						}
					}
					else
					{
						player->SendArena("Specified zone slot is empty");
					}

					return true;
				}

				if (MatchString(command, "?listzone", 9))
				{	// List all zones
					_listnode <Zone> * zone_parse;
					_listnode <Host> * host_parse;
					Host * host;
					Zone * zone;
					bool Found = false;
					_string s;

					zone_parse = DATABASE.ZoneDatabase.head;
					while (zone_parse)
					{
						zone = zone_parse->item;

						if ((player->OpStatus->GroupID == 0) || (player->OpStatus->GroupID == zone->GroupID))
						{
							Found = true;
							s = zone->ScoreID;

							host = Packet->Source->Socket->FindHost(zone->ScoreID);

							if (host)
							{
								if (zone->GroupID != host->GroupID)
								{
									s += " - {Invalid GroupID (";
									s += zone->GroupID;
									s += ")(";
									s += host->GroupID;
									s += ")} Online: ";
								}
								else
								{
									s += " - {";
									s += host->GroupID;
									s += "} Online: ";
								}
								s += (char*)&host->Name[0];
								s += "(";
								s += host->UserList.Total;
								s += ")";
							}
							else
							{
								s += " - {";
								s += zone->GroupID;
								s += "} Offline.";
							}

							player->SendArena(s.Text);
						}

						zone_parse = zone_parse->next;
					}

					host_parse = Packet->Source->Socket->HostList.head;
					while (host_parse)
					{
						host = host_parse->item;

						if ((player->OpStatus->GroupID == 0) || (player->OpStatus->GroupID == host->GroupID))
						{
							if (host->ScoreID != -1)
							{
								if (DATABASE.FindZone(host->ScoreID) == NULL)
								{
									Found = true;

									s  = "";
									s += host->ScoreID;
									s += " - {";
									s += host->GroupID;
									s += "} Online(Unregistered): ";
									s += (char*)&host->Name[0];
									s += "(";
									s += host->UserList.Total;
									s += ")";

									player->SendArena(s.Text);
								}
							}
							else
							{
								Found = true;

								LONG IP		= (*(sockaddr_in*)&host->Remote).sin_addr.S_un.S_addr;
								LONG Port	= htons((*(sockaddr_in*)&host->Remote).sin_port);

								s  = "Connecting: ";
								s += (int)(((unsigned char*)&IP)[0]);
								s += ".";
								s += (int)(((unsigned char*)&IP)[1]);
								s += ".";
								s += (int)(((unsigned char*)&IP)[2]);
								s += ".";
								s += (int)(((unsigned char*)&IP)[3]);
								s += " : ";
								s += (int) Port;

								player->SendArena(s.Text);
							}
						}

						host_parse = host_parse->next;
					}

					if (!Found)
						player->SendArena("No zones found");

					return true;
				}

				if (MatchString(command, "?closezone", 10))
				{	// Terminate zone (if possible)
					Host * host;

					if (length > 11)
					{
						char Number[8];
						int Index;

						memset(Number, 0, 8);
						memcpy(Number, &command[11], TrimInt(length - 11, 7));
						Index = atoi(Number);

						host = Packet->Source->Socket->FindHost(Index);
					}
					else
					{
						host = player->zone;
					}

					if (host)
					{	// Validate operator privelages
						if ((player->OpStatus->GroupID == 0) || (player->OpStatus->GroupID == host->GroupID && (!player->OpStatus->ScoreID || host->ScoreID == player->OpStatus->ScoreID)))
						{
							player->SendArena("Shutdown in progress");

							host->Shutdown();
						}
						else
						{
							player->SendArena("You do not have the necessary authority to shutdown the zone");
						}
					}
					else
					{
						player->SendArena("Specified zone slot is empty");
					}

					return true;
				}

				if (MatchString(command, "?recyclezone", 12))
				{	// Recycle zone (if possible)
					Host * host;

					if (length > 13)
					{
						char Number[8];
						int Index;

						memset(Number, 0, 8);
						memcpy(Number, &command[13], TrimInt(length - 13, 7));
						Index = atoi(Number);

						host = Packet->Source->Socket->FindHost(Index);
					}
					else
					{
						host = player->zone;
					}

					if (host)
					{	// Validate operator privelages
						if ((player->OpStatus->GroupID == 0) || (player->OpStatus->GroupID == host->GroupID && (!player->OpStatus->ScoreID || host->ScoreID == player->OpStatus->ScoreID)))
						{
							player->SendArena("Recycle in progress");

							host->Recycle();
						}
						else
						{
							player->SendArena("You do not have the necessary authority to recycle the zone");
						}
					}
					else
					{
						player->SendArena("Specified zone slot is empty");
					}

					return true;
				}

			case 2:		///////  Level 2 Operators - SysOps  ///////
				if (MatchString(command, "?broadcast", 10))
				{
					if (player->OpStatus->GroupID)
						return true;
					if (length < 12)
					{
						player->SendArena("This commands requires parameters. Type ?man broadcast for details");
						return true;
					}
					bool Append = true;
					_string s;
					s = &command[11];

					if (player->OpStatus->Level < 2) Append = false;

					if (Append)
					{
						s += " [";
						s += (char*)&player->player->Name[0];
						s += "@";
						s += (char*)&Packet->Source->Name[0];
						s += "]";
					}

					BYTE buffer[512];
					short bufferlength;
					memset(buffer, 0, 512);

					bufferlength = MakeZoneMessage(buffer, (BYTE*)&s.Text[0]);

					_listnode <Host> * parse = Packet->Source->Socket->HostList.head;
					while (parse)
					{
						if (parse->item->BroadcastChat)
							parse->item->Send(buffer, bufferlength, true);
						parse = parse->next;
					}

					return true;
				}

				if (MatchString(command, "?groupid", 8))
				{
					_string s;
					s = "GroupId:";
					s += player->zone->GroupID;

					if (player->OpStatus->GroupID == 0)
					{
						player->SendArena(s.Text);
					}
					else if ((player->OpStatus->ScoreID == 0) && (player->OpStatus->GroupID == player->zone->GroupID))
					{
						player->SendArena(s.Text);
					}
					else if (player->OpStatus->ScoreID == player->zone->ScoreID)
					{
						player->SendArena(s.Text);
					}
					else
					{
						player->SendArena("You do not have access here");
					}

					return true;
				}

				if (MatchString(command, "?scoreid", 8))
				{
					_string s;
					s = "ScoreId:";
					s += player->zone->ScoreID;

					if (player->OpStatus->GroupID == 0)
					{
						player->SendArena(s.Text);
					}
					else if ((player->OpStatus->ScoreID == 0) && (player->OpStatus->GroupID == player->zone->GroupID))
					{
						player->SendArena(s.Text);
					}
					else if (player->OpStatus->ScoreID == player->zone->ScoreID)
					{
						player->SendArena(s.Text);
					}
					else
					{
						player->SendArena("You do not have access here");
					}

					return true;
				}

				if (MatchString(command, "?notifyzone", 11))
				{
					// Check if there is a request for a certain state.
					if (length >= 13)
					{
						switch (command[12])
						{
						case 'o':
						case 'O':
							if (length >= 14)
							{
								if ((command[13] != 'f') && (command[13] != 'F'))
								{
									player->SendArena("Zone notify ENABLED");
									player->ZoneNotify = true;
									if (player->player->regform)
										player->player->regform->NotifyZone = 1;
								}
								else
								{
									player->SendArena("Zone notify DISABLED");
									player->ZoneNotify = false;
									if (player->player->regform)
										player->player->regform->NotifyZone = 0;
								}

								return true;
							}
						case 'y':
						case 'Y':
						case '1':
							{
								player->SendArena("Zone notify ENABLED");
								player->ZoneNotify = true;
								if (player->player->regform)
									player->player->regform->NotifyZone = 1;
							}
							return true;
						case 'n':
						case 'N':
						case '0':
							{
								player->SendArena("Zone notify DISABLED");
								player->ZoneNotify = false;
								if (player->player->regform)
									player->player->regform->NotifyZone = 0;
							}
							return true;
						};
					}
					
					if (player->ZoneNotify = false)
					{
						player->SendArena("Zone notify is currently DISABLED");
					}
					else
					{
						player->SendArena("Zone notify is currently ENABLED");
					}

					return true;
				}

				if (MatchString(command, "?extchat", 8))
				{
					// Check if there is a request for a certain state.
					if (length >= 10)
					{
						switch (command[9])
						{
						case 'o':
						case 'O':
							if (length >= 11)
							{
								if ((command[10] != 'f') && (command[10] != 'F'))
								{
									player->SendArena("External chat ENABLED");
									Packet->Source->BroadcastChat = true;
								}
								else
								{
									player->SendArena("External chat DISABLED");
									Packet->Source->BroadcastChat = false;
								}

								return true;
							}
						case 'y':
						case 'Y':
						case '1':
							{
								player->SendArena("External chat ENABLED");
								Packet->Source->BroadcastChat = true;
							}
							return true;
						case 'n':
						case 'N':
						case '0':
							{
								player->SendArena("External chat DISABLED");
								Packet->Source->BroadcastChat = false;
							}
							return true;
						};
					}

					if (Packet->Source->BroadcastChat == false)
					{
						player->SendArena("External chat is currently DISABLED");
					}
					else
					{
						player->SendArena("External chat is currently ENABLED");
					}

					return true;
				}

				if (MatchString(command, "?bantext", 8))
				{
					_listnode <Zone> * zparse = DATABASE.ZoneDatabase.head;
					Zone * zone = NULL;

					while (zparse)
					{
						if (zparse->item->ScoreID == Packet->Source->ScoreID)
						{
							zone = zparse->item;

							break;
						}

						zparse = zparse->next;
					}

					if (zone)
					{
						if (length >= 9)
						{
							char Comment[128];

							memset(Comment, 0, 128);
							memcpy(Comment, &command[9], TrimInt(length - 9, 127));
							memcpy(zone->BanText, Comment, 128);

							player->SendArena("Ban text changed.");
						}
						else
						{
							_string s;
							s = "Ban text: ";
							s += zone->BanText;
							player->SendArena(s.Text);
						}
					}
					else
					{
						player->SendArena("This zone is not registered. Only registered zones may have ban text");
					}

					return true;
				}

			case 3:		///////  Level 3 Operators - Super Moderators  ///////
				if (MatchString(command, "?addop", 6))
				{
					if (length > 7)
					{
						// Uses the current zone as a reference point
						// ?addop -g* -0 name:pw	Network Administrators
						// ?addop -g* -1 name:pw	Hosts
						// ?addop -g* -2 name:pw	SysOps
						// ?addop -g* -3 name:pw	Super Moderators
						// ?addop -g* -4 name:pw	Moderators
						// -g = group-wide (default = zone-wide)
						// -g=GroupID = new group-wide (default = zone-wide)
						// -* = network-wide (default = use -g switch settings)
						// -# = operator level (default = 4)

						SHORT Offset;
						LONG GroupID = 0;
						bool NetworkWide = false,
							 GroupWide = false;
						BYTE Level = 4;
						char GroupString[8];
						int Index;

						for (Offset = 7; Offset < length; Offset++)
						{
							if (command[Offset] == '-')
							{
								Offset++;

								if (Offset > length)
								{
									player->SendArena("Invalid switch");

									return true;
								}

								do
								{
									switch (command[Offset])
									{
									case '*':
										NetworkWide = true;

										break;
									case 'g':	// -g			(use current group id)
									case 'G':	// -g=Number	(specify a group id)
										Offset++;

										if ((Offset + 1) > length)
										{
											player->SendArena("Invalid switch");

											return true;
										}

										if (command[Offset] == '=')
										{
											Offset++;
											Index = 0;
											memset(GroupString, 0, 8);

											while (Offset < length)
											{
												if ((command[Offset] < 48) || (command[Offset] > 57))
												{
													GroupID = atoi(GroupString);

													Offset--;

													break;
												}
												else
												{
													GroupString[Index++] = command[Offset];

													if (Index >= 8)
													{
														player->SendArena("Invalid switch");

														return true;
													}
												}

												Offset++;
											};
										}
										else
										{
											Offset--;
										}

										GroupWide = true;

										break;
									case '0':
										Level = 0;

										break;
									case '1':
										Level = 1;

										break;
									case '2':
										Level = 2;

										break;
									case '3':
										Level = 3;

										break;
									case '4':
										Level = 4;

										break;
									default:
										player->SendArena("Invalid switch");

										return true;
									};

									Offset++;

									if (Offset > length)
									{
										player->SendArena("Invalid switch");

										return true;
									}
								} while (command[Offset] != ' ');
							}
							else if (command[Offset] != ' ')
								break;
						}

						if (Offset == length)
						{
							player->SendArena("This command requires parameters. Type ?man addop for details");

							return true;
						}

						bool Allow = false;

						if ((NetworkWide == false) && (GroupID == 0))
							GroupID = Packet->Source->GroupID;

						// Check if op is in the same sector of the biller
						if (player->OpStatus->GroupID == 0)
							Allow = true;
						else if (player->OpStatus->GroupID == GroupID)
						{
							if (player->OpStatus->ScoreID == 0)
								Allow = true;
							else if ((GroupWide == false) && (player->OpStatus->ScoreID == player->zone->ScoreID))
								Allow = true;
						}

						if (Allow == false)
						{
							player->SendArena("You do not have authority to register operators outside your sector");

							return true;
						}

						// Check if op has a restricted access level
						switch (player->OpStatus->Level)
						{
						case 1:	if (Level < 1) Allow = false;
							break;
						case 2:	if (Level < 2) Allow = false;
							break;
						case 3:	if (Level < 4) Allow = false;
							break;
						case 4:	Allow = false;
							break;
						};

						if (Allow == false)
						{
							player->SendArena("You do not have authority to register administrators below your level");

							return true;
						}

						BYTE Name[32];
						BYTE Password[32];
						memset(Name, 0, 32);
						memset(Password, 0, 32);

						// Parse input
						SHORT	LeftLength;
						char  * RightText;

						if (ColonParse(&command[Offset], length, LeftLength, RightText))
						{
							memcpy(Name, &command[Offset], LeftLength);
							memcpy(Password, RightText, length - LeftLength);

							_listnode <User> * parse = DATABASE.FindUser(Name);

							if (parse)
							{
								HashPassword(Password);

								_string s;
								s  = (char*)Name;
								s += ": Operator #";
								s += ++DATABASE.NextUniversalOpID;
								s += " @ level ";
								s += Level;
								if (NetworkWide)
								{
									s += " added network-wide.";

									DATABASE.OpDatabase.Append(new Operator(parse->item, DATABASE.NextUniversalOpID, 0, 0, Level, (char*)&Password[0]));
								}
								else
								{
									if (GroupWide)
									{
										s += " added group-wide.";

										DATABASE.OpDatabase.Append(new Operator(parse->item, DATABASE.NextUniversalOpID, GroupID, 0, Level, (char*)&Password[0]));
									}
									else
									{
										s += " added zone-wide.";

										DATABASE.OpDatabase.Append(new Operator(parse->item, DATABASE.NextUniversalOpID, GroupID, Packet->Source->ScoreID, Level, (char*)&Password[0]));
									}
								}

								player->SendArena(s.Text);
							}
							else
							{
								player->SendArena("Specified player not found");
							}
						}
						else
						{
							player->SendArena("Invalid parameters. Type ?man addop for details");
						}
					}
					else
					{
						player->SendArena("This command requires parameters. Type ?man addop for details");
					}

					return true;
				}

				if (MatchString(command, "?removeop", 9))
				{	// Unregister operator
					if (length > 10)
					{
						char Number[8];
						int Index;

						memset(Number, 0, 8);
						memcpy(Number, &command[10], TrimInt(length - 10, 7));
						Index = atoi(Number);

						if (Index < 1)
						{
							player->SendArena("Invalid operator ID");
							return true;
						}

						_listnode <Operator> * op_parse = DATABASE.FindOp(Index);

						if (op_parse)
						{
							Operator * op = op_parse->item;

							if (op != player->OpStatus)
							{
								bool Allow = true;

								// Check if op has a restricted access level
								switch (player->OpStatus->Level)
								{
									case 1:	if (op->Level <= 1)	Allow = false;
										break;
									case 2:	if (op->Level <= 2)	Allow = false;
										break;
									case 3:	if (op->Level <= 3)	Allow = false;
										break;
									case 4:	Allow = false;
										break;
								};

								// Send line if access is disallowed.
								if (Allow == false)
								{
									player->SendArena("You do not have authority to remove administrators at or below your level");

									return true;
								}
							}

							// Erase operator sessions with everyone online
							_listnode <Host> * parse = Packet->Source->Socket->HostList.head;

							while (parse)
							{
								Host * host = parse->item;

								_listnode <LUserNode> * parse_user = host->UserList.head;

								while (parse_user)
								{
									LUserNode * user = parse_user->item;

									if (user->OpStatus == op)
										user->OpStatus = NULL;

									parse_user = parse_user->next;
								}

								parse = parse->next;
							}

							// Delete the operator
							DATABASE.OpDatabase.Delete(op_parse, false);

							player->SendArena("Operator unregistered. All active sessions with this operator are closed");
						}
						else
						{
							player->SendArena("Specified operator slot is empty");
						}
					}
					else
					{
						player->SendArena("This command requires parameters. Type ?man removeop for details");
					}

					return true;
				}

				if (MatchString(command, "?changeop", 9))
				{	// Change operator password ?changeop #:pw
					if (length <= 10)
					{
						player->SendArena("This command requires parameters. Type ?man changeop for details");
						return true;
					}

					char Number[8];		// Op number (ASCII buffer)
					int Offset,			// Offset to password
						Index;			// Op number (Decimal form)

					memset(Number, 0, 8);
					for (Offset = 10; Offset < length; Offset++)
					{
						if (command[Offset] == ':')
						{
							memcpy(Number, &command[10], TrimInt(Offset - 10, 7));

							break;
						}
					}

					Index = atoi(Number);
					Offset++;

					if (Offset > length)
					{
						char Password[32];

						memset(Password, 0, 32);
						memcpy(Password, &command[10], TrimInt(length - 10, 31));

						HashPassword((BYTE*)&Password[0]);

						memcpy(player->OpStatus->Password, Password, 32);

						player->SendArena("Operator password successfully changed");

						return true;
					}

					if (Index < 1)
					{
							player->SendArena("Invalid operator ID");
							return true;
					}

					_listnode <Operator> * parse = DATABASE.FindOp(Index);

					if (parse)
					{
						Operator * op = parse->item;
						bool Allow = true;

						if (op != player->OpStatus)
						{
							// Check if op has a restricted access level
							if ((player->OpStatus->Level != 0) && (op->Level <= player->OpStatus->Level))
								Allow = false;

							// Send line if access is disallowed.
							if (Allow == false)
							{
								player->SendArena("You do not have authority to change operators' passwords at or below your level");

								return true;
							}
						}

						char Password[32];

						memset(Password, 0, 32);
						memcpy(Password, &command[Offset], TrimInt(length - Offset, 31));

						HashPassword((BYTE*)&Password[0]);

						memcpy(parse->item->Password, Password, 32);

						player->SendArena("Operator password changed");
					}
					else
					{
						player->SendArena("Specified operator slot is empty");
					}

					return true;
				}

				if (MatchString(command, "?info", 5))
				{	// ?info <name>
					User * user;

					if (length <= 6)
					{
						user = player->player;
					}
					else
					{
						_listnode <User> * parse = DATABASE.FindUser((BYTE*)&command[6]);
						if (parse)
							user = parse->item;
						else if (command[6] == '#')
						{
							user = NULL;

							char number[8];
							memset(number, 0, 8);
							memcpy(number, &command[7], TrimInt(length - 7, 7));
							int index = atoi(number);

							if (index > 0)
							{
								_listnode <User> * uparse = DATABASE.UserDatabase.head;

								while (uparse)
								{
									if (uparse->item->LoadID == index)
									{
										user = uparse->item;

										break;
									}
									uparse = uparse->next;
								}
							}
						}
						else
						{
							user = NULL;
						}
					}


					if (user)
					{
						_string s;
						tm * CDate;

						s  = (char*)&user->Name[0];
						s += " (#";
						s += user->LoadID;
						s += ") Last known information:";

						player->SendArena(s.Text);

						s  = "Creation: ";
						CDate = Localize(&user->CreationDate, player->player->TimeZoneBias);
						if (CDate)
						{
							s += CDate->tm_mon + 1;
							if (CDate->tm_mday < 10)
								s += "-0";
							else
								s += "-";
							s += CDate->tm_mday;
							s += "-";
							s += GetYear(CDate);
						}
						else
						{
							s += "(Invalid date)";
						}
						s += ", Login: ";
						CDate = Localize(&user->LastLogin, player->player->TimeZoneBias);
						if (CDate)
						{
							s += CDate->tm_mon + 1;
							if (CDate->tm_mday < 10)
								s += "-0";
							else
								s += "-";
							s += CDate->tm_mday;
							s += "-";
							s += GetYear(CDate);
						}
						else
						{
							s += "(Invalid date)";
						}
						s += ", PassBlock: ";
						switch (user->PassBlock)
						{
						case BLOCK_OK:		s += "Normal";
							break;
						default:			s += "Banfree";
							break;
						};
						s += ", TZB: ";
						s += user->TimeZoneBias;

						player->SendArena(s.Text);

						s  = "IP: ";
						s += (int)(((unsigned char*)&user->IP)[0]);
						s += ".";
						s += (int)(((unsigned char*)&user->IP)[1]);
						s += ".";
						s += (int)(((unsigned char*)&user->IP)[2]);
						s += ".";
						s += (int)(((unsigned char*)&user->IP)[3]);
						s += ", MachineID: ";
						s += user->MacID;
						s += ", HardwareID: ";
						if (user->regform)
							s += user->regform->HardwareID;
						else
							s += "(none)";

						player->SendArena(s.Text);
					}
					else
					{
						player->SendArena("Specified user was not found in the database");
					}

					return true;
				}
			};		///////  Level 4 Operators - Moderators  ///////

				if (MatchString(command, "?warn", 5))
				{	// ?changepassword Name:Password
					if (length <= 6)
					{
						player->SendArena("This command requires parameters. Type ?man warn for details");
						return true;
					}

					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in this zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != Packet->Source->GroupID)
							{
								player->SendArena("You only have limited powers in this zone");
								return true;
							}
						}
					}

					BYTE Name[32];
					BYTE Text[128];
					memset(Name, 0, 32);
					memset(Text, 0, 128);

					// Parse input
					SHORT	LeftLength;
					char  * RightText;

					if (ColonParse(&command[6], length - 6, LeftLength, RightText))
					{
						// Find user
						memcpy(Name, &command[6], LeftLength);

						_listnode <Host> * hparse = (Packet->Source->Socket)->HostList.head;
						while (hparse)
						{
							// Find user
							_listnode <LUserNode> * user = hparse->item->Find((char*)&Name[0], true);

							if (user)
							{
								_string s;
								s = "WARNING: ";
								s += RightText;
								s += " -";
								s += (char*)&player->player->Name[0];
								user->item->SendChat(0, s.Text);
								player->SendArena("Player warned");
								return true;
							}

							// Try next zone
							hparse = hparse->next;
						}

						player->SendArena("User not found");

						return true;
					}
					else
					{
						player->SendArena("This command requires parameters. Type ?man warn for details");
						return true;
					}
				}
			
				if (MatchString(command, "?banfree", 8))
				{	// ?banfree <name>

					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in this zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != Packet->Source->GroupID)
							{
								player->SendArena("You only have limited powers in this zone");
								return true;
							}
						}
					}

					User * user;
					BYTE OpLevel = player->OpStatus->Level;
					int NameLen = 0;
					BYTE Name[32] = "";
					bool NetWide = false;
					_string s;
					
					if (length <= 9)
					{
						player->SendArena("You must specify a player");
						return true;
					}
					else
					{
						int i = 9;
						while (i < length)
						{
							if (command[i] == '-')
							{
								i++;
								switch (command[i])
								{
									case '0':
									case '1':
									case '2':
									case '3':
									case '4':
										OpLevel = command[i] - 48;
										if (OpLevel < player->OpStatus->Level)
										{
											player->SendArena("You cannot add banfrees with levels greater than your own");
											return true;
										}
										break;
									case '*':
										if ((player->OpStatus->Level < 2) && (player->OpStatus->GroupID == 0))
										{
											NetWide = true;
										}
										else
										{
											player->SendArena("You need level 1 network-wide access to add network-wide banfrees");
											return true;
										}
										break;
								};
							}
							else if (command[i] != ' ')
							{
								for (int n = i; n < length; n++)
								{
									Name[NameLen++] = command[n];
									if (NameLen >= 20)
									{
										player->SendArena("Name: Specified name ludicrously long(Not over 20 characters please)");
										return true;
									}
								}
								break;
							}

							i++;
						}

						_listnode <User> * parse = DATABASE.FindUser(Name);

						if (parse)
							user = parse->item;
						else
						{
							user = NULL;

							char number[8];
							memset(number, 0, 8);
							memcpy(number, &command[9], TrimInt(length - 6, 7));
							int index = atoi(number);

							if (index > 0)
							{
								_listnode <User> * uparse = DATABASE.UserDatabase.head;

								while (uparse)
								{
									if (uparse->item->LoadID == index)
									{
										user = uparse->item;

										break;
									}

									uparse = uparse->next;
								}
							}
						}
					}

					if (user)
					{
						_listnode <Banfree> * bfparse = DATABASE.BanfreeDatabase.head;
						LONG Counter = 0;
						while(bfparse)
						{
							Counter++;
							if (bfparse->item->Owner == user->LoadID && ((bfparse->item->GroupID == 0 && NetWide ) || (!NetWide && bfparse->item->GroupID == Packet->Source->GroupID && (bfparse->item->ScoreID == Packet->Source->ScoreID || !bfparse->item->ScoreID))))
							{
								/*if ((bfparse->item->Level != OpLevel) && (bfparse->item->Level >= player->OpStatus->Level))
								{
									s = "The player or one of the players aliases was already banfreed under another accesslevel. Changed accesslevel to ";
									s += OpLevel;
									player->SendArena(s.Text);
									bfparse->item->Level = OpLevel;
									return true;
								}
								else if (bfparse->item->Level < player->OpStatus->Level)
								{
									player->SendArena("This player is banfreed under a higher level than yours. Cannot change level");
									return true;
								}
								else
								{*/
									player->SendArena("The player is already banfreed");
									_string s;
									s = "Match: banfree #";
									s += Counter;
									player->SendArena(s.Text);
									return true;
								//}
							}
							bfparse = bfparse->next;
						}

						LONG i1 = 0, i2 = 0;
					
						if (!NetWide)
						{
							i1 = Packet->Source->GroupID;
							i2 = Packet->Source->ScoreID;
						}

						_listnode <Ban> * bparse = DATABASE.BanDatabase.head;
						while (bparse)
						{
							if ((bparse->item->UserID == user->LoadID) || (bparse->item->MachineID == user->MacID))
							{
								if ((bparse->item->GroupID == 0) || (i1 == 0) ||
									(bparse->item->GroupID == i1) || (bparse->item->ScoreID == i2 && i2))
								{
									if (bparse->item->DenyBanfree == 1)
									{
										player->SendArena("This player cannot be banfreed. The player or one of the players aliases is banned with the -b switch");
										_string s;
										s = "First match: Ban #";
										s += bparse->item->BanID;
										player->SendArena(s.Text);
										return true;
									}
								}
							}
							bparse = bparse->next;
						}
						
						Banfree * banfree = new Banfree(++DATABASE.NextUniversalBanfreeID, i1, i2, user->LoadID, OpLevel);
						banfree->Player = user;
						DATABASE.BanfreeDatabase.Append(banfree);

						s  = "Banfree #";
						s += banfree->BanfreeID;
						s += " added: UserId #";
						s += user->LoadID;
						s += ". Banfree level ";
						s += OpLevel;

						player->SendArena(s.Text);
					}
					else
					{
						player->SendArena("Specified user was not found in the database");
						player->SendArena(Name);
					}

					return true;
				}

				if (MatchString(command, "?listbanfree", 12))
				{

					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in this zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != Packet->Source->GroupID)
							{
								player->SendArena("You only have limited powers in this zone");
								return true;
							}
						}
					}

					_listnode <Banfree> * parse = DATABASE.BanfreeDatabase.head;
					bool Found = false;
					bool ShowNetWide = false;

					if (length > 13)
					{
						int i = 13;
						while (i < length)
						{
							if (command[i] == '-')
							{
								i++;
								switch (command[i])
								{
									case '*':
										if (player->OpStatus->GroupID == 0) 
										{
											ShowNetWide = true;
										}
										else
										{
											player->SendArena("You cannot see banfrees network-wide");
											return true;
										}
										break;
								};
							}
							i++;
						}
					}

					while (parse)
					{
						Banfree * banfree = parse->item;

						if (banfree->Player == NULL)
							DATABASE.ComputeBanfree(banfree);

						_string s;
						s  = banfree->BanfreeID;
						if (banfree->ScoreID == 0) //Net or group wide
						{
							if (banfree->GroupID == 0) //Netwide
							{
								s += "*) ";
							}
							else //Groupwide
							{
								s += "$) {";
								s += banfree->GroupID;
								s += "} ";
							}
						}
						else //zonewide
						{
							s += ") <";
							Host * host = Packet->Source->Socket->FindHost(banfree->ScoreID);
							if (host)
								s += (char*)&host->Name[0];
							else
								s += banfree->ScoreID;
							s += "> ";
						}

						if (banfree->Player)
						{
							s += (char*)&banfree->Player->Name[0];
						}
						else
						{
							s += "UserId #";
							s += banfree->Owner;
						}
						
						s += ":";
						s += banfree->Level;

						bool send = false;
						if (banfree->ScoreID == 0) //Group-wide or net-wide
						{
							if (banfree->GroupID == 0) //Net-wide
							{
								send = true;
							}
							else //Group-wide
							{
								if ((banfree->GroupID == Packet->Source->GroupID) || (ShowNetWide)) send = true;
							}
						}
						else //Zone-wide
						{
							if ((banfree->ScoreID == Packet->Source->ScoreID) || (ShowNetWide)) send = true;
						}


						if (send == true) 
						{
							Found = true;
							player->SendArena(s.Text);
						}

						parse = parse->next;
					}

					if (Found == false)
						player->SendArena("No banfrees found");

					return true;
				}

				if (MatchString(command, "?liftbanfree", 12))
				{	// ?liftbanfree <#>

					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in this zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != Packet->Source->GroupID)
							{
								player->SendArena("You only have limited powers in this zone");
								return true;
							}
						}
					}

					if (length > 13)
					{
						// Find ban
						char Number[5];
						int Index;

						memset(Number, 0, 5);
						memcpy(Number, &command[13], TrimInt(length - 13, 4));
						Index = atoi(Number);

						_listnode <Banfree> * parse = DATABASE.BanfreeDatabase.head;

						while (parse)
						{
							if (parse->item->BanfreeID == Index)
							{
								if (parse->item->Level < player->OpStatus->Level)
								{
									player->SendArena("You do not have the authority to remove the banfree");
									return true;
								}

								DATABASE.BanfreeDatabase.Delete(parse, false);

								player->SendArena("Banfree lifted");

								return true;
							}

							parse = parse->next;
						}

						player->SendArena("Specified banfree slot is empty");
					}

					return true;
				}

				if (MatchString(command, "?notifyban", 8))
				{
					// Check if there is a request for a certain state.
					if (length >= 12)
					{
						switch (command[11])
						{
						case 'o':
						case 'O':
							if (length >= 13)
							{
								if ((command[13] != 'f') && (command[13] != 'F'))
								{
									player->SendArena("Ban notify ENABLED");
									player->BanNotify = true;
									if (player->player->regform)
										player->player->regform->NotifyBan = 1;
								}
								else
								{
									player->SendArena("Ban notify DISABLED");
									player->BanNotify = false;
									if (player->player->regform)
										player->player->regform->NotifyBan = 0;
								}

								return true;
							}
						case 'y':
						case 'Y':
						case '1':
							{
								player->SendArena("Ban notify ENABLED");
								player->BanNotify = true;
								if (player->player->regform)
									player->player->regform->NotifyBan = 1;
							}
							return true;
						case 'n':
						case 'N':
						case '0':
							{
								player->SendArena("Ban notify DISABLED");
								player->BanNotify = false;
								if (player->player->regform)
									player->player->regform->NotifyBan = 0;
							}
							return true;
						};
					}

					if (player->BanNotify == false)
					{
						player->SendArena("Ban notify is currently DISABLED");
					}
					else
					{
						player->SendArena("Ban notify is currently ENABLED");
					}

					return true;
				}

				if (MatchString(command, "?changeop", 9))
				{	// Change operator password ?changeop pw
					if (length <= 10)
					{
						player->SendArena("This command requires parameters. Type ?man changeop for details");
						return true;
					}

					char Password[32];

					memset(Password, 0, 32);
					memcpy(Password, &command[10], TrimInt(length - 10, 31));
					HashPassword((BYTE*)&Password[0]);
					memcpy(player->OpStatus->Password, Password, 32);

					player->SendArena("Operator password changed");

					return true;
				}

				if (MatchString(command, "?listban", 8))
				{

					if (player->OpStatus->Level > 2) //SysOp and above can see in all zones
					{
						if (player->OpStatus->ScoreID != 0) //Zone-wide op?
						{
							if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
							//No biller access in this zone?
							{
								player->SendArena("You only have limited powers in this zone");
								player->SendArena("You need to have at least level 2 access to see bans in this zone");
								return true;
							}
						}
						else //Not a zone-wide op
						{
							if (player->OpStatus->GroupID != 0)
							//Group-wide
							{
								if (player->OpStatus->GroupID != Packet->Source->GroupID)
								{
									player->SendArena("You only have limited powers in this zone");
									player->SendArena("You need to have at least level 2 access to see bans in this zone");
									return true;
								}
							}
						}
					}

					bool Found = false;
					bool ShowDetails = false;
					bool ShowNetWide = false;
					//char PageString[8];
					//int Index;
					//int Page;
					LONG BanNr = NULL;

					_listnode <Ban> * parse = DATABASE.BanDatabase.head;
					_string s;

					if (length > 9)
					{
						int i = 9;
						while (i < length)
						{
							if (command[i] == '-')
							{
								i++;
								switch (command[i])
								{
									case 'd':
									case 'D':
										ShowDetails = true;
										break;
									case '*':
										if (player->OpStatus->GroupID == 0)
										{
											ShowNetWide = true;
										}
										else
										{
											player->SendArena("You cannot see bans network-wide");
										}
										break;
								};
							}
							else if (command[i] != ' ')
							{
								BanNr = atol(&command[i]);
								break;
							}
							i++;
						}
					}

					if (BanNr)
					{
						if (player->OpStatus->GroupID == 0) ShowNetWide = true;
						ShowDetails = true;
					}

					while (parse)
					{
						Ban * ban = parse->item;

						bool send = false;
						
						if (BanNr)
						{
						if (BanNr != ban->BanID)
						{
							parse = parse->next;
							continue;
						}
						}

							s = ban->BanID;
							if (ban->GroupID == 0)
								s += "*) ";
							else if (ban->ScoreID == 0)
							{
								s += "$) {";
								s += ban->GroupID;
								s += "} ";
							}
							else
							{
								s += ") <";
								Host * host = Packet->Source->Socket->FindHost(ban->ScoreID);
								if (host)
									s += (char*)&host->Name[0];
								else
									s += ban->ScoreID;
								s += "> ";
							}
							s += ban->Name;

							if (ShowDetails)
							{
								s += " [";
								switch (ban->IPEnd ^ ban->IPStart)
								{
								case 0:
									{
										s += (int)(((unsigned char*)&ban->IPStart)[0]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[1]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[2]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[3]);
									}
									break;
								case 0xFF000000:
									{
										s += (int)(((unsigned char*)&ban->IPStart)[0]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[1]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[2]);
										s += ".*";
									}
									break;
								case 0xFFFF0000:
									{
										s += (int)(((unsigned char*)&ban->IPStart)[0]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[1]);
										s += ".*.*";
									}
									break;
								case 0xFFFFFF00:
									{
										s += (int)(((unsigned char*)&ban->IPStart)[0]);
										s += ".*.*.*";
									}
									break;
								default:
									{
										s += (int)(((unsigned char*)&ban->IPStart)[0]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[1]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[2]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPStart)[3]);
										s += "-";
										s += (int)(((unsigned char*)&ban->IPEnd)[0]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPEnd)[1]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPEnd)[2]);
										s += ".";
										s += (int)(((unsigned char*)&ban->IPEnd)[3]);
									}
									break;
								};
								s += "] until ";
								tm * CDate = Localize(&ban->Timer, player->player->TimeZoneBias);
								if (CDate)
								{
									s += CDate->tm_mon + 1;
									if (CDate->tm_mday < 10)
										s += "-0";
									else
										s += "-";
									s += CDate->tm_mday;
									s += "-";
									s += GetYear(CDate);
								}
								else
								{
									s += "(Invalid date)";
								}
								s += ". Set ";
								CDate = Localize(&ban->CreationDate, player->player->TimeZoneBias);
								if (CDate)
								{
									s += CDate->tm_mon + 1;
									if (CDate->tm_mday < 10)
										s += "-0";
									else
										s += "-";
									s += CDate->tm_mday;
									s += "-";
									s += GetYear(CDate);
								}
								else
								{
									s += "(Invalid date)";
								}
								s += " by ";
								if (ban->Creator)
									s += (char*)&ban->Creator->Name[0];
								else
									s += "(Unknown)";
								s += ":";
								s += ban->OpLevel;
								if (ban->DenyBanfree) s += ". No banfrees allowed";
								if (ban->LastLogin != ban->CreationDate)
								{
									CDate = Localize(&ban->LastLogin, player->player->TimeZoneBias);
									if (CDate)
									{
										s += ". Last access ";
										s += CDate->tm_mon + 1;
										if (CDate->tm_mday < 10)
											s += "-0";
										else
											s += "-";
										s += CDate->tm_mday;
										s += "-";
										s += GetYear(CDate);
										s += " ";
										s += CDate->tm_hour;
										if (CDate->tm_min < 10)
											s += ":0";
										else
											s += ":";
										s += CDate->tm_min;
										if (CDate->tm_sec < 10)
											s += ".0";
										else
											s += ".";
										s += CDate->tm_sec;
									}
									else
									{
										s += ". Last access (Invalid date)";
									}
								}
							}
							if (ban->ScoreID == 0) //Group-wide or net-wide
							{
								if (ban->GroupID == 0) //Net-wide
								{
									send = true;
								}
								else //Group-wide
								{
									if ((ban->GroupID == Packet->Source->GroupID) || (ShowNetWide)) send = true;
								}
							}
							else //Zone-wide
							{
								if ((ban->ScoreID == Packet->Source->ScoreID) || (ShowNetWide)) send = true;
							}
								
							if (send)
							{
								player->SendArena(s.Text);

								// Send ban comment
								if (strlen(ban->Comment))
									player->SendArena(ban->Comment);
								Found = true;
							}

						parse = parse->next;
					}

					if (!Found)
					{
						if (BanNr)
						{
							player->SendArena("Ban not found");
						}
						else
						{
							player->SendArena("No bans found");
						}
					}

					return true;
				}

				if (MatchString(command, "?commentban", 11))
				{
					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in this zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != Packet->Source->GroupID)
							{
								player->SendArena("You only have limited powers in this zone");
								return true;
							}
						}
					}

					if (length <= 12)
					{
						player->SendArena("This command requires parameters. Type ?man commentban for details");
						return true;
					}

					// Find ban
					char Number[5];		// Ban number (ASCII buffer)
					int Offset,			// Offset to comment
						Index;			// Ban number (Decimal form)

					memset(Number, 0, 5);
					for (Offset = 12; Offset < length; Offset++)
					{
						if (command[Offset] == ':')
						{
							memcpy(Number, &command[12], TrimInt(Offset - 12, 4));

							break;
						}
					}
					Index = atoi(Number);
					Offset++;

					if (Offset > length)
					{
						player->SendArena("SYNTAX: ?commentban <Ban ID>:<Comment>");
						return true;
					}

					if ((Index > 0) && (Index <= DATABASE.NextUniversalBanID))
					{
						_listnode <Ban> * parse = DATABASE.BanDatabase.head;

						while (parse)
						{
							Ban * ban = parse->item;

							if (ban->BanID == Index)
							{
								if ( (player->OpStatus->GroupID == 0) ||
									 ((player->OpStatus->ScoreID == 0) && (player->OpStatus->GroupID == ban->GroupID)) ||
									 (player->OpStatus->ScoreID == ban->ScoreID) ||
									 (player->OpStatus->Level <= ban->OpLevel) )
								{
									// Trigger comment
									char Comment[128];

									memset(Comment, 0, 128);
									memcpy(Comment, &command[Offset], TrimInt(length - Offset, 127));
									memcpy(parse->item->Comment, Comment, 128);

									player->SendArena("Ban comment changed");
								}
								else
								{
									// Show error
									player->SendArena("You do not have authority to comment this ban");
								}

								return true;
							}

							parse = parse->next;
						}
						player->SendArena("Specified ban slot is empty");
					}
					else
					{
						player->SendArena("Specified ban slot is empty");
					}

					return true;
				}

				if (MatchString(command, "?liftban", 8))
				{
					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in this zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != Packet->Source->GroupID)
							{
								player->SendArena("You only have limited powers in this zone");
								return true;
							}
						}
					}

					if (length > 9)
					{
						// Find ban
						char Number[5];
						int Index;

						memset(Number, 0, 5);
						memcpy(Number, &command[9], TrimInt(length - 9, 4));
						Index = atoi(Number);

						if ((Index > 0) && (Index <= DATABASE.NextUniversalBanID))
						{
							_listnode <Ban> * parse = DATABASE.BanDatabase.head;

							while (parse)
							{
								Ban * ban = parse->item;

								if (ban->BanID == Index)
								{
									if ( ((player->OpStatus->GroupID == 0) ||
										 ((player->OpStatus->ScoreID == 0) && (player->OpStatus->GroupID == ban->GroupID)) ||
										 (player->OpStatus->ScoreID == ban->ScoreID)) &&
										 (player->OpStatus->Level <= ban->OpLevel) )
									{
										// Trigger delete
										DATABASE.UnbanUser(ban);

										player->SendArena("Ban lifted");
									}
									else
									{
										// Check for permission
										player->SendArena("You do not have authority to lift this ban");
									}

									return true;
								}

								parse = parse->next;
							}

							player->SendArena("Specified ban slot is empty");
						}
						else
						{
							player->SendArena("Specified ban slot is empty");
						}
					}

					return true;
				}

				if (MatchString(command, "?ban", 4))
				{
					if (length <= 5)
					{
						player->SendArena("This command requires parameters. Type ?man ban for details");
						return true;
					}

					bool Kick = true,		// Kick player after adding ban
						 UseRange = false,	// Use IP range in the ban
						 AutoBlock = false,	// WhoIs hostname to get NetBlock
						 AutoRange = false;	// Automatically do a subnet ban
					LONG RangeStart,		// IPStart
						 RangeEnd,			// IPEnd
						 Offset,			// ParseIP's 5th argument
						 GroupID,			// Network-wide ban
						 ScoreID;			// Group-wide ban
					char Name[32];			// Buffer containing parsed player name
					int	 NameLen = 0;		// Length of name buffer
					BYTE OpLevel;			// Operator access level
					long Days = 1;			// Ban days
					char DayString[8];		// Ban days as a string
					int	 Index;				// Used to parse the ban days string
					char GrpString[8];		// GroupID as a string
					int	 GrpIndex;			// Used to parse the group string
					BYTE DenyBanfree = 0;	// Unbanfreeable
					char ScoreIDString[8];
					int  ScoreIDIndex;
					_string s;

					/* Set default values */
					GroupID = Packet->Source->GroupID;
					ScoreID = Packet->Source->ScoreID;
					OpLevel = player->OpStatus->Level;

					/* Parse switches */
					int i = 5;
					while (i < length)
					{
						if (command[i] == '-')
						{
							// Switches:
							//  -k (don't kick)
							//  -i 127.0.0.1-255.255.255.255 (ip range)
							//  -i 127.0.0.* (ip subnet)
							//  -i 127.0.0.1 (static ip)
							//  -s (subnet only)
							//  -! (entire IP range)
							//  -* (maximum ban influence)
							//  -d=days (day timer)
							//  -b unbanfreeable
							//  -r=scoreid (remote ban)

							i++;
							switch (command[i])
							{
							case 'r':
							case 'R':
								i++;

								if (i >= length)
								{
									player->SendArena("Invalid switch");
									return true;
								}

								if (command[i] == '=')
								{
									i++;
									ScoreIDIndex = 0;
									memset(ScoreIDString, 0, 8);

									while (i < length)
									{
										if ((command[i] < 48) || (command[i] > 57))
										{
											if (player->OpStatus->Level > 1)
											{
												player->SendArena("You cannot remote ban");
												return true;
											}

											ScoreID = atoi(ScoreIDString);

											_listnode <Zone> * parse = DATABASE.FindZone(ScoreID);
											if (!parse)
											{
												player->SendArena("Target zone not found");
												return true;
											}

											/*if (!((player->OpStatus->GroupID == 0) || (player->OpStatus->GroupID == parse->item->GroupID) || (player->OpStatus->ScoreID == parse->item->ScoreID)))
											{
												player->SendArena("You do not the authority to ban in the target zone");
												return true;
											}*/

											i--;

											break;
										}
										else
										{
											ScoreIDString[ScoreIDIndex++] = command[i];

											if (ScoreIDIndex >= 8)
											{
												player->SendArena("Invalid switch");

												return true;
											}
										}

										i++;
									};
								}
								else
								{
									i--;
								}
								break;
							case 'b':
							case 'B':
								DenyBanfree = 1;
								break;
							case 'K':		// Don't kick the player
							case 'k':
								Kick = false;
								break;
							case 'D':		// Ban timer
							case 'd':
								i++;

								if (i >= length)
								{
									player->SendArena("Invalid switch");

									return true;
								}

								if (command[i] == '=')
								{
									i++;
									Index = 0;
									memset(DayString, 0, 8);

									while (i < length)
									{
										if ((command[i] < 48) || (command[i] > 57))
										{
											Days = atoi(DayString);

											if (Days <= 0)
												Days = 1;

											if (Days > 5000)
												Days = 5000;

											switch (player->OpStatus->Level)
											{
											case 2: if (Days > 365) Days = 365;	break;
											case 3: if (Days > 60) Days = 60;	break;
											case 4: if (Days > 7) Days = 7;		break;
											};

											i--;

											break;
										}
										else
										{
											DayString[Index++] = command[i];

											if (Index >= 8)
											{
												player->SendArena("Invalid switch");

												return true;
											}
										}

										i++;
									};
								}
								else
								{
									i--;
								}

								break;
							case 'G':		// Group-wide ban
							case 'g':
								i++;

								if (i >= length)
								{
									player->SendArena("Invalid switch");

									return true;
								}

								if (command[i] == '=')
								{
									i++;
									GrpIndex = 0;
									memset(GrpString, 0, 8);

									while (i < length)
									{
										if ((command[i] < 48) || (command[i] > 57))
										{
											if ((player->OpStatus->GroupID == 0) || (player->OpStatus->ScoreID == 0))
											{
												GroupID = atoi(GrpString);
												ScoreID = 0;
												break;
											}
											else
											{
												player->SendArena("You cannot add group-wide bans");
												return true;
											}
										}
										else
										{
											GrpString[GrpIndex++] = command[i];

											if (GrpIndex >= 8)
											{
												player->SendArena("Invalid switch");

												return true;
											}
										}

										i++;
									};
								}
								else
								{
									if ((player->OpStatus->GroupID == 0) || (player->OpStatus->ScoreID == 0))
									{
										ScoreID = 0;
									}
									else
									{
										player->SendArena("You cannot add group-wide bans");
										return true;
									}
								}
								break;
							case '*':		// Network
								{
									if (player->OpStatus->GroupID == 0)
									{
										GroupID = 0;
										ScoreID = 0;
									}
									/*else if (player->OpStatus->ScoreID == 0)
									{
										ScoreID = 0;
									}*/
									else
									{
										//player->SendArena("-*: You cannot add network-wide/group-wide bans.");
										player->SendArena("-*: You cannot add network-wide bans");

										return true;
									}
								}
								break;
							case '!':		// Automatic ISP-router ban
								{
									if (player->OpStatus->Level <= 2)
									{
										UseRange = true;
										AutoBlock = true;
									}
									else
									{
										player->SendArena("-!: You cannot ban player ISP's");

										return true;
									}
								}
								break;
							case 'S':		// Automatic subnet ban
							case 's':
								{
									if (player->OpStatus->Level <= 3)
									{
										UseRange = true;
										AutoRange = true;
									}
									else
									{
										player->SendArena("-s: You cannot add subnet bans");

										return true;
									}
								}
								break;
							case '0':		// Manual access level
							case '1':
							case '2':
							case '3':
							case '4':
								{
									OpLevel = command[i] - 48;	// ASCII -> Decimal

									if (player->OpStatus->Level > OpLevel)
									{
										player->SendArena("-level: You cannot add bans at the requested access level");

										return true;
									}
								}
								break;
							case 'I':		// Manual IP Range
							case 'i':
								{
									if (player->OpStatus->Level == 0)
									{
										UseRange = true;

										if (length <= (i + 2))
										{
											player->SendArena("Invalid IP range parameters. Aborted");
											return true;
										}

										if (ParseIP(&command[i + 2], length - (i + 2), RangeStart, RangeEnd, Offset))
										{
											i += Offset + 1;
										}
										else
										{
											player->SendArena("Invalid IP range parameters. Aborted");
											return true;
										}
									}
									else
									{
										player->SendArena("-i: You cannot specify an IP range");

										return true;
									}
								}
								break;
							};
						}
						else if (command[i] != ' ')
						{
							for (int n = i; n < length; n++)
							{
								Name[NameLen++] = command[n];

								// Name information
								if (NameLen >= 20)
								{
									player->SendArena("Name: Specified name ludicrously long(Not over 20 characters please)");

									return true;
								}
							}

							break;
						}

						i++;
					}

					Name[NameLen] = 0;

					/* Parse username */
					User * UD;

					_listnode <LUserNode> * parse = Packet->Source->Find(Name, true);

					if (parse)
					{
						UD = parse->item->player;
					}
					else
					{
						_listnode <User> * user = DATABASE.FindUser((BYTE*)&Name[0]);

						if (user)
						{
							UD = user->item;
						}
						else
						{
							player->SendArena("Unable to find user");
							player->SendArena(Name);

							return true;
						}
					}

					// Check if this ban can't be made
					_listnode <Operator> * oparse = DATABASE.OpDatabase.head;

					while (oparse)
					{
						if ((oparse->item->Owner == UD) && (oparse->item->Level <= player->OpStatus->Level) && !oparse->item->GroupID)
						{
					
							player->SendArena("You cannot ban operators at or below your level");

							return true;
						}

						oparse = oparse->next;
					}

					_listnode <Ban> * bparse = DATABASE.BanDatabase.head;
					while (bparse)
					{
						if (bparse->item->UserID == UD->LoadID)
						{
							player->SendArena("NOTICE: Player is already banned(But the ban does not nessecarily affect the target zone)");
							s = "BanID of old ban: ";
							s += bparse->item->BanID;
							player->SendArena(s.Text);
							break;
						}
						bparse = bparse->next;
					}

					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in the target zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != GroupID)
							{
								player->SendArena("You only have limited powers in the target zone");
								return true;
							}
						}
					}

					// Apply switch settings to IP range
					if (UseRange)
					{
						if (AutoRange)
						{
							RangeStart	= UD->IP & 0x00FFFFFF;
							RangeEnd	= UD->IP | 0xFF000000;
						}
						else if (AutoBlock)
						{
							player->SendArena("Please wait while downloading information for NetBlock...");//from whois.arin.net...");
							if (!UD->IP || !GetBlock(UD->IP, RangeStart, RangeEnd))
							{
								player->SendArena("Unable to calculate NetBlock. Try your request again soon");

								return true;
							}
						}
					}
					else
					{
						RangeStart = RangeEnd = UD->IP;
					}

					if (DenyBanfree)
					{
						int Counter = 0;

						_listnode <Banfree> * bfparse = DATABASE.BanfreeDatabase.head;
						while (bfparse)
						{
							Counter++;
							if (bfparse->item->Player == NULL)
								DATABASE.ComputeBanfree(bfparse->item);
							if ((UD->LoadID == bfparse->item->Player->LoadID) || (UD->MacID == bfparse->item->Player->MacID))
							{
								player->SendArena("This player or one of the players aliases has at least one banfree. Cannot ban with -b switch unless there are no banfrees for the player");
								_string s;
								s = "First match: Banfree #";
								s += Counter;
								player->SendArena(s.Text);
								return true;
							}
							bfparse = bfparse->next;
						}
					}

					/* Add ban */
					DATABASE.BanUser(UD, GroupID, ScoreID, OpLevel, "", RangeStart, RangeEnd, UD->MacID, (UD->regform ? UD->regform->HardwareID : 0), Days, player->player, DenyBanfree);

					// Kick the player if directed
					if (parse && Kick) parse->item->Kick(0, DATABASE.NextUniversalBanID, DATABASE.NextUniversalBanID);

					s  = "Ban #";
					s += DATABASE.NextUniversalBanID;

					if (GroupID == 0)
						s += " added network-wide.";
					else if (ScoreID == 0)
					{
						s += " added group-wide( Group";
						s += GroupID;
						s += ").";
					}
					else if (ScoreID == Packet->Source->ScoreID)
						s += " added zone-wide.";
					else
					{
						s += " added zone-wide in zone #";
						s += ScoreID;
						s += ".";
					}

					s += " Duration: ";
					s += Days;
					s += " day(s).";

					player->SendArena(s.Text);

					return true;
				}

				if (MatchString(command, "?alias", 6))
				{
					_listnode <User> * parse;

					// Target player info
					LONG IP,
						 AIP,
						 MacID,
						 HwID;

					if (length <= 7)
					{
						IP		= player->player->IP;
						AIP		= player->player->IP & 0x00FFFFFF;
						MacID	= player->player->MacID;
						HwID	= (player->player->regform ? player->player->regform->HardwareID : 0);
					}
					else
					{
						parse = DATABASE.FindUser((BYTE*)&command[7]);

						if (parse)
						{
							IP		= parse->item->IP;
							AIP		= parse->item->IP & 0x00FFFFFF;
							MacID	= parse->item->MacID;
							HwID	= (parse->item->regform ? parse->item->regform->HardwareID : 0);
						}
					}

					if (parse)
					{
						if (IP == 0)
						{
							player->SendArena("This account has not been accessed yet");

							return true;
						}

						// Output
						int Total	= 0;
						bool Found	= false;
						char buffer[256];
						memset(buffer, 0, 256);

						parse = DATABASE.UserDatabase.head;

						while (parse)
						{
							User * user = parse->item;

							if (!buffer[0])
								strcat(buffer, "Aliases: ");

							if (user->IP != 0)		// IP == 0 disables alias checking
							{
								if (user->IP == IP)
								{
									if (Total) strcat(buffer, ", ");
									strcat(buffer, (char*)&user->Name[0]);
									strcat(buffer, "[IP+]");
									Total++;
								}
								else if (user->MacID == MacID)
								{
									if (Total) strcat(buffer, ", ");
									strcat(buffer, (char*)&user->Name[0]);
									strcat(buffer, "[MAC]");
									Total++;
								}
								else if (user->regform && (user->regform->HardwareID == HwID))
								{
									if (Total) strcat(buffer, ", ");
									strcat(buffer, (char*)&user->Name[0]);
									strcat(buffer, "[ID2]");
									Total++;
								}
								else if ((user->IP & 0x00FFFFFF) == AIP)
								{
									if (Total) strcat(buffer, ", ");
									strcat(buffer, (char*)&user->Name[0]);
									strcat(buffer, "[~IP]");
									Total++;
								}
							}

							if (Total == 5)
							{
								player->SendArena(buffer);
								Total = 0;
								memset(buffer, 0, 256);
								Found = true;
							}

							parse = parse->next;
						}

						if (Total)
						{
							player->SendArena(buffer);
						}
						else if (Found == false)
						{
							player->SendArena("No aliases");
						}
					}
					else
					{
						player->SendArena("Unable to find specified player");
					}

					return true;
				}

				if (MatchString(command, "?listop", 7))
				{	// List all operators

					if (player->OpStatus->ScoreID != 0) //Zone-wide op?
					{
						if (player->OpStatus->ScoreID != Packet->Source->ScoreID)
						//No biller access in this zone?
						{
							player->SendArena("You only have limited powers in this zone");
							return true;
						}
					}
					else //Not a zone-wide op
					{
						if (player->OpStatus->GroupID != 0)
						//Group-wide
						{
							if (player->OpStatus->GroupID != Packet->Source->GroupID)
							{
								player->SendArena("You only have limited powers in this zone");
								return true;
							}
						}
					}

					_listnode <Operator> * parse = DATABASE.OpDatabase.head;

					bool Found = false;
					_string s;

					while (parse)
					{
						Operator * op = parse->item;
						bool Allow = false;

						// Check if next op is in the same sector of the biller
						if (player->OpStatus->GroupID == 0)
							Allow = true;
						else if (Packet->Source->GroupID == op->GroupID)
						{
							if (player->OpStatus->ScoreID == 0)
								Allow = true;
							else if (player->OpStatus->ScoreID == op->ScoreID)
								Allow = true;
						}
						

						// Check if op has a restricted viewing level
						/*switch (player->OpStatus->Level)
						{
							case 2:	if (op->Level <= 0)	Allow = false;
								break;
							case 3:	if (op->Level <= 1)	Allow = false;
								break;
							case 4:	if (op->Level <= 1)	Allow = false;
								break;
						};*/

						// Send line if access is allowed.
						if (Allow)
						{
							Found = true;

							s = op->OpID;
							if (op->GroupID == 0)		// Listed operator is network-wide
								s += "*";
							else if (op->ScoreID == 0)	// Listed operator is group-wide
							{
								s += "$";
							}
							s += ") ";

							if (player->OpStatus->GroupID == 0)
							{	// Needs zone/group id
								if (op->GroupID == 0)
								{	// Has nothing
								}
								else if (op->ScoreID == 0)
								{	// Has group
									s += "{";
									s += op->GroupID;
									s += "} ";
								}
								else
								{	// Has zone/group
									s += "{";
									s += op->GroupID;
									s += ":";
									Host * host = Packet->Source->Socket->FindHost(op->ScoreID);
									if (host)
										s += (char*)&host->Name[0];
									else
										s += op->ScoreID;
									s += "} ";
								}
							}
							else if (player->OpStatus->ScoreID == 0)
							{	// Needs zone
								if (op->GroupID == 0)
								{	// Has nothing
								}
								else if (op->ScoreID == 0)
								{	// Has group
								}
								else
								{	// Has zone/group
									s += "{";
									s += op->GroupID;
									s += ":";
									Host * host = Packet->Source->Socket->FindHost(op->ScoreID);
									if (host)
										s += (char*)&host->Name[0];
									else
										s += op->ScoreID;
									s += "} ";
								}
							}

							if (op->Owner)
								s += (char*)&op->Owner->Name[0];
							else
								s += "~";

							s += ":";
							s += (int) op->Level;

							if (op->LoginCounter)
							{
								s += " -> ";
								s += op->LoginCounter;
								if (op->LoginCounter == 1)
									s += " session";
								else
									s += " sessions";
							}

							player->SendArena(s.Text);
						}

						parse = parse->next;
					}

					if (!Found)
						player->SendArena("No operators found");

					return true;
				}

				if (MatchString(command, "?adduser", 8))
				{	// ?adduser Name:Password
					if (length <= 9)
					{
						player->SendArena("This command requires parameters. Type ?man adduser for details");
						return true;
					}

					BYTE Name[32];
					BYTE Password[32];
					memset(Name, 0, 32);
					memset(Password, 0, 32);

					// Parse input
					SHORT	LeftLength;
					char  * RightText;

					if (ColonParse(&command[9], length - 9, LeftLength, RightText))
					{
						// Find user
						SpaceTrim(Name, &command[9], LeftLength);
						memcpy(Password, RightText, length - LeftLength - 10);

						if (!DATABASE.FindUser(Name))
						{
							if (InvalidUsername((char*)&Name[0]))
							{
								player->SendArena("Invalid username");
							}
							else
							{
								DATABASE.AddUser(Name, Password, 0, 0, 0);

								player->SendArena("User added");
							}
						}
						else
						{
							player->SendArena("User already exists");
						}
					}

					return true;
				}

				if (MatchString(command, "?kick", 5))
				{
					if (length < 7)
					{
						player->SendArena("This command requires parameters. Type ?man kick for details");
						return true;
					}
					
					int Delay = 0;
					char DelayString[8];
					char Name[32];
					BYTE NameLen = 0;
					int Index;

					memset(Name, 0, 32);

					int i = 6;
					while (i < length)
					{
						if (command[i] == '-')
						{
							i++;
							if (i > length)
							{
								player->SendArena("Invalid switch");
								return true;
							}
							switch (command[i])
							{
							case 'w':
								i++;
								if ((i + 1 > length) || (command[i] != '='))
								{
									player->SendArena("Invalid switch");
									return true;
								}
								i++;
								Index = 0;
								memset(DelayString, 0, 8);
								while (i < length)
								{
									if ((command[i] < 48) || (command[i] > 57))
									{
										Delay = atoi(DelayString);

										if (Delay <= 0)
											Delay = 1;
										if (Delay > 60)
											Delay = 60;
											break;
									}
									else
									{
										DelayString[Index++] = command[i];
										if (Index >= 8)
										{
											player->SendArena("Invalid switch");
											return true;
										}
									}
									i++;
								}
							}
						}
						else if (command[i] != ' ')
						{
							for (int n = i; n < length; n++)
							{
								Name[NameLen++] = command[n];

								// Name information
								if (NameLen >= 20)
								{
									player->SendArena("Name: Specified name ludicrously long(Not over 20 characters please)");

									return true;
								}
							}

							break;							
						}
						i++;
					}

					_listnode <Host> * hparse = (Packet->Source->Socket)->HostList.head;
					_listnode <LUserNode> * parse;
					while (hparse)
					{
						parse = hparse->item->Find(Name, true);
						if (parse) break;
						hparse = hparse->next;
					}

					if (!parse)
					{
						player->SendArena("Player not found");
						player->SendArena(Name);
						return true;
					}

					bool Allow = false;

					if (player->OpStatus->ScoreID == 0) //Group-wide ro net-wide
					{
						if (player->OpStatus->GroupID == 0) //Net-wide
						{
							Allow = true;
						}
						else //Group-wide
						{
							if (player->OpStatus->GroupID == parse->item->zone->GroupID) Allow = true;
						}
					}
					else //Zone-wide
					{
						if (player->OpStatus->ScoreID == parse->item->zone->ScoreID) Allow = true;
					}
					
					if (parse->item->OpStatus)
					{
						if (player->OpStatus->Level >= parse->item->OpStatus->Level) Allow = false;
					}
					
					if (!Allow)
					{
						player->SendArena("You do not have the necessary authority to kick the player");
						_string s;
						s = "NOTICE: ";
						s += (char*)&player->player->Name;
						s += " tried to ?kick you!";
						parse->item->SendArena(s.Text);
						return true;
					}

					_string s;
					s = "Kicking player";
					if (Delay) 
					{
						s += " in ";
						s += Delay;
						s += " seconds";
					}
					player->SendArena(s.Text);
					parse->item->Kick(Delay * 1000, -1, -1);
					
					return true;
				}
			}

			if ((DATABASE.DisableHelp == false) && MatchString(command, "?man", 4))
			{
				if (length <= 6)
				{
					player->SendArena("Chat commands: chat, notifychat, c-grant, c-welcome, c-plist, c-allow, c-revoke, c-release, c-own, c-list, c-kick, c-needp");
					if (player->player->squad && (player->player->squad->Owner == player->player))
						player->SendArena("Squad owner commands: squaddissolve, squadpassword, squadkick, squadgrant, squadname");
					player->SendArena("Squad commands: squadcreate, squadjoin, squadlist, squadleave, squadowner, squadonline, squadownlist, squad");
					player->SendArena("Messaging commands: msgclear, msglist, msgview, msgpost, msgdel");
					player->SendArena("Other commands: login, version, top10, whois, coin, password, details, setemail, setname, ghost, seen, find");

					if (player->OpStatus)
					{
						switch (player->OpStatus->Level)
						{
						case 0:
							player->SendArena("L0: networkreset, netbantext, write, read, block, changepassword, changename, changesquad");
						case 1:
							player->SendArena("L1: resetscore, addzone, listzone, removezone, recyclezone, changezone, closezone");
						case 2:
							player->SendArena("L2: bantext, broadcast, **message, extchat, notifyzone, groupid, scoreid");
						case 3:
							player->SendArena("L3: addop, removeop, info");
						case 4:
							player->SendArena("L4: warn, kick, changeop, listop, adduser, alias, ban, listban, liftban, commentban, notifyban, logout, banfree, listbanfree, liftbanfree");
						};
					}
					player->SendArena("(Press ESC to view the entire page. ?man <command> returns detailed info)");
							
					if (player->OpStatus)
					{
						_string s;
						s += "You are ";
						if (player->OpStatus->ScoreID != 0 && player->OpStatus->GroupID != 0)
						{
							s += "zone-wide";
							if (player->OpStatus->ScoreID == Packet->Source->ScoreID)
							{
								s += "(In this zone)";
							}
							else
							{
								s += "(Not in this zone)";
							}
						}
						if (player->OpStatus->GroupID != 0 && player->OpStatus->ScoreID == 0)
						{
							s += "group-wide(Group ";
							s += player->OpStatus->GroupID;
							s += ")";
							if (player->OpStatus->GroupID == Packet->Source->GroupID)
							{
								s += "(This zone in group)";
							}
							else
							{
								s += "(This zone not in group)";
							}
						}
						if (player->OpStatus->ScoreID == 0 && player->OpStatus->GroupID == 0)
						{
							s += "network-wide";
						}
						s += " operator level ";
						switch (player->OpStatus->Level)
						{
						case 0:		s += "0 - God";			// Network Administrator";
							break;
						case 1:		s += "1 - Archangel";	// Group Manager";
							break;
						case 2:		s += "2 - Overlord";	// SysOp";
							break;
						case 3:		s += "3 - Keeper";		// Super Moderator";
							break;
						case 4:		s += "4 - Slave";		// Moderator";
							break;
						};
						player->SendArena(s.Text);
					}

					if (!player->player->regform)
					{
						player->SendArena("***************************************************************************");
						player->SendArena("WARNING: You have not sent registration form data. Some commands");
						player->SendArena("         will not function and some settings will not be saved.");
						player->SendArena("To send registration form data, enter any zone on the network with Subspace");
					}

				}
				else
				{
					command = &command[4];

					while ((command[0] != 0) && ((command[0] == ' ') || (command[0] == '?')))
					{
						command = &command[1];
					}

					if (MatchString(command, "password", 8))
					{
						player->SendArena("  The password command allows you to change the password");
						player->SendArena("  you type when you enter the game.  Pick it wisely, as");
						player->SendArena("  it is possible to guess poorly-chosen passwords.  The");
						player->SendArena("  best passwords mix upper/lower-case letters, numbers");
						player->SendArena("  and symbols.  Limit 32 characters.");
						player->SendArena("SYNTAX: ?password=<new password>");
						player->SendArena("EXAMPLE: ?password=mamahadacow");
						player->SendArena("EXAMPLE: ?passsword=idontcare");
					}
					else if (MatchString(command, "c-grant", 7))
					{
						player->SendArena("  Allows you to transfer chat ownership to another player");
						player->SendArena("  without having to re-add all allowed users. Everyone will be");
						player->SendArena("  kicked from the chat when owners are changed.");
						player->SendArena("SYNTAX: ?c-grant <chat>:<new owner>");
						player->SendArena("EXAMPLE: ?c-grant thechatofgods:Mr. God");
						player->SendArena("EXAMPLE: ?c-grant The Warriors Chat:SomeWeirdGuy");
					}
					else if (MatchString(command, "c-list", 6))
					{
						player->SendArena("  Displays a list of chat channels that you own.");
						player->SendArena("SYNTAX: ?c-list");
						player->SendArena("EXAMPLE: ?c-list");
					}
					else if (MatchString(command, "c-needp", 7))
					{
						player->SendArena("  Sets/views if players need premission to enter the chat channel.");
						player->SendArena("  Only the channel owner can set it, but everyone can view it.");
						player->SendArena("SYNTAX: ?c-needp <channel>[:<yes/no>]");
						player->SendArena("EXAMPLE: ?c-needp thegreatoldchat:yes");
						player->SendArena("EXAMPLE: ?c-needp someweirdchat");
					}
					else if (MatchString(command, "c-welcome", 9))
					{
						player->SendArena("  Sets or views chat welcome message. Only the owner of a");
						player->SendArena("  chat can view/set the welcome message. If text is not specified");
						player->SendArena("  then shows welcome message.");
						player->SendArena("SYNTAX: ?c-welcome <channel>[:<text>]");
						player->SendArena("EXAMPLE: ?c-welcome squadchatofus:Welcome to our squad chat");
						player->SendArena("EXAMPLE: ?c-welcome thisisachat");
					}
					else if (MatchString(command, "c-own", 5))
					{
						player->SendArena("  Grabs a chat channel under your name. Does not join it.");
						player->SendArena("  You can not own more than 9 chats.");
						player->SendArena("SYNTAX: ?c-own <chat name>");
						player->SendArena("EXAMPLE: ?c-own kingschat");
						player->SendArena("EXAMPLE: ?c-own rebies");
					}
					else if (MatchString(command, "squadownlist", 12))
					{
						player->SendArena("  Shows what squads you own.");
						player->SendArena("SYNTAX: ?squadownlist");
						player->SendArena("EXAMPLE: ?squadownlist");
					}
					else if (MatchString(command, "squadonline", 11))
					{
						player->SendArena("  Shows who in a squad is online. If no squad specified shows own squad.");
						player->SendArena("SYNTAX: ?squadonline [squad]");
						player->SendArena("EXAMPLE: ?squadonline");
						player->SendArena("EXAMPLE: ?squadonline Striker Knights");
					}
					else if (MatchString(command, "c-allow", 7))
					{
						player->SendArena("  Gives a player access to a chat channel that you own.");
						player->SendArena("SYNTAX: ?c-allow <channel>:<player>");
						player->SendArena("EXAMPLE: ?c-allow chatchatchat:SomeGuy");
						player->SendArena("EXAMPLE: ?c-allow staff:Mr. Moderator");
					}
					else if (MatchString(command, "c-release", 9))
					{
						player->SendArena("  Releases a chat channel from your ownership.");
						player->SendArena("  Level 2 operators can release channels that other players");
						player->SendArena("  own, unless the chat owner is a higher or equal level operator.");
						player->SendArena("SYNTAX: ?c-release <channel>");
						player->SendArena("EXAMPLE: ?c-release boom");
						player->SendArena("EXAMPLE: ?c-release staffchat");
					}
					else if (MatchString(command, "warn", 4))
					{
						player->SendArena("  Sends the specified player a warning message that has your nametag.");
						player->SendArena("SYNTAX: ?warn <player>:<text>");
						player->SendArena("EXAMPLE: ?warn Mr. Morden:No spamming!!!");
						player->SendArena("EXAMPLE: ?warn OldMan:It is illegal to get old in this zone. Leave. Now.");
					}
					else if (MatchString(command, "c-revoke", 8))
					{
						player->SendArena("  Revokes a players right to use a chat channel that you own.");
						player->SendArena("SYNTAX: ?c-revoke <channel>:<player>");
						player->SendArena("EXAMPLE: ?c-revoke bluha:someplayer");
						player->SendArena("EXAMPLE: ?c-revoke asdf:That Guy");
					}
					else if (MatchString(command, "c-plist", 7))
					{
						player->SendArena("  Displays allowed players on a chat channel.");
						player->SendArena("SYNTAX: ?c-plist <channel>");
						player->SendArena("EXAMPLE: ?c-plist progagaga");
						player->SendArena("EXAMPLE: ?c-plist znichel");
					}
					else if (MatchString(command, "c-kick", 6))
					{
						player->SendArena("  Kicks a player for a chat that you own. Level 2 or higher");
						player->SendArena("  biller operators can kick poeple from other peoples chats or unowned chats.");
						player->SendArena("SYNTAX: ?c-kick <channel>:<player>");
						player->SendArena("EXAMPLE: ?c-kick staffchat:AbusingBiker");
						player->SendArena("EXAMPLE: ?c-kick sc:Jericho24.");
					}
					else if (MatchString(command, "msglist", 7))
					{
						player->SendArena("  Shows your messages. You can delete them with ?msgdel and");
						player->SendArena("  read them with ?msgview. If you have 10 messages, then you");
						player->SendArena("  cannot receive any more messages");
						player->SendArena("SYNTAX: ?msglist");
						player->SendArena("EXAMPLE: ?msglist");
					}
					else if (MatchString(command, "top10", 5))
					{
						player->SendArena("  Shows top 10 players in the zone, listed by specified type.");
						player->SendArena("  Types: average/losses/wins/score/ratio");
						player->SendArena("  Default type is score.");
						player->SendArena("SYNTAX: ?top10 [type]");
						player->SendArena("EXAMPLE: ?top10 ratio");
						player->SendArena("EXAMPLE: ?top10 wins");
					}
					else if (MatchString(command, "netbantext", 10))
					{
						player->SendArena("  Changes or views ban text shown to netbanned players.");
						player->SendArena("SYNTAX: ?netbantext[=text]");
						player->SendArena("EXAMPLE: ?netbantext");
						player->SendArena("EXAMPLE: ?netbantext=You have been b4nz0r3d! Mail me at me@you.com to say youre sorry.");
					}
					else if (MatchString(command, "msgdel", 6))
					{
						player->SendArena("  Deletes a message.");
						player->SendArena("SYNTAX: ?msgdel <message number>");
						player->SendArena("EXAMPLE: ?msgdel 1");
						player->SendArena("EXAMPLE: ?msgdel 66235");
					}
					else if (MatchString(command, "msgpost", 7))
					{
						player->SendArena("  Posts a message to a player.");
						player->SendArena("SYNTAX: ?msgpost <player>:<message>");
						player->SendArena("EXAMPLE: ?msgpost hacket:Hello, sup?");
						player->SendArena("EXAMPLE: ?msgpost Krazi Tom:KILL KILLL KILLLLL BURN BURN BURN");
					}
					else if (MatchString(command, "msgview", 7))
					{
						player->SendArena("  Displays specified message.");
						player->SendArena("SYNTAX: ?msgview <message number>");
						player->SendArena("EXAMPLE: ?msgview 55");
						player->SendArena("EXAMPLE: ?msgview 3");
					}
					else if (MatchString(command, "version", 7))
					{
						player->SendArena("  Displays biller version");
						player->SendArena("SYNTAX: ?version");
						player->SendArena("EXAMPLE: ?version");
					}
					else if (MatchString(command, "coin", 4))
					{
						player->SendArena("  Flip a coin");
						player->SendArena("SYNTAX: ?coin");
						player->SendArena("EXAMPLE: ?coin");
					}
					else if (MatchString(command, "msgclear", 8))
					{
						player->SendArena("  Deletes all of your messages.");
						player->SendArena("SYNTAX: ?msgclear");
						player->SendArena("EXAMPLE: ?msgclear");
					}
					else if (MatchString(command, "login", 6))
					{
						player->SendArena("  Activate your operator powers. You can change the");
						player->SendArena("  password with ?changeop. Contact a higher level operator");
						player->SendArena("  if you do not remember your password.");
						player->SendArena("SYNTAX: ?login <password>");
						player->SendArena("EXAMPLE: ?login MySecretPassword");
						player->SendArena("EXAMPLE: ?login GreatMaker!!");
					}
					else if (MatchString(command, "kick", 4))
					{
						player->SendArena("  Kicks a player. Works network-wide as long as you have");
						player->SendArena("  powers in the zone that the player is in.");
						player->SendArena("  -w=seconds - Delay until kick(0-60 seconds)");
						player->SendArena("SYNTAX: ?kick [-w=seconds] <name>");
						player->SendArena("EXAMPLE: ?kick StupidBot");
						player->SendArena("EXAMPLE: ?kick -w=20 Jericho24.");
					}
					else if (MatchString(command, "logout", 7))
					{
						player->SendArena("  Deactivates your operator powers.");
						player->SendArena("SYNTAX: ?logout");
						player->SendArena("EXAMPLE: ?logout");
					}
					else if (MatchString(command, "chat", 4))
					{
						player->SendArena("  Typing \"?chat\" by itself will list all channels you are");
						player->SendArena("  listening to.");
						player->SendArena("SYNTAX: ?chat");
						player->SendArena("  The chat assignment command provides for reconfiguring");
						player->SendArena("  the contents and order of your chat channels.");
						player->SendArena("SYNTAX: ?chat=[chan>,<chan>,<chan>,<chan>]");
						player->SendArena("EXAMPLE: ?chat=");
						player->SendArena("EXAMPLE: ?chat=staffchat,squadchat,whateverchat,orthechat");
					}
					else if (MatchString(command, "extchat", 7))
					{
						player->SendArena("  Toggle ability to receive **messages and chat channel");
						player->SendArena("  messages from other zones.");
						player->SendArena("SYNTAX: ?extchat [yes|no]");
						player->SendArena("EXAMPLE: ?extchat");
						player->SendArena("EXAMPLE: ?extchat yes");
					}
					else if (MatchString(command, "notifychat", 7))
					{
						player->SendArena("  Using the notifychat feature enables or disables messages");
						player->SendArena("  that report when players join and exit your chat channels");
						player->SendArena("SYNTAX: ?notifychat [yes|no]");
						player->SendArena("EXAMPLE: ?notifychat");
						player->SendArena("EXAMPLE: ?notifychat no");
					}
					else if (MatchString(command, "notifyban", 7))
					{
						player->SendArena("  Using the notifyban feature enables or disables messages");
						player->SendArena("  that report when banned players join and exit your network");
						player->SendArena("SYNTAX: ?notifyban [yes|no]");
						player->SendArena("EXAMPLE: ?notifyban yes");
						player->SendArena("EXAMPLE: ?notifyban");
					}
					else if (MatchString(command, "notifyzone", 7))
					{
						player->SendArena("  Using the notifyzone feature enables or disables messages");
						player->SendArena("  that report when zones join and exit the network");
						player->SendArena("SYNTAX: ?notifyzone [yes|no]");
						player->SendArena("EXAMPLE: ?notifyzone");
						player->SendArena("EXAMPLE: ?notifyzone yes");
					}
					else if (MatchString(command, "details", 7))
					{
						player->SendArena("  Typing \"?details\" alone, displays your personal details.");
						player->SendArena("SYNTAX: ?details");
						player->SendArena("  To change your details, use the details assignment command.");
						player->SendArena("  Max comment length is 127 characters.");
						player->SendArena("SYNTAX: ?details=<personal commentary>");
						player->SendArena("  To view others' details, omit the '='.");
						player->SendArena("SYNTAX: ?details <player name>");
						player->SendArena("EXAMPLE: ?details=Im a sexy 23 year old blonde woman who likes subspace");
						player->SendArena("EXAMPLE: ?details SexyGirl");
					}
					else if (MatchString(command, "setname", 7))
					{
						player->SendArena("  Use setname to change the name you entered in the registration form.");
						player->SendArena("SYNTAX: ?setname=<real name>");
						player->SendArena("EXAMPLE: ?setname=Priit Kasesalu");
						player->SendArena("EXAMPLE: ?setname=I dont know my name");
					}
					else if (MatchString(command, "setemail", 8))
					{
						player->SendArena("  Use setemail to change the address you entered in the");
						player->SendArena("  registration form.");
						player->SendArena("SYNTAX: ?setemail=<new email address>");
						player->SendArena("EXAMPLE: ?setemail=sander.saares@mail.ee");
						player->SendArena("EXAMPLE: ?setemail=PopGoes@TheBomb.org");
					}
					else if (MatchString(command, "squadjoin", 9))
					{
						player->SendArena("  To join a squad or change your squad, use squadjoin.");
						player->SendArena("SYNTAX: ?squadjoin=<squad name>:<squad password>");
						player->SendArena("  If you are a squad owner, the password is optional.");
						player->SendArena("SYNTAX: ?squadjoin=<squad name>");
						player->SendArena("EXAMPLE: ?squadjoin=MyVeryOwnSquad");
						player->SendArena("EXAMPLE: ?squadjoin=KewlSquad:pepper");
					}
					else if (MatchString(command, "networkreset", 12))
					{
						player->SendArena("  Will reset all scores in all zones on the biller.");
						player->SendArena("  You require network-wide access to use this command.");
						player->SendArena("SYNTAX: ?networkreset");
						player->SendArena("EXAMPLE: ?networkreset");
					}
					else if (MatchString(command, "resetscore", 10))
					{
						player->SendArena("  Resets scores of the zone with the selected ScoreID.");
						player->SendArena("  The specified zone will be recycled if online.");
						player->SendArena("SYNTAX: ?resetscore <ScoreID>");
						player->SendArena("EXAMPLE: ?resetscore 522");
					}
					else if (MatchString(command, "seen", 4))
					{
						player->SendArena("  Displays the last time at which the player was seen online.");
						player->SendArena("SYNTAX: ?seen <player name>");
						player->SendArena("EXAMPLE: ?seen sos");
						player->SendArena("EXAMPLE: ?seen ninja");
					}
					else if (MatchString(command, "squadleave", 10))
					{
						player->SendArena("  When you want to leave a squad, use squadleave.  This");
						player->SendArena("  will NOT remove your status as squad leader, nor will it");
						player->SendArena("  delete the squad.");
						player->SendArena("SYNTAX: ?squadleave");
						player->SendArena("EXAMPLE: ?squadleave");
					}
					else if (MatchString(command, "squadcreate", 11))
					{
						player->SendArena("  Createsquad will create a squad and make you the owner.");
						player->SendArena("SYNTAX: ?squadcreate=<squad name>:<squad password>");
						player->SendArena("EXAMPLE: ?squadcreate=MyVeryOwnSquad:TheCashFlows Nicely");
						player->SendArena("EXAMPLE: ?squadcreate=Blah:Blah");
					}
					else if (MatchString(command, "squadowner", 10))
					{
						player->SendArena("  If you are unsure of who is the current owner of a squad,");
						player->SendArena("  use the squadowner command.  This works for people on or");
						player->SendArena("  off the squad.");
						player->SendArena("SYNTAX: ?squadowner <squad name>");
						player->SendArena("EXAMPLE: ?squadowner MyVeryOwnSquad");
						player->SendArena("EXAMPLE: ?squadowner Striker Knights");
					}
					else if (MatchString(command, "find", 4))
					{
						player->SendArena("  The find command locates a player on the network.  It will");
						player->SendArena("  display the zone or arena he/she is in.");
						player->SendArena("SYNTAX: ?find <part of the player's name>");
						player->SendArena("  Typing \"?find=:name\" searches for an exact name match.  Use");
						player->SendArena("  if someone with a similar name to the searchee interferes.");
						player->SendArena("SYNTAX: ?find :<exact player name>");
						player->SendArena("EXAMPLE: ?find=a");
						player->SendArena("EXAMPLE: ?find=:anderson");
					}
					else if (MatchString(command, "squaddissolve", 13))
					{
						player->SendArena("  Squaddissolve completely removes a squad from the system.");
						player->SendArena("  Players will be able to re-claim the squad afterwards.");
						player->SendArena("SYNTAX: ?squaddissolve");
						player->SendArena("EXAMPLE: ?squaddissolve");
					}
					else if (MatchString(command, "squadpassword", 13))
					{
						player->SendArena("  Using the squadpassword command changes your squad's password,");
						player->SendArena("  but existing squad members will not need to re-join.");
						player->SendArena("SYNTAX: ?squadpassword=<new password>");
						player->SendArena("EXAMPLE: ?squadpassword=Riznakalaaff");
						player->SendArena("EXAMPLE: ?squadpassword=BowRe");
					}
					else if (MatchString(command, "squadkick", 9))
					{
						player->SendArena("  Whenever you would like to ban a player from your squad, use");
						player->SendArena("  squadkick in combination with changing squad password to lock them out.");
						player->SendArena("SYNTAX: ?squadkick <player name>");
						player->SendArena("EXAMPLE: ?squadkick Rubic");
						player->SendArena("EXAMPLE: ?suqadkick KillerSpawner");
					}
					else if (MatchString(command, "squadgrant", 10))
					{
						player->SendArena("  Ever want to transfer zone ownership to another player?");
						player->SendArena("  Utilize squadgrant to accomplish this.");
						player->SendArena("SYNTAX: ?squadgrant <new owner's name>");
						player->SendArena("EXAMPLE: ?squadgrant The Guy With Baggy Eyes");
						player->SendArena("EXAMPLE: ?squadgrant MyWife");
					}
					else if (MatchString(command, "squadlist", 9))
					{
						player->SendArena("  List the members of your squad.");
						player->SendArena("SYNTAX: ?squadlist");
						player->SendArena("EXAMPLE: ?squadlist");
					}
					else if (MatchString(command, "squadname", 9))
					{
						player->SendArena("  Squadname allows you to rename your squad.  Members will NOT");
						player->SendArena("  be required to re-join the squad, and the squad owner will NOT");
						player->SendArena("  be deposed.");
						player->SendArena("SYNTAX: ?squadname=<new squad name>");
						player->SendArena("EXAMPLE: ?squadname=Mental Midgets");
						player->SendArena("EXAMPLE: ?squadname=Bank of englandz");
					}
					else if (MatchString(command, "write", 5))
					{
						player->SendArena("  Activate the database save function.  Use sparingly.");
						player->SendArena("SYNTAX: ?write");
						player->SendArena("EXAMPLE: ?write");
					}
					else if (MatchString(command, "read", 4))
					{
						player->SendArena("  Re-read SUBBILL.INI, to update biller settings without");
						player->SendArena("  a network-wide recycle.");
						player->SendArena("SYNTAX: ?read");
						player->SendArena("EXAMPLE: ?read");
					}
					else if (MatchString(command, "alias", 5))
					{
						player->SendArena("  Find the other names a player has used.  Match types(in order of testing):");
						player->SendArena("  [IP]  = Exact IP match; no doubt about it.");
						player->SendArena("  [MAC] = MachineID match; this CAN be spoofed but is unlikely.");
						player->SendArena("  [ID2] = HardwareID match; experimental, thus untrustworthy.");
						player->SendArena("  [~IP] = Fuzzy IP match; there is a distinct possibility.");
						player->SendArena("SYNTAX: ?alias <player name>");
						player->SendArena("EXAMPLE: ?alias sos");
						player->SendArena("EXAMPLE: ?alias some weird player");
					}
					else if (MatchString(command, "adduser", 7))
					{
						player->SendArena("  Create a new user account with the given name and password.");
						player->SendArena("SYNTAX: ?adduser=<player name>:<player password>");
						player->SendArena("EXAMPLE: ?adduser=SomeGuy:SomePasword");
						player->SendArena("EXAMPLE: ?adduser=Whaaaa!:Rgaahahahahahah");
					}
					else if (MatchString(command, "changesquad", 11))
					{
						player->SendArena("  Change the given squad's owner.");
						player->SendArena("SYNTAX: ?changesquad=<squad name>:<new owner>");
						player->SendArena("EXAMPLE: ?changesquad=Striker Knights:NewMan");
						player->SendArena("EXAMPLE: ?changasquad=KookingMan:Mawahaha");
					}
					else if (MatchString(command, "changepassword", 14))
					{
						player->SendArena("  Change the given player's password.");
						player->SendArena("SYNTAX: ?changepassword=<player name>:<new password>");
						player->SendArena("EXAMPLE: ?changepassword=SOS:newpassword");
						player->SendArena("EXAMPLE: ?changepassword=Ablodendrik:Big Cube");
					}
					else if (MatchString(command, "changename", 10))
					{
						player->SendArena("  Change the player's name.  Score information, ban status, squad");
						player->SendArena("  ownership etc are all transferred to the new name.");
						player->SendArena("SYNTAX: ?changename=<old name>:<new name>");
						player->SendArena("EXAMPLE: ?changename=Mr. Morden:Mr. Lennier");
						player->SendArena("EXAMPLE: ?chatname=N00b:ExPert!");
					}
					else if (MatchString(command, "**", 2))
					{
						player->SendArena("  Mods use *arena. SMods use *zone. SysOps use **.");
						player->SendArena("  This command enables any zone sysop to send auto-tagged messages");
						player->SendArena("  network-wide.  The tag is not present when you are logged in");
						player->SendArena("  as billing operator level 1 or above.");
						player->SendArena("SYNTAX: **<message>");
						player->SendArena("EXAMPLE: **There is a prty in DeathHouse!! Get over there");
						player->SendArena("EXAMPLE: **WARNING: Incoming lag               (jk)");
					}
					else if (MatchString(command, "broadcast", 9))
					{
						player->SendArena("  Alternative for the **message for biller operators who are not zone sysops.");
						player->SendArena("  Level 2 and above operators can send network-wide auto-tagged messages with this command.");
						player->SendArena("  The tag is not present when you are logged in as billing operator level 1 or above.");
						player->SendArena("SYNTAX: ?broadcast <message>");
						player->SendArena("EXAMPLE: ?broadcast We got the hold in the olympics!!!!!!!!!!!!!!!!!!!!!!!!");
						player->SendArena("EXAMPLE: ?broadcast Whos WE?????!?!?!??");
					}
					else if (MatchString(command, "liftbanfree", 11))
					{
						player->SendArena("  To remove a banfree, use listbanfree to retrieve the banfree number");
						player->SendArena("  and use that number with liftbanfree.");
						player->SendArena("SYNTAX: ?liftbanfree <banfree number>");
						player->SendArena("EXAMPLE: ?liftbanfree 5");
						player->SendArena("EXAMPLE: ?liftbanfree 62357478");
					}
					else if (MatchString(command, "listbanfree", 11))
					{
						player->SendArena("  List players who cannot be banned in the current zone.");
						player->SendArena("  -*         - Show banfrees network-wide");
						player->SendArena("  *)         - Network-wide banfree");
						//player->SendArena("  $) {Group} - Group-wide banfree");
						player->SendArena("  )          - Zone-wide banfree");
						player->SendArena("SYNTAX: ?listbanfree [-*]");
						player->SendArena("EXAMPLE: ?listbanfree");
						player->SendArena("EXAMPLE: ?listbanfree -*");
					}
					else if (MatchString(command, "banfree", 7))
					{
						player->SendArena("  Players who are banfreed cannot be banned in the current zone.");
						player->SendArena("  -level - Specifys banfree level.");
						player->SendArena("  -*     - Network-wide banfree");
						player->SendArena("SYNTAX: ?banfree [-(0-4)] [-*] <player name>");
						player->SendArena("EXAMPLE: ?banfree -3 LubricatedMan");
						player->SendArena("EXAMPLE: ?banfree -0 -* ImportantGuy");
					}
					else if (MatchString(command, "bantext", 7))
					{
						player->SendArena("  This command does NOT change player ban comments - it changes the");
						player->SendArena("  text shown when banned players enter the game in this zone.");
						player->SendArena("SYNTAX: ?bantext <new text>");
						player->SendArena("EXAMPLE: ?bantext You have been banned!!! BAM!");
					}
					else if (MatchString(command, "ban", 3))
					{
						player->SendArena("  The ban command restricts access to your network.  Switches come");

						if (player->OpStatus)
						{
							player->SendArena("  before the player name, which is always last.  Switches:");

							switch (player->OpStatus->Level)
							{
							case 0:
								player->SendArena("  -i 127.0.*.*; -i 1.1.1.1-2.2.2.2 - Change IP range attributes.");
							case 1:
								player->SendArena("  -*           - Set a network-wide.");
								player->SendArena("  -r=<ScoreID> - Remotely ban in zone with specified ScoreID");
							case 2:
								player->SendArena("  -!           - Block all traffic from his/her ISP.");
							case 3:
								player->SendArena("  -s           - Auto-ban IP subnet.  Default blocks only current IP.");
								player->SendArena("  -lvl         - Change access level needed to modify the ban. (0-4)");
							case 4:
								player->SendArena("  -k           - Do not kick the player from the zone.");
								player->SendArena("  -d=<days>    - Set ban timer.");
								player->SendArena("  -b           - Deny banfrees for this player.");
								player->SendArena("  -g[=GroupID] - Set group-wide ban.");
							};
						}
						else
						{
							player->SendArena("  before the player name, which is always last.");
						}

						player->SendArena("SYNTAX: ?ban [-b] [-i IP1-IP2] [-s] [-d=days] [-k] [-*] [-!] [-level] [-g=group] <player name>");
						player->SendArena("EXAMPLE: ?ban -s -* -d=5000 -0 AGuyThatDidMuchEvil");
						player->SendArena("EXAMPLE: ?ban -d=3 -k LittleBoy");
					}
					else if (MatchString(command, "listban", 7))
					{
						player->SendArena("  List all bans within your sector of the network.  Bans may not be zone-wide:");
						player->SendArena("  If you specify a BanID, then it will show info for that ban only");
						player->SendArena("  #) <Zone Name/Zone ScoreID> Name ... - Zone-wide ban.");
						player->SendArena("  #$) {GroupID} Name ...               - Group-wide ban.");
						player->SendArena("  #*) Name ...                         - Network-wide ban.");
						player->SendArena("  -d - Show details");
						player->SendArena("  -* - Show bans netowrk-wide");
						player->SendArena("SYNTAX: ?listban [-d] [-*] [BanID]");
						player->SendArena("EXAMPLE: ?listban 55");
						player->SendArena("EXAMPLE: ?listban");
					}
					else if (MatchString(command, "commentban", 10))
					{
						player->SendArena("  To add a ban comment, use listban to retrieve the ban number");
						player->SendArena("  and use that number with commentban. Max comment length is 127 characters.");
						player->SendArena("SYNTAX: ?commentban <ban number>:<comment>");
						player->SendArena("EXAMPLE: ?commentban 4:Used antiwarp in center area. Banned for a day");
						player->SendArena("EXAMPLE: ?commentban 642:Crashed server. 5000 day ban");
					}
					else if (MatchString(command, "liftban", 7))
					{
						player->SendArena("  To remove a ban, use listban to retrieve the ban number");
						player->SendArena("  and use that number with liftban.");
						player->SendArena("SYNTAX: ?liftban <ban number>");
						player->SendArena("EXAMPLE: ?liftban 6");
						player->SendArena("EXAMPLE: ?liftbasn 8898");
					}
					else if (MatchString(command, "listzone", 8))
					{
						player->SendArena("  List all logged hosts, and display registration and connection status.");
						player->SendArena("  Online(Unregistered) - The zone has connected with the 'skeleton password'.");
						player->SendArena("                         It is advisable to use the addzone system instead.");
						player->SendArena("  Offline              - Registered with a password but not connected.");
						player->SendArena("  Online               - Registered with a password and connected.");
						player->SendArena("  Connecting: IP:Port  - Generally means that a connection has not been closed yet.");
						player->SendArena("                         Sometimes this means that a zone had an error and needs a manual restart.");
						player->SendArena("SYNTAX: ?listzone");
						player->SendArena("EXAMPLE: ?listzone");
					}
					else if (MatchString(command, "closezone", 9))
					{
						player->SendArena("  To shutdown a zone, use listzone to get the zone's score id,");
						player->SendArena("  and use the id with closezone.  The zone must be connected.");
						player->SendArena("SYNTAX: ?closezone <zone score id>");
						player->SendArena("EXAMPLE: ?closezone 5");
						player->SendArena("EXAMPLE: ?closezone 4746");
					}
					else if (MatchString(command, "recyclezone", 11))
					{
						player->SendArena("  To recycle a zone, use listzone to get the zone's score id,");
						player->SendArena("  and use the id with recyclezone.  The zone must be connected.");
						player->SendArena("SYNTAX: ?recyclezone=<zone score id>");
						player->SendArena("EXAMPLE: ?recyclezone=246246");
						player->SendArena("EXAMPLE: ?recyclezone=46");
					}
					else if (MatchString(command, "addzone", 7))
					{
						player->SendArena("  Addzone assigns private passwords to new zones entering the");
						player->SendArena("  network.  This prevents DoS attacks by disgruntled operators");
						player->SendArena("  on other zones.  The 'skeleton key' biller password is still");
						player->SendArena("  available, but it should be randomized soon after the first");
						player->SendArena("  zone connects, as the addzone system is more efficient.");
						player->SendArena("  If you are a network-wide operator then the zone will be added");
						player->SendArena("  under the group of the zone that you are in. If you are a");
						player->SendArena("  group-wide operator then the zone will be added under the");
						player->SendArena("  group of the zone you are op in.");
						player->SendArena("SYNTAX: ?addzone <score id>:<password>");
						player->SendArena("EXAMPLE: ?addzone 8000:asfd");
						player->SendArena("EXAMPLE: ?addzone 246:111111113653");
					}
					else if (MatchString(command, "removezone", 10))
					{
						player->SendArena("  Removezone will unregister a zone password, but will not");
						player->SendArena("  disconnect the related zone.  To block access, use closezone");
						player->SendArena("SYNTAX: ?removezone=<score id>");
						player->SendArena("EXAMPLE: ?removezone=52");
						player->SendArena("EXAMPLE: ?removezone=2536");
					}
					else if (MatchString(command, "changezone", 10))
					{
						player->SendArena("  Changezone changes a zone's password.  This password is secure.");
						player->SendArena("SYNTAX: ?changezone=<ScoreID>:<new password>");
						player->SendArena("EXAMPLE: ?changezone=1346:asgafg");
						player->SendArena("EXAMPLE: ?changezone=10000:6666624");
					}
					else if (MatchString(command, "squad", 5))
					{
						player->SendArena("  Shows what squad the player is in");
						player->SendArena("SYNTAX: ?squad <player name>");
						player->SendArena("EXAMPLE: ?squad sos");
						player->SendArena("EXAMPLE: ?squad BeefEater");
					}
					else if (MatchString(command, "ghost", 5))
					{
						player->SendArena("  The ghost command makes you invisible to the find and whois");
						player->SendArena("  commands.  Billing operators gain invisibility in chat channels.");
						player->SendArena("SYNTAX: ?ghost [yes|no]");
						player->SendArena("EXAMPLE: ?ghost yes");
						player->SendArena("EXAMPLE: ?ghost");
					}
					else if (MatchString(command, "whois", 6))
					{
						player->SendArena("  Retrieve player operator status. Will not work if player is ghosted.");
						player->SendArena("SYNTAX: ?whois <player name>");
						player->SendArena("EXAMPLE: ?whois sos");
						player->SendArena("EXAMPLE: ?whois he. man. woman.");
					}
					else if (MatchString(command, "info", 4))
					{
						player->SendArena("  Shows database entry contents for given user including ban info.");
						player->SendArena("  Replace name with #UserID to show info of user with specified UserID");
						player->SendArena("SYNTAX: ?info <Name | #UserID>");
						player->SendArena("EXAMPLE: ?info #123");
						player->SendArena("EXAMPLE: ?info sos");
					}
					else if (MatchString(command, "scoreid", 7))
					{
						player->SendArena("  Shows current zone's score id.");
						player->SendArena("SYNTAX: ?scoreid");
						player->SendArena("EXAMPLE: ?scoreid");
					}
					else if (MatchString(command, "groupid", 7))
					{
						player->SendArena("  Shows current zone's group id.");
						player->SendArena("SYNTAX: ?groupid");
						player->SendArena("EXAMPLE: ?groupid");
					}
					else if (MatchString(command, "group", 5))
					{
						player->SendArena("  The biller commands have varying potency depending on operator");
						player->SendArena("  level.  (Type ?man levels for info)  There is also an option");
						player->SendArena("  in the ?addop command to restrict power to a sector of the");
						player->SendArena("  network.  There are three types of restrictions:");
						player->SendArena("  Network-wide = Has power in all zones on the network");
						player->SendArena("  Group-wide   = Has power only in one GroupID [See SERVER.INI]");
						player->SendArena("  Zone-wide    = Has power only in one zone.  In listops, these");
						player->SendArena("                 operators have the name or ScoreID of the zone");
					}
					else if (MatchString(command, "level", 5))
					{
						player->SendArena("  SSBiller2 uses an access-system for operators based on zone");
						player->SendArena("  ScoreID and GroupID.  (Type ?man groups for info)");
						player->SendArena("  Operator levels are independant of these restrictions.");
						player->SendArena("  0 = God");
						player->SendArena("  1 = Archangel");
						player->SendArena("  2 = Overlord");
						player->SendArena("  3 = Keeper");
						player->SendArena("  4 = Slave");
					}
					else if (MatchString(command, "addop", 5))
					{
						player->SendArena("  ?login <password> can be set to allow private passwords using");
						player->SendArena("  the addop command.  Type ?man levels or ?man groups for more info.");
						player->SendArena("  The operator's zone is the current one.  Switches:");
						player->SendArena("  -*         = Network-wide");
						player->SendArena("  -g         = Group-wide within current group");
						player->SendArena("  -g=GroupID = Group-wide within given group");
						player->SendArena("  -level     = Operator level (0-4)");
						player->SendArena("SYNTAX: ?addop [-*] [-level] [-g] [-g=group] <name>:<password>");
						player->SendArena("EXAMPLE: ?addop -* -0 NetOp:ThePassword");
						player->SendArena("EXAMPLE: ?addop -4 MOderata:Kabasada");
					}
					else if (MatchString(command, "listop", 6))
					{
						player->SendArena("  List registered operators.  Type ?man level or ?man group for info.");
						player->SendArena("  *)             - Network-wide operator.");
						player->SendArena("  $) {Group}     - Group-wide operator.");
						player->SendArena("  ) {Group:Zone} - Zone-wide operator.");
						player->SendArena("SYNTAX: ?listop");
						player->SendArena("EXAMPLE: ?listop");
					}
					else if (MatchString(command, "changeop", 8))
					{
						player->SendArena("  The changeop command allows you to change an operator's password.");
						player->SendArena("  Type ?man levels or ?man groups for more info.");
						player->SendArena("SYNTAX: ?changeop <operator entry #>:<new password>");
						player->SendArena("  When you are a level 4 operator, however, you are only permitted to");
						player->SendArena(" change your own password, and so the operator entry # is omitted..");
						player->SendArena("SYNTAX: ?changeop <new password>");
						player->SendArena("EXAMPLE: ?changeop MyNewPasswordIsCookies");
						player->SendArena("EXAMPLE: ?changeop 624:Cookamaz");
					}
					else if (MatchString(command, "removeop", 8))
					{
						player->SendArena("  The removeop command deletes an operator's private ?login password.");
						player->SendArena("  Type ?man levels or ?man groups for more info.");
						player->SendArena("SYNTAX: ?removeop <operator entry #>");
						player->SendArena("EXAMPLE: ?removeop 52");
						player->SendArena("EXAMPLE: ?removeop 642");
					}
					else if (MatchString(command, "block", 5))
					{
						player->SendArena("  Toggles between the Block states:");
						player->SendArena("    Normal  - No special treatment.");
						player->SendArena("    Banfree - Pass bans and some other blocks.");
						player->SendArena("SYNTAX: ?block <player name>");
						player->SendArena("EXAMPLE: ?block SOS");
						player->SendArena("EXAMPLE: ?block sniil ydis");
					}
					else
					{
						player->SendArena("  Help in this catagory is unavailable.");
					}
				}

				return true;
			}

			/* Diagnostic commands */

			if (MatchString(command, "?find", 5))
			{
				if (length <= 6)
				{
					player->SendArena("This command requires parameters. Type ?man find for details");

					return true;
				}

				if (command[6] == ':')
				{
					_listnode <Host> * parse = (Packet->Source->Socket)->HostList.head;
					int Counter = 0;

					while (parse)
					{
						// Find user
						_listnode <LUserNode> * user = parse->item->Find(&command[7], true);

						if (user && !user->item->Ghost)
						{
							// Prepare response
							char buffer[512];

							strcpy(buffer, (char*)&user->item->player->Name[0]);
							strcat(buffer, " is in ");
							strcat(buffer, (char*)&user->item->zone->Name[0]);

							// Send it
							player->SendArena(buffer);
							Counter++;

							if (Counter > 5)
								return true;
						}

						// Try next zone
						parse = parse->next;
					}

					if (Counter == 0)
						player->SendArena("User offline");
				}
				else
				{
					_listnode <Host> * parse = (Packet->Source->Socket)->HostList.head;
					int Counter = 0;

					while (parse)
					{
						// Find user
						if (parse->item != Packet->Source)
						{
							_listnode <LUserNode> * user = parse->item->Find(&command[6], false);

							if (user && !user->item->Ghost)
							{
								// Prepare response
								char buffer[512];

								strcpy(buffer, (char*)&user->item->player->Name[0]);
								strcat(buffer, " is in ");
								strcat(buffer, (char*)&user->item->zone->Name[0]);

								// Send it
								player->SendArena(buffer);
								Counter++;

								if (Counter > 5)
									return true;
							}
						}

						// Try next zone
						parse = parse->next;
					}

					if (Counter == 0)
						player->SendArena("User offline");
				}

				return true;
			}

#ifdef ERROR_TRAPPER

}
catch (...)		// catch any kind of error
{
	printf("%i - Command crash attempt by %s -> '%s'\n", Packet->Source->ScoreID, player->player->Name, command);
}

#endif

	}
	else
	{
		// Does not

		printf("%i - (INVALID)>%s\n", Packet->Source->ScoreID, &Packet->Message[5]);
	}

	return true;
}
